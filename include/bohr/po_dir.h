/******************************************************************************
* Polonium - a portability library
* Po_dir - a portable interface for finding files and directories
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#ifndef __bohr_po_dir_h__
#define __bohr_po_dir_h__

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#ifdef _WIN32
#  include <windows.h>
#else
#  include <limits.h>
#  include <dirent.h>
#  include <fnmatch.h>
#  include <sys/stat.h>
#endif


//Controls inlining of this library's functions.
#ifndef Po_DIR_INLINE
#  ifdef Po_INLINE
#     define Po_DIR_INLINE Po_INLINE
#  else
#     define Po_DIR_INLINE inline static
#  endif
#endif

//Nix non-critical C99 keywords in compilers that don't support them.
#if((!defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
 && !defined(restrict))
#  define restrict
#  define _Po_DIR_DEFINED_RESTRICT
#endif


//How big the match member of Po_findfiles is.
#ifdef _WIN32
#  define Po_MATCH_SIZE MAX_PATH
#else
#  define Po_MATCH_SIZE (NAME_MAX+1)
#endif


//Data passed to/from Po_FindFiles().  For speed, this dramatically changes
//size across platforms.
typedef struct Po_findfiles
{
   const char * pattern;      //(in) pattern to search for

   char match[Po_MATCH_SIZE]; //(out) matching filename
   int  match_dir;            //(out) whether match identifies a directory

   void * context;            //(opaque) internal context
#ifndef _WIN32
   char   context2[PATH_MAX+NAME_MAX+1+NAME_MAX+1];
   char * context3;           //(opaque) more internal context
#endif

} Po_findfiles;
#define Po_FINDFILES_INIT {NULL, {'\0'}, 0, NULL} //initializer


/* Returns a pointer to a string containing the current user's home directory,
 * or "." if it can't be determined.  Never returns NULL.  The string returned
 * will likely not have a trailing slash.  It's an error to free() the buffer
 * returned by this function.
 */
Po_DIR_INLINE const char * Po_GetHomeDirectory(void)
{
   char * home;

   //FIXME: I'll bet getenv() in Windows doesn't handle Unicode...

#ifdef _WIN32
   if(!(home = getenv("USERPROFILE")))
#else
   if(!(home = getenv("HOME")))
#endif
      home = ".";

   return home;
}

/* Iterates over filenames matching a pattern, pointed to by find->pattern for
 * the first call to this function with any Po_findfiles buffer (subsequent
 * calls using the same Po_findfiles ignore find->pattern).  The pattern should
 * be in the form [<path>]<file> where <path> if given is a relative or
 * absolute path ending with a slash and <file> is a filename possibly
 * containing wildcards (* and ?).  This ensures maximum portability and keeps
 * the algorithm simple.  Returns nonzero if a match was found (it is returned
 * in find->match and find->match_dir), or 0 if an error occurs or no (more)
 * matches are found.  Some context is maintained between calls; to free this
 * context without having to call this function until it returns 0, use
 * Po_CancelFind().  This function is thread-safe only with different
 * Po_findfiles buffers.
 */
Po_DIR_INLINE int Po_FindFiles(Po_findfiles * restrict find)
{
#ifdef _WIN32
   //find->context is the FindFirstFile() HANDLE.

   WIN32_FIND_DATA fd;
   int success;

   if(find->context)
      success = (FindNextFile((HANDLE)find->context, &fd) != 0);
   else
   {
#  ifdef UNICODE
      WCHAR pattern[MAX_PATH];

      if(!MultiByteToWideChar(CP_UTF8, 0, find->pattern, -1, pattern, MAX_PATH))
         success = 0;
      else
         success = ((HANDLE)(find->context = (void *)FindFirstFile(pattern, &fd)) != INVALID_HANDLE_VALUE);
#  else
      success = ((HANDLE)(find->context = (void *)FindFirstFile(find->pattern, &fd)) != INVALID_HANDLE_VALUE);
#  endif
   }

   if(success)
   {
#  ifdef UNICODE
      if(!WideCharToMultiByte(CP_UTF8, 0, fd.cFileName, -1, find->match, MAX_PATH, NULL, NULL))
         success = 0;
#  else
      memcpy(find->match, fd.cFileName, MAX_PATH);
#  endif
      find->match_dir = ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0);
   }
   if(!success)
   {
      FindClose((HANDLE)find->context);
      *find = (Po_findfiles)Po_FINDFILES_INIT;
   }

   return success;
#else
   //find->context is the opendir() DIR pointer.
   //context2 is a string as such:
   //   <diropen()ed path>/<readdir()'s d_name>[padding]\0<fnmatch() pattern>
   //context3 points to the beginning of <readdir()'s d_name>.

   struct dirent * entry;
   int len;
   int success = 1;

   if(!find->context)
   {
      //First time calling this function with this find->pattern.

      const char * base;

      if(!(base = strrchr(find->pattern, '/')))
      {
         //Use "./{find->pattern}".

         find->context2[0] = '.';
         find->context2[1] = '\0';
         len = 1;
         base = find->pattern - 1;
      }
      else
      {
         //Copy first part of find->pattern as the directory.
         if((len = base - find->pattern) > PATH_MAX-1)
            len = PATH_MAX-1;
         memcpy(find->context2, find->pattern, len);
         find->context2[len] = '\0';
      }

      //Copy last part of find->pattern as the pattern.
      strncpy(find->context2 + PATH_MAX + NAME_MAX + 1, base + 1, NAME_MAX);
      find->context2[PATH_MAX + NAME_MAX + 1 + NAME_MAX] = '\0';

      //Open the directory part.
      if(!(find->context = (void *)opendir(find->context2)))
         return 0;

      //Terminate the directory part with a / and save its position.
      find->context3 = find->context2 + len + 1;
      find->context2[len] = '/';
   }

   do
   {
      //Get the next entry in the directory.
      if(!(entry = readdir((DIR *)find->context)))
      {
         success = 0;
         break;
      }

      //Loop until we find a match.
   } while(fnmatch(find->context2 + PATH_MAX + NAME_MAX + 1,
                   entry->d_name, FNM_NOESCAPE) != 0);

   if(success)
   {
      //We found a (matching) file.

      struct stat st;

      //Copy it to the match member...
      if((len = strlen(entry->d_name)) > NAME_MAX)
         len = NAME_MAX;
      memcpy(find->match, entry->d_name, len);
      find->match[len] = '\0';

      //...and append it to our directory name so we can stat() it.
      memcpy(find->context3, entry->d_name, len);
      find->context3[len] = '\0';

      if(stat(find->context2, &st))
         success = 0;

      find->match_dir = (S_ISDIR(st.st_mode) != 0);
   }

   if(!success)
   {
      //Some failure along the way.

      closedir((DIR *)find->context);
      *find = (Po_findfiles)Po_FINDFILES_INIT;
   }

   return success;
#endif
}

/* Terminates an unfinished series of Po_FindFiles() calls.
 */
Po_DIR_INLINE void Po_CancelFind(Po_findfiles * restrict find)
{
#ifdef _WIN32
   if(find->context)
      FindClose((HANDLE)find->context);
#else
   if(find->context)
      closedir((DIR *)find->context);
#endif
   *find = (Po_findfiles)Po_FINDFILES_INIT;
}


#ifdef _Po_DIR_DEFINED_RESTRICT
#undef _Po_DIR_DEFINED_RESTRICT
#undef restrict
#endif

#endif

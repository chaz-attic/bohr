/******************************************************************************
* Darmstadtium - a library of data structures
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#ifndef __bohr_ds_h__
#define __bohr_ds_h__


//Include all the modules.
#include <bohr/ds_queue.h>
#include <bohr/ds_str.h>
#include <bohr/ds_vector.h>
#include <bohr/ds_hash.h>


#endif

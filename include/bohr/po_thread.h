/******************************************************************************
* Polonium - a portability library
* Po_thread - a portable thread management interface
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#ifndef __bohr_po_thread_h__
#define __bohr_po_thread_h__

#include <stddef.h>
#ifdef _WIN32
#  include <windows.h>
#  include <process.h>
#else
#  include <stdint.h>
#  include <pthread.h>
#endif


//Controls inlining of this library's functions.
#ifndef Po_THREAD_INLINE
#  ifdef Po_INLINE
#     define Po_THREAD_INLINE Po_INLINE
#  else
#     define Po_THREAD_INLINE inline static
#  endif
#endif

//Nix non-critical C99 keywords in compilers that don't support them.
#if((!defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
 && !defined(restrict))
#  define restrict
#  define _Po_THREAD_DEFINED_RESTRICT
#endif


//Windows-only calling convention modifier for thread functions.
#ifdef _WIN32
#  define Po_THREADCALL __stdcall
#else
#  define Po_THREADCALL
#endif

//A thread handle.  Changes sizes and behaviors across platforms.  Treat as an
//opaque struct, and never do arithmetic or comparison.
#ifdef _WIN32
typedef HANDLE Po_thread;
#else
typedef pthread_t Po_thread;
#endif
//NO INITIALIZER!

//A thread function pointer--declare as:
//   int Po_THREADCALL MyThread(void * restrict param);
typedef int (Po_THREADCALL * Po_thread_fn)(void *);


/* Creates a new thread, optionally joinable.  Joinable threads can be waited
 * on with Po_JoinThread(), and in fact must have Po_JoinThread() called on
 * them eventually; non-joinable threads can't automatically be waited on, but
 * have no such restriction.  Returns 0/nonzero on failure/success.  Pass NULL
 * in joinable_thread_out for a non-joinable thread; or a pointer to a
 * Po_thread, in which case the thread handle is returned in the pointee, for a
 * joinable thread.
 */
Po_THREAD_INLINE int Po_CreateThread(Po_thread * restrict joinable_thread_out,
                                     Po_thread_fn execute,
                                     void * restrict param)
{
#ifdef _WIN32
   Po_thread t;

   if(!(t = (HANDLE)_beginthreadex(NULL,
                                   0,
                                   (unsigned (__stdcall *)(void *))execute,
                                   param,
                                   0,
                                   NULL)))
   {
      return 0;
   }

   if(joinable_thread_out)
      *joinable_thread_out = t;
   else
      CloseHandle(t);

   return 1;
#else
   int success = 0;

   pthread_attr_t attr;
   if(!pthread_attr_init(&attr))
   {
      if(!pthread_attr_setdetachstate(&attr, (joinable_thread_out ?
                                              PTHREAD_CREATE_JOINABLE :
                                              PTHREAD_CREATE_DETACHED)))
      {
         Po_thread t;
         if(!pthread_create(&t, &attr, (void * (*)(void *))execute, param))
         {
            if(joinable_thread_out)
               *joinable_thread_out = t;
            success = 1;
         }
      }

      pthread_attr_destroy(&attr);
   }

   return success;
#endif
}

/* Explicitly ends the calling thread (only useful from within main(), which
 * would otherwise automatically end the whole process when it exits).
 */
Po_THREAD_INLINE void Po_ExitThread(int code)
{
#ifdef _WIN32
   _endthreadex((unsigned)code);
#else
   pthread_exit((void *)(intptr_t)code);
#endif
}

/* Violently ends the specified (joinable) thread's life.  Only available for
 * threads created as joinable.
 */
Po_THREAD_INLINE void Po_KillThread(Po_thread thread)
{
#ifdef _WIN32
   TerminateThread(thread, (DWORD)-1);
   CloseHandle(thread);
#else
   pthread_cancel(thread);
   pthread_detach(thread);
#endif
}

/* Waits for the specified (joinable) thread to finish executing and frees
 * associated resources.  Only available for threads created as joinable.
 * Optionally captures the thread's return code.  Returns 0/nonzero on
 * failure/success.
 */
Po_THREAD_INLINE int Po_JoinThread(Po_thread thread, int * restrict rc)
{
#ifdef _WIN32
   int success = 0;

   if(WaitForSingleObject(thread, INFINITE) != WAIT_FAILED)
   {
      if(rc)
      {
         DWORD win_rc;
         if(GetExitCodeThread(thread, &win_rc))
         {
            *rc = (int)win_rc;
            success = 1;
         }
      }
      else
         success = 1;

      CloseHandle(thread);
   }

   return success;
#else
   void * pt_rc;

   if(pthread_join(thread, &pt_rc))
      return 0;

   if(rc)
      *rc = (int)(intptr_t)pt_rc;

   return 1;
#endif
}


#ifdef _Po_THREAD_DEFINED_RESTRICT
#undef _Po_THREAD_DEFINED_RESTRICT
#undef restrict
#endif

#endif

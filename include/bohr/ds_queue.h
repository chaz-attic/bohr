/******************************************************************************
* Darmstadtium - a library of data structures
* Ds_queue - a thread-safe queue library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


//You must be careful about including this file multiple times.  Each time it's
//included in the same source file it must have a different Ds_QUEUE_SUFFIX.

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <bohr/po_sync.h>


//Controls inlining of this library's functions.
#ifndef Ds_QUEUE_INLINE
#  ifdef Ds_INLINE
#     define Ds_QUEUE_INLINE Ds_INLINE
#  else
#     define Ds_QUEUE_INLINE inline static
#  endif
#endif

//Nix non-critical C99 keywords in compilers that don't support them.
#if((!defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
 && !defined(restrict))
#  define restrict
#  define _Ds_QUEUE_DEFINED_RESTRICT
#endif


//How to grow when the buffer is insufficient.  The following values are
//available: 0 - the buffer is limited by its initial size and never grows;
//1 - the buffer will grow to the exact needed size; 2 - the  buffer will
//double in size until sufficient (default); or 4 - the buffer will quadruple
//in size until sufficient.
#ifndef Ds_QUEUE_BEHAVIOR
#define Ds_QUEUE_BEHAVIOR 2
#endif

//The type stored in Ds_queues for this instance of the library.
#ifndef Ds_QUEUE_TYPE
#define Ds_QUEUE_TYPE int
#endif

//Suffix appended to all symbols exported by this header.
#ifndef Ds_QUEUE_SUFFIX
#define Ds_QUEUE_SUFFIX
#endif


//Macros to make symbols including a defined suffix (there's a seemingly
//extraneous middle step because it must pass through the macro processor twice
//to get what Ds_QUEUE_SUFFIX is defined as, instead of "Ds_QUEUE_SUFFIX").
#define __Ds_QUEUE_SYMBOL_CAT(symbol, suffix) symbol ## suffix
#define _Ds_QUEUE_SYMBOL_CAT(symbol, suffix) \
                                          __Ds_QUEUE_SYMBOL_CAT(symbol, suffix)
#define _Ds_QUEUE_SYMBOL(symbol) _Ds_QUEUE_SYMBOL_CAT(symbol, Ds_QUEUE_SUFFIX)

//Define symbols that in reality have a suffix attached to them.
#define Ds_queue                   _Ds_QUEUE_SYMBOL(Ds_queue)
#define Ds_InitQueue               _Ds_QUEUE_SYMBOL(Ds_InitQueue)
#define Ds_FreeQueue               _Ds_QUEUE_SYMBOL(Ds_FreeQueue)
#define Ds_QueueSize               _Ds_QUEUE_SYMBOL(Ds_QueueSize)
#define Ds_QueueCapacity           _Ds_QUEUE_SYMBOL(Ds_QueueCapacity)
#define Ds_Enqueue                 _Ds_QUEUE_SYMBOL(Ds_Enqueue)
#define Ds_Dequeue                 _Ds_QUEUE_SYMBOL(Ds_Dequeue)


//Tells Ds_Dequeue() to wait indefinitely until an item is enqueued.
#define Ds_WAIT_FOREVER Po_WAIT_FOREVER


//A queue object.
typedef struct Ds_queue
{
   Ds_QUEUE_TYPE * buf;  //memory buffer
   size_t          num;  //how many items currently in it
   size_t          cap;  //how many items it can hold
   size_t          head; //pointer to the first item
   size_t          tail; //pointer to the last item
   Po_mutex        mtx;  //a mutex for thread synchronization
   Po_cond         cnd;  //a condition variable for waiting on

} Ds_queue;
#define Ds_QUEUE_INIT {NULL, 0, 0, 0, 0} //initializer


//TODO: make sure optimizations won't break this code anywhere.  From doing
//some basic tests (with GCC 4.2.2), it appears that regardless of whether a
//function is inline, the kind of optimizations that would cause multithreaded
//code to misbehave happen only inside the function's boundaries; i.e. you can
//write inline functions as if they were going to be made into a library and
//linked in, because they're optimized internally the same way.  I'm not sure
//where the official C spec falls, but if this is true it means the code below
//should be correct.  If, however, it turns out compilers can optimize inline
//functions within the context of where they're called, this code may break.
//The solution if this is the case is to carefully use volatile like I do in
//Ds_Dequeue(), adding its use on assignments as well.  This increases the
//code's complexity a lot, so I'll leave it off until I hear of a problem.


/* Initializes the Ds_queue object, optionally giving it an initial capacity.
 * If Ds_QUEUE_BEHAVIOR is 0, you'll want to specify cap as > 0.  broadcast is
 * passed to Po_InitCond(), and specifies how many threads are awakened when
 * items are Ds_Enqueue()d.  Returns 0/nonzero on failure/success.
 */
Ds_QUEUE_INLINE int Ds_InitQueue(Ds_queue * restrict q, size_t cap,
                                 int broadcast)
{
   *q = (Ds_queue)Ds_QUEUE_INIT;

   if(!Po_InitMutex(&q->mtx))
      return 0;
   if(!Po_InitCond(&q->cnd, broadcast))
   {
      Po_FreeMutex(&q->mtx);
      return 0;
   }

   if(cap > 0)
   {
      if(!(q->buf = (Ds_QUEUE_TYPE *)malloc(cap * sizeof(Ds_QUEUE_TYPE))))
      {
         Po_FreeCond(&q->cnd);
         Po_FreeMutex(&q->mtx);
         return 0;
      }

      q->cap = cap;
   }

   return 1;
}

/* Frees the Ds_queue's buffer and releases its mutex and condition variable.
 */
Ds_QUEUE_INLINE void Ds_FreeQueue(Ds_queue * restrict q)
{
   if(q->buf)
      free(q->buf);
   Po_FreeCond(&q->cnd);
   Po_FreeMutex(&q->mtx);
   *q = (Ds_queue)Ds_QUEUE_INIT;
}

/* Returns how many items are in the Ds_queue right now.  Uses the mutex to
 * synchronize access between threads.  Don't put too much weight on this value
 * as it may have changed from another thread by the time you use it.
 */
Ds_QUEUE_INLINE size_t Ds_QueueSize(Ds_queue * restrict q)
{
   size_t s = 0;
   if(Po_LockMutex(&q->mtx))
   {
      s = q->num;
      Po_UnlockMutex(&q->mtx);
   }
   return s;
}

/* Returns how many items the Ds_queue can hold right now.  Uses the mutex to
 * synchronize access between threads.  Don't put too much weight on this value
 * as it may have changed from another thread by the time you use it.
 */
Ds_QUEUE_INLINE size_t Ds_QueueCapacity(Ds_queue * restrict q)
{
   size_t c = 0;
   if(Po_LockMutex(&q->mtx))
   {
      c = q->cap;
      Po_UnlockMutex(&q->mtx);
   }
   return c;
}

/* Adds items to the Ds_queue.  Grows the Ds_queue if the behavior is right.
 * Returns how many items were added, or 0 on error.
 */
Ds_QUEUE_INLINE size_t Ds_Enqueue(Ds_queue * restrict q,
                                  const Ds_QUEUE_TYPE * restrict items,
                                  size_t num)
{
   if(!Po_LockMutex(&q->mtx))
      return 0;

#if(Ds_QUEUE_BEHAVIOR == 0)
   //if no growth, make sure num is capped at how many we can fit
   if(num > q->cap - q->num)
      num = q->cap - q->num;
#else
   //check capacity, grow if necessary
   size_t new_cap;

#  if(Ds_QUEUE_BEHAVIOR == 1)
   //grow to fit
   new_cap = q->num + num;
#  else
   //double or quadruple
   new_cap = (q->cap ? q->cap : 1);
   while(new_cap < q->num + num)
#     if(Ds_QUEUE_BEHAVIOR == 4)
      new_cap <<= 2; //the same as *= 4
#     else
      new_cap <<= 1; //the same as *= 2
#     endif
#  endif
   if(new_cap > q->cap)
   {
      //resize if necessary
      Ds_QUEUE_TYPE * new_buf;
      if(!(new_buf = (Ds_QUEUE_TYPE *)realloc(q->buf,
                                             new_cap * sizeof(Ds_QUEUE_TYPE))))
      {
         Po_UnlockMutex(&q->mtx);
         return 0;
      }
      q->buf = new_buf;

      //if we need to rearrange some items to keep things sequential, do so
      if(q->num > 0 && q->tail <= q->head)
      {
         memcpy(q->buf + q->cap, q->buf, q->tail * sizeof(Ds_QUEUE_TYPE));
         q->tail += q->cap;
      }

      q->cap = new_cap;
   }
#endif

   //At this point, we know there's enough capacity to hold num more items, but
   //we may need to split the items up.

   //get how many items will fit at the end
   size_t end_cap = q->cap - q->tail;

   //copy as much as will fit at the end
   memcpy(q->buf + q->tail, items, (num <= end_cap ? num : end_cap)
                                   * sizeof(Ds_QUEUE_TYPE));

   //copy whatever we missed to the beginning
   if(num > end_cap)
      memcpy(q->buf, items + end_cap, (num - end_cap) * sizeof(Ds_QUEUE_TYPE));

   //move the tail pointer down, and wrap around
   q->tail += num;
   if(q->tail >= q->cap)
      q->tail -= q->cap;

   //and, we've got this many more items now
   q->num += num;

   //if we actually added anything, wake up any waiting threads
   if(num > 0)
      Po_SignalCond(&q->cnd);

   Po_UnlockMutex(&q->mtx);
   return num;
}

/* Reads items in the Ds_queue.  If peek is 0, also removes them.  If
 * max_wait_ms is nonzero and the Ds_queue is initially empty, blocks for that
 * many milliseconds or until someone else enqueues some items (use
 * Ds_WAIT_FOREVER to never time out).  Returns how many items it read/removed.
 */
Ds_QUEUE_INLINE size_t Ds_Dequeue(Ds_queue * restrict q,
                                  Ds_QUEUE_TYPE * restrict items_out,
                                  size_t max,
                                  unsigned long max_wait_ms,
                                  int peek)
{
   if(!Po_LockMutex(&q->mtx))
      return 0;

   //save q->num as it is now (technically shouldn't need this volatile
   //rigamarole here as this is the variable's first access in this function,
   //but we'll do it for consistency's sake--it shouldn't slow things down)
   size_t q_num = *(size_t volatile *)&q->num;

   //wait or just dip out if there aren't any items
   if(!q_num)
   {
      //wait if requested
      if(max_wait_ms)
         Po_WaitForCond(&q->cnd, &q->mtx, max_wait_ms);

      //if we didn't wait, or we still don't have any items after waiting (this
      //DOES need the volatile rigamarole, since its value may have just
      //changed)
      if(!max_wait_ms || !(q_num = *(size_t volatile *)&q->num))
      {
         Po_UnlockMutex(&q->mtx);
         return 0;
      }
   }

   //now that we may have waited, and thus another thread may have just updated
   //some things, we'll capture everything else's value once (needs volatile so
   //it's not optimized to actually happen before the Po_WaitForCond() call)
   Ds_QUEUE_TYPE * q_buf  = *(Ds_QUEUE_TYPE * volatile *)&q->buf;
   size_t          q_cap  = *(size_t volatile *)&q->cap;
   size_t          q_head = *(size_t volatile *)&q->head;

   //cap how many they get at how many we have
   if(max > q_num)
      max = q_num;

   //get how many items there may be at the end of the queue
   size_t num_end = q_cap - q_head;

   //copy as many as we can from the end
   memcpy(items_out, q_buf + q_head, (max <= num_end ? max : num_end)
                                     * sizeof(Ds_QUEUE_TYPE));

   //copy anything left from the beginning
   if(max > num_end)
      memcpy(items_out + num_end, q_buf, (max - num_end)
                                         * sizeof(Ds_QUEUE_TYPE));

   //if they want to remove items, not just examine them
   if(!peek)
   {
      //move the head pointer down, and wrap around (shouldn't need to use
      //volatile here for assignment since we don't care how the compiler
      //optimizes it so long as it's eventually done correctly)
      q_head += max;
      if(q_head >= q_cap)
         q_head -= q_cap;
      q->head = q_head;

      //and, we've got this many fewer items now
      q_num -= max;
      q->num = q_num;
   }

   Po_UnlockMutex(&q->mtx);
   return max;
}


//We don't want these internal names clogging up the global namespace.
#undef Ds_Dequeue
#undef Ds_Enqueue
#undef Ds_QueueCapacity
#undef Ds_QueueSize
#undef Ds_FreeQueue
#undef Ds_InitQueue
#undef Ds_queue
#undef _Ds_QUEUE_SYMBOL
#undef _Ds_QUEUE_SYMBOL_CAT
#undef __Ds_QUEUE_SYMBOL_CAT

//Undefine these so it's easier to change them and include this file again.
#undef Ds_QUEUE_SUFFIX
#undef Ds_QUEUE_TYPE
#undef Ds_QUEUE_BEHAVIOR

#ifdef _Ds_QUEUE_DEFINED_RESTRICT
#undef _Ds_QUEUE_DEFINED_RESTRICT
#undef restrict
#endif

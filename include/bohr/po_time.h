/******************************************************************************
* Polonium - a portability library
* Po_time - a portable interface for reading high-resolution timers
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#ifndef __bohr_po_time_h__
#define __bohr_po_time_h__

#ifndef __STDC_CONSTANT_MACROS
#define __STDC_CONSTANT_MACROS
#endif
#ifndef __STDC_FORMAT_MACROS
#define __STDC_FORMAT_MACROS
#endif

#include <stdint.h>
#include <inttypes.h>
#ifdef _WIN32
#  include <windows.h>
#else
#  include <sys/time.h>
#endif


//Controls inlining of this library's functions.
#ifndef Po_TIME_INLINE
#  ifdef Po_INLINE
#     define Po_TIME_INLINE Po_INLINE
#  else
#     define Po_TIME_INLINE inline static
#  endif
#endif

//Nix non-critical C99 keywords in compilers that don't support them.
#if((!defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
 && !defined(restrict))
#  define restrict
#  define _Po_TIME_DEFINED_RESTRICT
#endif


//An integral number of ticks of the system's clock frequency.
typedef int64_t Po_time_t;

//Declares an integer constant of the type Po_time_t.
#define Po_TIME_C INT64_C

//Minimum and maximum values for a Po_time_t.
#define Po_TIME_MIN INT64_MIN
#define Po_TIME_MAX INT64_MAX

//printf() specifiers to print Po_time_ts.
#define PRIdPo_TIME PRId64
#define PRIiPo_TIME PRIi64
#define PRIoPo_TIME PRIo64
#define PRIuPo_TIME PRIu64
#define PRIxPo_TIME PRIx64
#define PRIXPo_TIME PRIX64

//scanf() specifiers to read Po_time_ts.
#define SCNdPo_TIME SCNd64
#define SCNiPo_TIME SCNi64
#define SCNoPo_TIME SCNo64
#define SCNuPo_TIME SCNu64
#define SCNxPo_TIME SCNx64


//Clock frequency for anything but Windows is 1mHz.
#define _Po_UNIX_FREQUENCY Po_TIME_C(1000000)

/* Returns the system clock frequency, i.e. ticks per second.  As this function
 * is potentially slow, call it once in initialization and save its value.
 * Returns 0 on failure.  This function must be successfully called before
 * calling Po_GetTime().
 */
Po_TIME_INLINE Po_time_t Po_GetFrequency(void)
{
#ifdef _WIN32
   Po_time_t frequency;
   if(!QueryPerformanceFrequency((LARGE_INTEGER *)&frequency))
      frequency = Po_TIME_C(0);
   return frequency;
#else
   return _Po_UNIX_FREQUENCY;
#endif
}

/* Returns the system time in ticks, based on some arbitrary (but constant for
 * the duration of the program) starting point.
 */
Po_TIME_INLINE Po_time_t Po_GetTime(void)
{
#ifdef _WIN32
   Po_time_t now;
   QueryPerformanceCounter((LARGE_INTEGER *)&now);
   return now;
#else
   struct timeval now;
   gettimeofday(&now, 0);
   return (((Po_time_t)now.tv_sec * _Po_UNIX_FREQUENCY + (Po_time_t)now.tv_usec));
#endif
}

#undef _Po_UNIX_FREQUENCY


#ifdef _Po_TIME_DEFINED_RESTRICT
#undef _Po_TIME_DEFINED_RESTRICT
#undef restrict
#endif

#endif

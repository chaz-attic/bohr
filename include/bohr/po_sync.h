/******************************************************************************
* Polonium - a portability library
* Po_sync - a portable thread synchronization library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#ifndef __bohr_po_sync_h__
#define __bohr_po_sync_h__

#if(!defined(_GNU_SOURCE) && (!defined(_XOPEN_SOURCE) || _XOPEN_SOURCE < 500))
#  error files including bohr/po_sync.h must be compiled with \
            -D_XOPEN_SOURCE=500 (or =600) or -D_GNU_SOURCE
#endif

#include <stddef.h>
#include <limits.h>
#include <assert.h>
#ifdef _WIN32
#  include <windows.h>
#else
#  include <time.h>
#  include <pthread.h>
#  include <sched.h>
#endif


//Controls inlining of this library's functions.
#ifndef Po_SYNC_INLINE
#  ifdef Po_INLINE
#     define Po_SYNC_INLINE Po_INLINE
#  else
#     define Po_SYNC_INLINE inline static
#  endif
#endif

//Nix non-critical C99 keywords in compilers that don't support them.
#if((!defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
 && !defined(restrict))
#  define restrict
#  define _Po_SYNC_DEFINED_RESTRICT
#endif


//Tells Po_WaitForCond() to never time out.
#define Po_WAIT_FOREVER ULONG_MAX


//A mutex object.  For speed, this dramatically changes size across platforms.
//It's an opaque structure that should not be moved or copied once declared.
#ifdef _WIN32
   typedef HANDLE Po_mutex;
#else
   typedef pthread_mutex_t Po_mutex;
#endif
//NO INITIALIZER!


//A condition variable object.  Also opaque and dramatically different across
//platforms; don't move or copy once declared.  Every condition variable must
//have exactly one associated Po_mutex; you can use the associated Po_mutex
//elsewhere, but it's an error to use any Po_mutex other than the associated
//one with a condition variable.
#ifdef _WIN32
   typedef HANDLE Po_cond;
#else
   typedef struct Po_cond
   {
      pthread_cond_t cnd;
      int            broadcast;

   } Po_cond;
#endif
//NO INITIALIZER!


/* Puts the calling thread to sleep for approximately the specified number of
 * milliseconds.  If sleep_ms is 0, this function yields the calling thread to
 * other threads of the same priority, but doesn't block.
 */
Po_SYNC_INLINE void Po_Sleep(unsigned long sleep_ms)
{
#ifdef _WIN32
   Sleep((DWORD)sleep_ms);
#else
   if(!sleep_ms)
      sched_yield();
   else
   {
      struct timespec sleep, rem;
      rem.tv_sec = sleep_ms / 1000;
      rem.tv_nsec = (sleep_ms - rem.tv_sec*1000) * 1000000;
      do
      {
         sleep = rem;
      } while(nanosleep(&sleep, &rem));
   }
#endif
}

/* Initializes internals of a mutex.  Returns 0/nonzero on failure/success.
 */
Po_SYNC_INLINE int Po_InitMutex(Po_mutex * restrict mtx)
{
#ifdef _WIN32
   return ((*mtx = CreateMutex(NULL, FALSE, NULL)) != NULL);
#else
   int success = 0;

   //we need to guarantee that the mutices are recursive
   pthread_mutexattr_t attr;
   if(!pthread_mutexattr_init(&attr))
   {
      if(!pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE))
         success = !pthread_mutex_init(mtx, &attr);

      pthread_mutexattr_destroy(&attr);
   }

   return success;
#endif
}

/* Frees internals of a mutex initialized with Po_InitMutex().  The mutex must
 * not be locked.
 */
Po_SYNC_INLINE void Po_FreeMutex(Po_mutex * restrict mtx)
{
#ifdef _WIN32
   CloseHandle(*mtx);
#else
   pthread_mutex_destroy(mtx);
#endif
}

/* Locks the mutex initialized with Po_InitMutex().  Only one thread can hold a
 * lock on the mutex at once.  Returns 0/nonzero on failure/success.
 */
Po_SYNC_INLINE int Po_LockMutex(Po_mutex * restrict mtx)
{
#ifdef _WIN32
   return (WaitForSingleObject(*mtx, INFINITE) == WAIT_OBJECT_0);
#else
   return (!pthread_mutex_lock(mtx));
#endif
}

/* Locks the mutex if it's immediately available.  Returns 0 on failure or if
 * the mutex wasn't available, or nonzero on successfully locking the mutex.
 */
Po_SYNC_INLINE int Po_TryLockMutex(Po_mutex * restrict mtx)
{
#ifdef _WIN32
   return (WaitForSingleObject(*mtx, 0) == WAIT_OBJECT_0);
#else
   return (!pthread_mutex_trylock(mtx));
#endif
}

/* Unlocks the mutex previously successfully locked with Po_LockMutex() (or
 * Po_TryLockMutex()).
 */
Po_SYNC_INLINE void Po_UnlockMutex(Po_mutex * restrict mtx)
{
#ifdef _WIN32
   ReleaseMutex(*mtx);
#else
   pthread_mutex_unlock(mtx);
#endif
}

/* Initializes a condition variable.  If broadcast is nonzero, Po_SignalCond()
 * will act as a broadcast on this Po_cond, waking up all waiting threads (else
 * just one is woken up).  Returns 0/nonzero on failure/success.
 */
Po_SYNC_INLINE int Po_InitCond(Po_cond * restrict cnd, int broadcast)
{
#ifdef _WIN32
   //To just signal one thread at a time, we use an auto-reset event, which the
   //MSDN docs claim will return to non-signaled after one thread wakes up.
   //For broadcasting, we use a manual-reset and rely on the call to
   //ResetEvent() at the beginning of Po_WaitForCond() to stop the threads that
   //want to wait.
   return ((*cnd = CreateEvent(NULL,
                               (broadcast ? TRUE : FALSE),
                               FALSE,
                               NULL)) != NULL);
#else
   cnd->broadcast = broadcast;
   return (!pthread_cond_init(&cnd->cnd, NULL));
#endif
}

/* Frees a condition variable initialized with Po_InitCond().
 */
Po_SYNC_INLINE void Po_FreeCond(Po_cond * restrict cnd)
{
#ifdef _WIN32
   CloseHandle(*cnd);
#else
   pthread_cond_destroy(&cnd->cnd);
#endif
}

/* Waits for the condition variable to become signaled and wake up the calling
 * thread.  As stated above, each condition variable must have exactly one
 * associated Po_mutex; it's an error to use any Po_mutex other than the
 * associated one here.  This function MUST ONLY be called from a construct as
 * such, where the actual predicate is checked before waiting:
 *
 *    Po_LockMutex(the associated mutex);
 *    while(some predicate we need to proceed evaluates to false)
 *       Po_WaitForCond(the condition, the associated mutex, however long);
 *    //...do work, or unlock the associated mutex and do work, or whatever
 *
 * The associated mutex must obviously be locked before calling this function;
 * this function atomically unlocks the mutex and waits on the condition.
 * Returns 0 if the wait times out (use Po_WAIT_FOREVER for max_wait_ms for it
 * to never time out) or there's an error, or nonzero if the condition is
 * probably true now.
 */
Po_SYNC_INLINE int Po_WaitForCond(Po_cond * restrict cnd,
                                  Po_mutex * restrict mtx,
                                  unsigned long max_wait_ms)
{
#ifdef _WIN32
   int success = 0;

   //since we assume we've just checked that the predicate for our cond is
   //false, we make sure the event wasn't still set from last time
   ResetEvent(*cnd);

   //atomically release the mutex and wait on the event to get signaled
   success = (SignalObjectAndWait(*mtx,
                                  *cnd,
                                  (max_wait_ms == Po_WAIT_FOREVER ?
                                   INFINITE :
                                   (DWORD)max_wait_ms),
                                  FALSE) == WAIT_OBJECT_0);

   //and then relock the mutex
   int relock_failed = (WaitForSingleObject(*mtx, INFINITE) != WAIT_OBJECT_0);
   assert(!relock_failed);

   return success;
#else
   //if they're waiting indefinitely, we don't need a timed wait
   if(max_wait_ms == Po_WAIT_FOREVER)
      return (!pthread_cond_wait(&cnd->cnd, mtx));

   //we need to convert between milliseconds and absolute time
   struct timespec ts;
   int clock_gettime_failed = clock_gettime(CLOCK_REALTIME, &ts);
   assert(!clock_gettime_failed);

   unsigned long sec;
   ts.tv_sec += (sec = max_wait_ms/1000);
   if((ts.tv_nsec += (max_wait_ms - sec*1000) * 1000000) > 999999999)
   {
      ts.tv_nsec -= 1000000000;
      ts.tv_sec += 1;
   }

   //and do a timed wait
   return (!pthread_cond_timedwait(&cnd->cnd, mtx, &ts));
#endif
}

/* Awakens one or more, depending on how the Po_cond was created, threads
 * waiting on the condition.  Note that this MUST be called from a thread that
 * has a lock on the associated Po_mutex:
 *
 *    Po_LockMutex(the associated mutex);
 *    //...
 *    Po_SignalCond(the condition);
 *    //... unlock the mutex or do some more work or whatever
 */
Po_SYNC_INLINE void Po_SignalCond(Po_cond * restrict cnd)
{
#ifdef _WIN32
   //In Windows, the difference is in how the event is created, not signaled.
   SetEvent(*cnd);
#else
   if(cnd->broadcast)
      pthread_cond_broadcast(&cnd->cnd);
   else
      pthread_cond_signal(&cnd->cnd);
#endif
}


#ifdef _Po_SYNC_DEFINED_RESTRICT
#undef _Po_SYNC_DEFINED_RESTRICT
#undef restrict
#endif

#endif

/******************************************************************************
* Indium - a portable low-level input library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#ifndef __bohr_in_h__
#define __bohr_in_h__

#ifndef __STDC_CONSTANT_MACROS
#define __STDC_CONSTANT_MACROS
#endif

#include <stddef.h>
#include <stdint.h>
#include <bohr/po_time.h>


//Controls importing in Windows.
#ifndef In_PUBLIC
#  if(defined(_WIN32))
#     define In_PUBLIC __declspec(dllimport)
#  else
#     define In_PUBLIC
#  endif
#endif

//Nix non-critical C99 keywords in compilers that don't support them.
#if((!defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
 && !defined(restrict))
#  define restrict
#  define _In_DEFINED_RESTRICT
#endif


#ifdef __cplusplus
extern "C"
{
#endif


//The version of Indium this header comes from.
#define In_HEADER_VERSION UINT32_C(0x00010000) //v0.1.0(.0)

//The version of Indium you want when you include this file.
#ifndef In_VERSION
#define In_VERSION In_HEADER_VERSION
#endif


//Bit masks for In_action values, below.  These three are mutually exclusive.
#define In_KEY_MASK    0x4000 //if set, it's a keyboard action
#define In_MOUSE_MASK  0x2000 //if set, it's a mouse action
#define In_JOY_MASK    0x1000 //if set, it's a joystick action

//Bit flag for whether the In_action is a button or other (e.g. axis) action.
#define In_BUTTON_FLAG 0x0800 //if set, it's a button-style action


//An input action.  This tells you what happened in an In_event.  Note that
//depending on the hardware, drivers, and platform Indium is running on, some
//of these actions may never be generated.
typedef enum In_action
{
   In_UNKNOWN_ACTION, //an unknown action


   /***************************************************************************
   ********************************************************* KEYBOARD ACTIONS *
   ***************************************************************************/

   //The only rhyme or reason to these keyboard actions is that I try to keep
   //logically sequential things together.  Otherwise, this ordering is
   //probably wrong for every input driver on the planet.  So, these may change
   //in the future.

   //alphabet
   In_KEY_A = (1 | In_KEY_MASK | In_BUTTON_FLAG),    In_KEY_B,
   In_KEY_C, In_KEY_D, In_KEY_E, In_KEY_F, In_KEY_G, In_KEY_H,
   In_KEY_I, In_KEY_J, In_KEY_K, In_KEY_L, In_KEY_M, In_KEY_N,
   In_KEY_O, In_KEY_P, In_KEY_Q, In_KEY_R, In_KEY_S, In_KEY_T,
   In_KEY_U, In_KEY_V, In_KEY_W, In_KEY_X, In_KEY_Y, In_KEY_Z,

   //numbers
   In_KEY_0, In_KEY_1, In_KEY_2, In_KEY_3, In_KEY_4,
   In_KEY_5, In_KEY_6, In_KEY_7, In_KEY_8, In_KEY_9,

   //other keys on main keyboard (on "standard" keyboards)
   In_KEY_QUOTE,             /* '"                */
   In_KEY_BACKQUOTE,         /* `~                */
   In_KEY_SLASH,             /* /?                */
   In_KEY_BACKSLASH,         /* \|                */
   In_KEY_MINUS,             /* -_                */
   In_KEY_EQUALS,            /* =+                */
   In_KEY_BACKSPACE,
   In_KEY_SPACE,             /* space bar         */
   In_KEY_ENTER,
   In_KEY_TAB,
   In_KEY_CAPSLOCK,
   In_KEY_SEMICOLON,         /* ;:                */
   In_KEY_COMMA,             /* ,<                */
   In_KEY_PERIOD,            /* .>                */
   In_KEY_LBRACKET,          /* [{                */
   In_KEY_RBRACKET,          /* ]}                */
   In_KEY_LSHIFT,            /* left shift        */
   In_KEY_RSHIFT,            /* right shift       */
   In_KEY_LCTRL,             /* left control      */
   In_KEY_RCTRL,             /* right control     */
   In_KEY_LALT,              /* left alt          */
   In_KEY_RALT,              /* right alt         */
   In_KEY_LMETA,             /* left meta         */
   In_KEY_RMETA,             /* right meta        */
   In_KEY_COMPOSE,           /* compose key       */

   //an absurd number of function keys
   In_KEY_F1,  In_KEY_F2,  In_KEY_F3,  In_KEY_F4,  In_KEY_F5,  In_KEY_F6,
   In_KEY_F7,  In_KEY_F8,  In_KEY_F9,  In_KEY_F10, In_KEY_F11, In_KEY_F12,
   In_KEY_F13, In_KEY_F14, In_KEY_F15, In_KEY_F16, In_KEY_F17, In_KEY_F18,
   In_KEY_F19, In_KEY_F20, In_KEY_F21, In_KEY_F22, In_KEY_F23, In_KEY_F24,

   //other keys on the very top row
   In_KEY_ESC,               /* escape            */
   In_KEY_SYSRQ,             /* or print screen   */
   In_KEY_SCRLOCK,           /* scroll lock       */
   In_KEY_BREAK,             /* or pause          */

   //extended keyboards
   In_KEY_INS,               /* insert            */
   In_KEY_DEL,               /* delete            */
   In_KEY_HOME,
   In_KEY_END,
   In_KEY_PGUP,              /* page up           */
   In_KEY_PGDN,              /* page down         */

   //arrow keys
   In_KEY_UP, In_KEY_LEFT, In_KEY_DOWN, In_KEY_RIGHT,

   //numpad
   In_KEY_NUMLOCK,
   In_KEY_PAD_ENTER,
   In_KEY_PAD_EQUALS,     /* on some non-"standard" keyboards */
   In_KEY_PAD_PLUS,
   In_KEY_PAD_MINUS,
   In_KEY_PAD_STAR,
   In_KEY_PAD_SLASH,
   In_KEY_PAD_PERIOD,
   In_KEY_PAD_COMMA,      /* on some non-"standard" keyboards */
   In_KEY_PAD_0, In_KEY_PAD_1, In_KEY_PAD_2, In_KEY_PAD_3, In_KEY_PAD_4,
   In_KEY_PAD_5, In_KEY_PAD_6, In_KEY_PAD_7, In_KEY_PAD_8, In_KEY_PAD_9,

   //keys on newfangled keyboards
   In_KEY_MEDIA_PLAY,        /* play/pause        */
   In_KEY_MEDIA_STOP,        /* stop playback     */
   In_KEY_MEDIA_NEXT,        /* next track        */
   In_KEY_MEDIA_PREV,        /* prev track        */
   In_KEY_MEDIA_MUTE,        /* mute volume       */
   In_KEY_MEDIA_VOLUP,       /* volume up         */
   In_KEY_MEDIA_VOLDN,       /* volume down       */
   In_KEY_MEDIA_SELECT,      /* media select      */
   //These keys are commented out pending a thorough examination of their
   //usefulness in games, and how complicated it is to get these codes on
   //different platforms.
   //In_KEY_WEB_HOME,          /* browse to home    */
   //In_KEY_WEB_SEARCH,        /* search page       */
   //In_KEY_WEB_BOOKMARKS,     /* show favorites    */
   //In_KEY_WEB_REFRESH,       /* refresh page      */
   //In_KEY_WEB_STOP,          /* stop loading page */
   //In_KEY_WEB_FORWARD,       /* history forward   */
   //In_KEY_WEB_BACK,          /* history back      */
   //In_KEY_SYS_POWER,         /* system power      */
   //In_KEY_SYS_SLEEP,         /* system sleep      */
   //In_KEY_SYS_WAKE,          /* system wake up    */
   //In_KEY_APP_WWWBROWSER,    /* launch browser    */
   //In_KEY_APP_CALC,          /* go calculator!    */
   //In_KEY_APP_FILEBROWSER,   /* go my computer!   */
   //In_KEY_APP_MAIL,          /* GO MAIL!          */
   //In_KEY_APP_HELP,          /* help launcher     */

   //keys on old/non-US keyboards
   //These keys are commented out pending a review of what the christ they
   //actually are.
   //In_KEY_102,               /* <> or \| on RT 102-key keyboard  */
   //In_KEY_J_YEN,             /* yen key on Japanese keyboards    */
   //In_KEY_J_KANJI,           /* (?) on Japanese keyboards        */
   //In_KEY_J_CONVERT,         /* (?) on Japanese keyboards        */
   //In_KEY_J_NOCONVERT,       /* (?) on Japanese keyboards        */
   //In_KEY_J_AX,              /* (?) on Japanese AX keyboard      */
   //In_KEY_J_UNLABELED,       /* unlabeled key on J3100 keyboard  */
   //In_KEY_PC98_AT,           /* @ on NEC PC98 keyboard           */
   //In_KEY_PC98_COLON,        /* : on NEC PC98 keyboard           */
   //In_KEY_PC98_UNDERSCORE,   /* _ on NEC PC98 keyboard           */
   //In_KEY_PC98_STOP,         /* stop key on NEC PC98 keyboard    */

   //alternate names
   In_KEY_HELP      = In_KEY_INS,         /* help on mac keyboards */
   In_KEY_PAD_CLEAR = In_KEY_NUMLOCK,     /* clear on mac keypads  */
   In_KEY_LWIN      = In_KEY_LMETA,       /* left win95/vista key  */
   In_KEY_RWIN      = In_KEY_RMETA,       /* right win95/vista key */
   In_KEY_MENU      = In_KEY_COMPOSE,     /* win95 app menu key    */


   /***************************************************************************
   ************************************************************ MOUSE ACTIONS *
   ***************************************************************************/

   //mouse axes
   In_MOUSE_AXIS_X = (1 | In_MOUSE_MASK),  /* horizontal        */
   In_MOUSE_AXIS_Y,                        /* vertical          */
   In_MOUSE_AXIS_Z,                        /* mouse wheel!      */

   //main buttons
   In_MOUSE_BUTTON_0 = (1 | In_MOUSE_MASK | In_BUTTON_FLAG),  /* left   */
   In_MOUSE_BUTTON_1,                                         /* right  */
   In_MOUSE_BUTTON_2,                                         /* middle */

   //other buttons
   In_MOUSE_BUTTON_3,  In_MOUSE_BUTTON_4,  In_MOUSE_BUTTON_5,
   In_MOUSE_BUTTON_6,  In_MOUSE_BUTTON_7,  In_MOUSE_BUTTON_8,
   In_MOUSE_BUTTON_9,  In_MOUSE_BUTTON_10, In_MOUSE_BUTTON_11,
   In_MOUSE_BUTTON_12, In_MOUSE_BUTTON_13, In_MOUSE_BUTTON_14,
   In_MOUSE_BUTTON_15, In_MOUSE_BUTTON_16, In_MOUSE_BUTTON_17,
   In_MOUSE_BUTTON_18, In_MOUSE_BUTTON_19, In_MOUSE_BUTTON_20,
   In_MOUSE_BUTTON_21, In_MOUSE_BUTTON_22, In_MOUSE_BUTTON_23,
   In_MOUSE_BUTTON_24, In_MOUSE_BUTTON_25, In_MOUSE_BUTTON_26,
   In_MOUSE_BUTTON_27, In_MOUSE_BUTTON_28, In_MOUSE_BUTTON_29,
   In_MOUSE_BUTTON_30, In_MOUSE_BUTTON_31,

   //alternate names
   In_MOUSE_BUTTON_LEFT    = In_MOUSE_BUTTON_0,
   In_MOUSE_BUTTON_RIGHT   = In_MOUSE_BUTTON_1,
   In_MOUSE_BUTTON_MIDDLE  = In_MOUSE_BUTTON_2,
   In_MOUSE_AXIS_WHEEL     = In_MOUSE_AXIS_Z,


   /***************************************************************************
   ********************************************************* JOYSTICK ACTIONS *
   ***************************************************************************/

   //axis positions
   In_JOY_AXIS_X = (1 | In_JOY_MASK),   /* left-right        */
   In_JOY_AXIS_Y,                       /* foreward-backward */
   In_JOY_AXIS_Z,                       /* throttle          */

   //axis rotations (count as axes)
   In_JOY_ROT_X,             /* x axis rotation   */
   In_JOY_ROT_Y,             /* y axis rotation   */
   In_JOY_ROT_Z,             /* rudder            */

   //extra axis (silder) positions (count as axes)
   In_JOY_SLIDER_0, In_JOY_SLIDER_1,

   //POV hat directions (count as axes)
   In_JOY_HAT_0, In_JOY_HAT_1, In_JOY_HAT_2, In_JOY_HAT_3,

   //buttons
   In_JOY_BUTTON_0 = (1 | In_JOY_MASK | In_BUTTON_FLAG),
   In_JOY_BUTTON_1,   In_JOY_BUTTON_2,   In_JOY_BUTTON_3,
   In_JOY_BUTTON_4,   In_JOY_BUTTON_5,   In_JOY_BUTTON_6,   In_JOY_BUTTON_7,
   In_JOY_BUTTON_8,   In_JOY_BUTTON_9,   In_JOY_BUTTON_10,  In_JOY_BUTTON_11,
   In_JOY_BUTTON_12,  In_JOY_BUTTON_13,  In_JOY_BUTTON_14,  In_JOY_BUTTON_15,
   In_JOY_BUTTON_16,  In_JOY_BUTTON_17,  In_JOY_BUTTON_18,  In_JOY_BUTTON_19,
   In_JOY_BUTTON_20,  In_JOY_BUTTON_21,  In_JOY_BUTTON_22,  In_JOY_BUTTON_23,
   In_JOY_BUTTON_24,  In_JOY_BUTTON_25,  In_JOY_BUTTON_26,  In_JOY_BUTTON_27,
   In_JOY_BUTTON_28,  In_JOY_BUTTON_29,  In_JOY_BUTTON_30,  In_JOY_BUTTON_31,
   In_JOY_BUTTON_32,  In_JOY_BUTTON_33,  In_JOY_BUTTON_34,  In_JOY_BUTTON_35,
   In_JOY_BUTTON_36,  In_JOY_BUTTON_37,  In_JOY_BUTTON_38,  In_JOY_BUTTON_39,
   In_JOY_BUTTON_40,  In_JOY_BUTTON_41,  In_JOY_BUTTON_42,  In_JOY_BUTTON_43,
   In_JOY_BUTTON_44,  In_JOY_BUTTON_45,  In_JOY_BUTTON_46,  In_JOY_BUTTON_47,
   In_JOY_BUTTON_48,  In_JOY_BUTTON_49,  In_JOY_BUTTON_50,  In_JOY_BUTTON_51,
   In_JOY_BUTTON_52,  In_JOY_BUTTON_53,  In_JOY_BUTTON_54,  In_JOY_BUTTON_55,
   In_JOY_BUTTON_56,  In_JOY_BUTTON_57,  In_JOY_BUTTON_58,  In_JOY_BUTTON_59,
   In_JOY_BUTTON_60,  In_JOY_BUTTON_61,  In_JOY_BUTTON_62,  In_JOY_BUTTON_63,
   In_JOY_BUTTON_64,  In_JOY_BUTTON_65,  In_JOY_BUTTON_66,  In_JOY_BUTTON_67,
   In_JOY_BUTTON_68,  In_JOY_BUTTON_69,  In_JOY_BUTTON_70,  In_JOY_BUTTON_71,
   In_JOY_BUTTON_72,  In_JOY_BUTTON_73,  In_JOY_BUTTON_74,  In_JOY_BUTTON_75,
   In_JOY_BUTTON_76,  In_JOY_BUTTON_77,  In_JOY_BUTTON_78,  In_JOY_BUTTON_79,
   In_JOY_BUTTON_80,  In_JOY_BUTTON_81,  In_JOY_BUTTON_82,  In_JOY_BUTTON_83,
   In_JOY_BUTTON_84,  In_JOY_BUTTON_85,  In_JOY_BUTTON_86,  In_JOY_BUTTON_87,
   In_JOY_BUTTON_88,  In_JOY_BUTTON_89,  In_JOY_BUTTON_90,  In_JOY_BUTTON_91,
   In_JOY_BUTTON_92,  In_JOY_BUTTON_93,  In_JOY_BUTTON_94,  In_JOY_BUTTON_95,
   In_JOY_BUTTON_96,  In_JOY_BUTTON_97,  In_JOY_BUTTON_98,  In_JOY_BUTTON_99,
   In_JOY_BUTTON_100, In_JOY_BUTTON_101, In_JOY_BUTTON_102, In_JOY_BUTTON_103,
   In_JOY_BUTTON_104, In_JOY_BUTTON_105, In_JOY_BUTTON_106, In_JOY_BUTTON_107,
   In_JOY_BUTTON_108, In_JOY_BUTTON_109, In_JOY_BUTTON_110, In_JOY_BUTTON_111,
   In_JOY_BUTTON_112, In_JOY_BUTTON_113, In_JOY_BUTTON_114, In_JOY_BUTTON_115,
   In_JOY_BUTTON_116, In_JOY_BUTTON_117, In_JOY_BUTTON_118, In_JOY_BUTTON_119,
   In_JOY_BUTTON_120, In_JOY_BUTTON_121, In_JOY_BUTTON_122, In_JOY_BUTTON_123,
   In_JOY_BUTTON_124, In_JOY_BUTTON_125, In_JOY_BUTTON_126, In_JOY_BUTTON_127

} In_action;


//The following are helpers for making In_action values out of sensible
//integers.

//Turns upper case ascii chars to In_KEY_A-In_KEY_Z:
#define In_KEY_ALPHA(ch)     ((In_action)((int)In_KEY_A + (int)(ch) - 'A'))

//Turns (integers, not ascii) 0-9 to In_KEY_0-In_KEY_9:
#define In_KEY_NUMBER(x)     ((In_action)((int)In_KEY_0          + (int)(x)))

//Turns 1-24 into In_KEY_F1-In_KEY_F24:
#define In_KEY_FUNCTION(x)   ((In_action)((int)In_KEY_F1 - 1     + (int)(x)))

//Turns (integers, not ascii) 0-9 to In_KEY_PAD_0-In_KEY_PAD_9:
#define In_KEY_PAD_NUMBER(x) ((In_action)((int)In_KEY_PAD_0      + (int)(x)))

//Turns 0-15 into In_MOUSE_BUTTON_0-In_MOUSE_BUTTON_15:
#define In_MOUSE_BUTTON(x)   ((In_action)((int)In_MOUSE_BUTTON_0 + (int)(x)))

//Turns 0-2 into In_MOUSE_AXIS_X-In_MOUSE_AXIS_Z:
#define In_MOUSE_AXIS(x)     ((In_action)((int)In_MOUSE_AXIS_X   + (int)(x)))

//Turns 0-127 into In_JOY_BUTTON_0-In_JOY_BUTTON_127:
#define In_JOY_BUTTON(x)     ((In_action)((int)In_JOY_BUTTON_0   + (int)(x)))

//Turns 0-2 into In_JOY_AXIS_X-In_JOY_AXIS_Z:
#define In_JOY_AXIS(x)       ((In_action)((int)In_JOY_AXIS_X     + (int)(x)))

//Turns 0-2 into In_JOY_ROT_X-In_JOY_ROT_Z:
#define In_JOY_ROT(x)        ((In_action)((int)In_JOY_ROT_X      + (int)(x)))

//Turns 0-1 into In_JOY_SLIDER_0-In_JOY_SLIDER_1:
#define In_JOY_SLIDER(x)     ((In_action)((int)In_JOY_SLIDER_0   + (int)(x)))

//Turns 0-3 into In_JOY_HAT_0-In_JOY_HAT_3:
#define In_JOY_HAT(x)        ((In_action)((int)In_JOY_HAT_0      + (int)(x)))


//The following are helpers for turning In_action values into sensible
//integers.

//Turns an In_action in the range In_KEY_A-In_KEY_Z into 'A'-'Z' ascii values:
#define In_GET_KEY_ALPHA(a)      ((int)(a) - (int)In_KEY_A + 'A'   )

//Turns an In_action In_KEY_0-In_KEY_9 into 0-9 (integers, not ascii):
#define In_GET_KEY_NUMBER(a)     ((int)(a) - (int)In_KEY_0         )

//Turns In_KEY_F1-In_KEY_F24 into 1-24:
#define In_GET_KEY_FUNCTION(a)   ((int)(a) - (int)In_KEY_F1 + 1    )

//Turns an In_action In_KEY_PAD_0-In_KEY_PAD_9 into 0-9 (integers, not ascii):
#define In_GET_KEY_PAD_NUMBER(a) ((int)(a) - (int)In_KEY_PAD_0     )

//Turns an In_action In_MOUSE_BUTTON_0-In_MOUSE_BUTTON_15 into 0-15:
#define In_GET_MOUSE_BUTTON(a)   ((int)(a) - (int)In_MOUSE_BUTTON_0)

//Turns an In_action In_MOUSE_AXIS_X-In_MOUSE_AXIS_Z into 0-2:
#define In_GET_MOUSE_AXIS(a)     ((int)(a) - (int)In_MOUSE_AXIS_X  )

//Turns an In_action In_JOY_BUTTON_0-In_JOY_BUTTON_127 into 0-127:
#define In_GET_JOY_BUTTON(a)     ((int)(a) - (int)In_JOY_BUTTON_0  )

//Turns an In_action In_JOY_AXIS_X-In_JOY_AXIS_Z into 0-2:
#define In_GET_JOY_AXIS(a)       ((int)(a) - (int)In_JOY_AXIS_X    )

//Turns an In_action In_JOY_ROT_X-In_JOY_ROT_Z into 0-2
#define In_GET_JOY_ROT(a)        ((int)(a) - (int)In_JOY_ROT_X     )

//Turns an In_action In_JOY_SLIDER_0-In_JOY_SLIDER_1 into 0-1:
#define In_GET_JOY_SLIDER(a)     ((int)(a) - (int)In_JOY_SLIDER_0  )

//Turns an In_action In_JOY_HAT_0-In_JOY_HAT_3 into 0-3:
#define In_GET_JOY_HAT(a)        ((int)(a) - (int)In_JOY_HAT_0     )


//The following are helpers for getting information about In_action values.

//Nonzero if the action is for a button, 0 otherwise (e.g. an axis):
#define In_IS_BUTTON_ACTION(a) ((int)(a) & In_BUTTON_FLAG)

//Inverse of In_IS_BUTTON_ACTION():
#define In_IS_AXIS_ACTION(a)   (!In_IS_BUTTON_ACTION(a))

//Nonzero if the action is an In_KEY_*, 0 otherwise:
#define In_IS_KEY_ACTION(a)    ((int)(a) & In_KEY_MASK)

//Nonzero if the action is an In_MOUSE_*, 0 otherwise:
#define In_IS_MOUSE_ACTION(a)  ((int)(a) & In_MOUSE_MASK)

//Nonzero if the action is an In_JOY_*, 0 otherwise:
#define In_IS_JOY_ACTION(a)    ((int)(a) & In_JOY_MASK)


//Specifies a type of input device.
typedef enum In_device_type
{
   In_KEYBOARD = 1, //the device is a keyboard
   In_MOUSE,        //the device is a mouse-like device
   In_JOYSTICK      //the device is anything else

} In_device_type;


//How big device name buffers need to be.
#define In_NAME_SIZE 128

//Holds information about a particular input device.
typedef struct In_device_info
{
   char           name[In_NAME_SIZE]; //human-readable, probably-unique name
   In_device_type type;               //what type of device
   int            index;              //current index, in case you forgot

   int buttons; //how many buttons it has (wildly inaccurate for keyboards)
   int axes;    //how many axes (mouse/joy only)
   int hats;    //how many POV hats (joy only)

   int is_open; //whether it's currently open, in case you forgot

} In_device_info;


//The minimum and maximum values for joystick axes/hats/whatevers.  Think of
//the units as hundredths of a percent.
#define In_JOY_AXIS_MIN (-10000)
#define In_JOY_AXIS_MAX 10000

//Describes an input device event, like pressing a key or moving the mouse.
typedef struct In_event
{
   int       from;   //index of originating device
   In_action action; //what action happened

   //A union of different names to describe "how much".
   union
   {
      int down;   //(all buttons) nonzero if pressed, 0 if released
      int change; //(all mouse axes) how much it changed (arbitrary units)
      int value;  //(all joy axes) the axis' new value [In_JOY_AXIS_MIN,...MAX]
   };

   Po_time_t time; //approximate time the event was generated

} In_event;


//Define a queue that works with our In_events.  This queue has the type name
//Ds_queue_In; you can get items out of it with Ds_Dequeue_In().
#define Ds_QUEUE_BEHAVIOR 2
#define Ds_QUEUE_TYPE In_event
#define Ds_QUEUE_SUFFIX _In
#include <bohr/ds_queue.h>

//Define a callback type, so you can use either the queue or get called back.
//Declare as:
//   int MyCallback(In_event * restrict events, size_t num,
//                  void * restrict param);
typedef int (* In_callback_fn)(In_event * events, size_t num_events,
                               void * param);


/* Returns the version of the Indium interface this library supports.  Compare
 * with In_HEADER_VERSION.
 */
In_PUBLIC uint32_t In_GetVersion(void);

/* Returns a human-readable name of this implementation of Indium, in the
 * following format:
 *    OS[/Subsystem][ (comment)]
 * where each part in []s is optional.
 */
In_PUBLIC const char * In_GetImplName(void);

/* Reference-counted initialization.  You must call In_Quit() the same number
 * of times that you call this, regardless of if it succeeds.  Returns 0/
 * nonzero on failure/success.
 */
In_PUBLIC int In_Init(void);

/* Reference-counted cleanup.  Call this function the same number of times you
 * call In_Init().
 */
In_PUBLIC void In_Quit(void);

/* Refreshes the list of devices.  Returns 0/nonzero on failure/success.
 */
In_PUBLIC int In_Refresh(void);

/* Returns the number of devices currently in the list.
 */
In_PUBLIC int In_GetNumDevices(void);

/* Returns information about the device with the given index.  Returns 0/
 * nonzero on failure/success.
 */
In_PUBLIC int In_GetDeviceInfo(int index, In_device_info * restrict info_out);

/* Returns the index of the device with the given name, or a negative number if
 * the named device isn't found.  Specify "In_KEYBOARD", "In_MOUSE", or
 * "In_JOYSTICK" to return the index of the first device found with that type
 * (or negative if there aren't any devices of that type).
 */
In_PUBLIC int In_FindDevice(const char * restrict name);

/* Opens the device with the given index.  Once open, you can get events for
 * the device.  Returns 0/nonzero on failure/success.
 */
In_PUBLIC int In_OpenDevice(int index);

/* Closes the device with the given index.
 */
In_PUBLIC void In_CloseDevice(int index);

/* Polls the device with the given index for events.  The device must be in the
 * open state.  Events are put in order into the dump queue (retrieve them with
 * Ds_Dequeue_In()).  Returns how many events were put into the queue
 * (including 0), or a negative number on failure.
 */
In_PUBLIC int In_PollDevice(int index, Ds_queue_In * restrict dump);

/* Same as In_PollDevice(), but calls a callback instead of using a queue.
 * Returns whatever the callback returns.
 */
In_PUBLIC int In_PollDeviceCallback(int index,
                                    In_callback_fn callback,
                                    void * restrict param);

/* Polls all currently open devices for events.  Events are put in order of
 * occurrence (not necessarily by device) into the dump queue (retrieve them
 * with Ds_Dequeue_In()).  Returns the total number of events put into the
 * queue (including 0), or a negative number on failure.
 */
In_PUBLIC int In_PollOpenDevices(Ds_queue_In * restrict dump);

/* Same as In_PollOpenDevices(), except calls a callback instead of inserting
 * the events into a queue.  Returns whatever the callback returns.
 */
In_PUBLIC int In_PollOpenDevicesCallback(In_callback_fn callback,
                                         void * restrict param);


#ifdef __cplusplus
}
#endif

#ifdef _In_DEFINED_RESTRICT
#undef _In_DEFINED_RESTRICT
#undef restrict
#endif

#endif

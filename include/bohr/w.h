/******************************************************************************
* Tungsten - a portable OpenGL window library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#ifndef __bohr_w_h__
#define __bohr_w_h__

#ifndef __STDC_CONSTANT_MACROS
#define __STDC_CONSTANT_MACROS
#endif

#include <stddef.h>
#include <stdint.h>


//Controls importing in Windows.
#ifndef W_PUBLIC
#  if(defined(_WIN32))
#     define W_PUBLIC __declspec(dllimport)
#  else
#     define W_PUBLIC
#  endif
#endif

//Nix non-critical C99 keywords in compilers that don't support them.
#if((!defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
 && !defined(restrict))
#  define restrict
#  define _W_DEFINED_RESTRICT
#endif


#ifdef __cplusplus
extern "C"
{
#endif


//The version of Tungsten this header comes from.
#define W_HEADER_VERSION UINT32_C(0x00010000) //v0.1.0(.0)

//The version of Tungsten you want when you include this file.
#ifndef W_VERSION
#define W_VERSION W_HEADER_VERSION
#endif


//Buffer size needed to hold monitor names.
#define W_NAME_SIZE 128

//Information about a particular monitor.
typedef struct W_monitor_info
{
   char monitor_name[W_NAME_SIZE]; //human-readable name
   char adapter_name[W_NAME_SIZE]; //name of the graphics card it's attached to
   int  primary;                   //whether it's the primary monitor
   int  windowable;                //whether it can handle any windowed modes

} W_monitor_info;

//A fullscreen display mode.
typedef struct W_mode
{
   int width;   //pixels wide
   int height;  //pixels high
   int bpp;     //bits per pixel
   int refresh; //monitor frequency in Hz, or 0 for any

} W_mode;

//An OpenGL pixel format.
typedef struct W_format
{
   //TODO: order of color buffer components?
   int red_bits;      //bits in color buffer for red
   int green_bits;    //bits for green
   int blue_bits;     //bits for blue
   int alpha_bits;    //extra bits in color buffer for alpha channel, or 0
   int depth_bits;    //bits in the depth buffer, or 0 for no depth buffer
   int accum_bits;    //bits in the accumulation buffer, or 0
   int stencil_bits;  //bits in the stencil buffer, or 0 for no stencil buffer
   int double_buffer; //whether it's double-buffered
   int aux_buffers;   //how many auxiliary buffers
   //TODO: support for palettes?

} W_format;

//A Tungsten window.
#ifndef _W_WINDOW_DEFINED
#define _W_WINDOW_DEFINED
typedef void * W_window;
#endif

//Action for W_events, defining what actually happened.
typedef enum W_action
{
   W_CREATE = 1, //on window creation
   W_PAINT,      //the window needs (re-)drawing
   W_RESIZE,     //the window is being resized window resize
   W_ACTIVATE,   //the window focus is changing
   W_CLOSE,      //on window close
   W_SCREENSAVER //on screensaver trying to start

} W_action;

//Params for the W_CREATE action.
typedef struct W_create_params
{
   int      fullscreen; //whether the monitor went fullscreen
   W_mode   mode;       //the mode used
   W_format format;     //the format used

} W_create_params;

//Params for the W_RESIZE action.
typedef struct W_resize_params
{
   int width;  //new width
   int height; //new height

} W_resize_params;

//Params for the W_ACTIVATE action.
typedef struct W_activate_params
{
   int focus;  //nonzero if our window is in foreground, 0 else
   int hidden; //nonzero if minimized/iconified, etc, 0 if regular

} W_activate_params;

//Function to call on events from Tungsten.  Declare as:
//   int MyCallback(W_action action, void * restrict action_params,
//                  void * restrict app_param);
typedef int (* W_callback_fn)(W_action action, void * action_params,
                              void * app_param);

//A thread's OpenGL context.
#ifndef _W_GL_CONTEXT_DEFINED
#define _W_GL_CONTEXT_DEFINED
typedef void * W_gl_context;
#endif


/* Returns the version of the Tungsten interface this library supports.
 * Compare with W_HEADER_VERSION.
 */
W_PUBLIC uint32_t W_GetVersion(void);

/* Returns a human-readable name of this implementation of Tungsten, in the
 * following format:
 *    OS[/Subsystem][ (comment)]
 * where each part in []s is optional.
 */
W_PUBLIC const char * W_GetImplName(void);

/* Reference-counted initialization.  You must call W_Quit() the same number of
 * times that you call this, regardless of if it succeeds.  Returns 0/nonzero
 * on failure/success.
 */
W_PUBLIC int W_Init(void);

/* Reference-counted cleanup.  Call this function the same number of times you
 * call W_Init().
 */
W_PUBLIC void W_Quit(void);

/* Returns the number of monitors attached to the system.  Returns 0 on
 * failure.
 */
W_PUBLIC int W_GetNumMonitors(void);

/* Returns information about the monitor with the given index.  Returns 0/
 * nonzero on failure/success.
 */
W_PUBLIC int W_GetMonitorInfo(int monitor,
                              W_monitor_info * restrict info_out);

/* Returns the number of modes available for the monitor with the given index.
 * Returns 0 on failure.
 */
W_PUBLIC int W_GetNumModes(int monitor);

/* Returns the mode with the given index for the monitor with the given index.
 * Returns 0/nonzero on failure/success.
 */
W_PUBLIC int W_GetMode(int monitor,
                       int mode_index, W_mode * restrict mode_out);

/* Refreshes the list of monitors/modes.  After you call this function, you'll
 * need to do your monitor/mode enumeration over again.  Returns 0/nonzero on
 * failure/success.
 */
W_PUBLIC int W_RefreshMonitors(void);

/* Creates a new Tungsten window, which will run in its own thread, on the
 * given monitor, with all the given whatever.  If fullscreen, the only part of
 * mode that matters are the width and height.  Use NULL for mode or format for
 * some sensible defaults.
 */
W_PUBLIC W_window W_CreateWindow(int monitor,
                                 int fullscreen,
                                 W_mode * restrict mode,
                                 W_format * restrict format,
                                 const char * restrict title,
                                 const char * restrict icon_file,
                                 int hide_cursor,
                                 W_callback_fn notify,
                                 void * restrict notify_app_param);

/* Closes and frees resources associated with the window.  Note that the window
 * can already have been closed, such as through user interaction; in that case
 * this still must be called to free all resources.  Note that you MUST NOT
 * call this function from within the callback procedure for your window, or
 * the thread will deadlock and nothing will be freed.  Instead, call it from
 * anywhere else, such as from the thread that called W_CreateWindow().
 */
W_PUBLIC void W_FreeWindow(W_window restrict win);

//TODO: need some way of switching modes in existing windows

/* Fills id_out with the platform-specific window handle/id for the given
 * W_window.  For Windows, this is an HWND: id_out must point to an HWND and
 * size_id_out must be sizeof(HWND).  I haven't figured out other platforms
 * yet.  This is mostly for other platform-specific Bohr modules, not really
 * for applications to use.  Returns 0/nonzero on failure/success.
 */
W_PUBLIC int W_GetPlatformWindowId(W_window restrict win,
                                   size_t size_id_out, void * restrict id_out);

/* Posts an event to the given window.  The action must be either W_PAINT or
 * W_CLOSE, and action_params is ignored--for now.  In the future, you may be
 * able to resize the window with this function too.  Returns 0/nonzero on
 * failure/success (success being that the event was successfully posted).  The
 * event will be processed in another thread at a slightly later time.
 */
W_PUBLIC int W_PostEvent(W_window restrict win,
                         W_action action, const void * restrict action_params);

/* Creates, sets for the current thread, and returns a GL rendering context.
 * You must call this function from every thread that will be calling any
 * OpenGL functions, before it calls any OpenGL functions.  Each thread can be
 * attached to only one window at a time; I assume that multiple threads can be
 * attached to the same window at the same time.  Returns NULL on failure.
 */
W_PUBLIC W_gl_context W_AttachGl(W_window restrict win);

/* Detaches the current thread from the given W_gl_context, and frees it.
 */
W_PUBLIC void W_DetachGl(W_gl_context restrict ctx);

/* For double-buffered window formats, flips the back buffer (which all OpenGL
 * calls render to) with the front buffer.  You should flush any OpenGL calls
 * to the same window in other threads before calling this.  Note that you must
 * only call this for double-buffered formats--calling it on a non-double-
 * buffered format has undefined results.
 */
W_PUBLIC int W_Flip(W_window restrict win);


#ifdef __cplusplus
}
#endif

#ifdef _W_DEFINED_RESTRICT
#undef _W_DEFINED_RESTRICT
#undef restrict
#endif

#endif

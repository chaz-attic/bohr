/******************************************************************************
* Polonium - a portability library
* Po_lib - a portable library loader interface
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#ifndef __bohr_po_lib_h__
#define __bohr_po_lib_h__

#include <stddef.h>
#ifdef _WIN32
#  include <windows.h>
#else
#  include <dlfcn.h>
#endif


//Controls inlining of this library's functions.
#ifndef Po_LIB_INLINE
#  ifdef Po_INLINE
#     define Po_LIB_INLINE Po_INLINE
#  else
#     define Po_LIB_INLINE inline static
#  endif
#endif

//Nix non-critical C99 keywords in compilers that don't support them.
#if((!defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
 && !defined(restrict))
#  define restrict
#  define _Po_LIB_DEFINED_RESTRICT
#endif


//A library object.
typedef void * Po_lib;


/* Opens a library.  Returns a Po_lib object that can be specified in calls to
 * Po_LoadSymbols() or Po_CloseLibrary(), or NULL on failure.
 */
Po_LIB_INLINE Po_lib Po_OpenLibrary(const char * restrict filename)
{
#ifdef _WIN32
#  ifdef UNICODE
   HMODULE lib = NULL;
   WCHAR w_filename[MAX_PATH];

   if(MultiByteToWideChar(CP_UTF8, 0, filename, -1, w_filename, MAX_PATH))
   {
      w_filename[MAX_PATH-1] = L'\0';
      lib = LoadLibrary(w_filename);
   }

   return (Po_lib)lib;
#  else
   return (Po_lib)LoadLibrary(filename);
#  endif
#else
   return (Po_lib)dlopen(filename, RTLD_LAZY | RTLD_GLOBAL);
#endif
}

/* Closes the library opened with Po_OpenLibrary().
 */
Po_LIB_INLINE void Po_CloseLibrary(Po_lib lib)
{
#ifdef _WIN32
   FreeLibrary((HMODULE)lib);
#else
   dlclose((void *)lib);
#endif
}

/* Loads one or more symbols from the library opened with Po_OpenLibrary() into
 * memory.  Returns how many symbols it loaded, which will equal num if loading
 * every symbol succeeded.
 */
Po_LIB_INLINE int Po_LoadSymbols(Po_lib lib, void * restrict syms_out[],
                                 const char * restrict names[], int num)
{
   int loaded = 0;

   for(int i = 0; i < num; ++i)
   {
#ifdef _WIN32
      if((syms_out[i] = (void *)GetProcAddress((HMODULE)lib, names[i])) != NULL)
#else
      if((syms_out[i] = (void *)dlsym((void *)lib, names[i])) != NULL)
#endif
         ++loaded;
   }

   return loaded;
}


#ifdef _Po_LIB_DEFINED_RESTRICT
#undef _Po_LIB_DEFINED_RESTRICT
#undef restrict
#endif

#endif

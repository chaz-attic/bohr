/******************************************************************************
* Indium - a portable low-level input library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#include "internal.h"
#include <bohr/in.h>
#include <bohr/po_time.h>

#include <stddef.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>
#include <sys/time.h>


static Po_time_t g_freq_ratio = Po_TIME_C(0); //ratio of Po:system timers
static Po_time_t g_start_adj  = Po_TIME_C(0); //0 system time, in Po time


/* Sets up our time globals.  Returns 0/nonzero on failure/success.
 */
In_PRIVATE int InitPollTimers(void)
{
   Po_time_t freq = Po_GetFrequency();
   if(!freq)
      return 0;

   g_freq_ratio = (Po_time_t)((double)freq / 1000000.0 + 0.5);

   struct timeval tv;
   g_start_adj = Po_GetTime();
   gettimeofday(&tv, NULL);
   Po_time_t sys_now = (Po_time_t)tv.tv_sec*1000000 + tv.tv_usec;

   g_start_adj -= (g_freq_ratio * sys_now);

   return 1;
}

/* Polls the device for events that have happened, and saves them in the
 * device's buffer.  Returns 0/nonzero on failure/success.
 */
In_PRIVATE int GatherEvents(device * restrict dev)
{
   int n = read(dev->fd, dev->buffer, sizeof(dev->buffer));
   dev->buf_events = (n == -1 ? 0 : n / sizeof(struct input_event));

   if(n == -1 && errno != EAGAIN)
      return 0;

   return 1;
}

/* Translates a struct input_event to an In_event.  Returns 0/nonzero depending
 * on whether the evdev_event is one we don't care about/one we do care about.
 */
static inline int Translate(const device * restrict dev,
                            const struct input_event * restrict evt,
                            In_event * restrict out)
{
   out->from = dev->info.index;

   switch(dev->info.type)
   {
   case In_KEYBOARD:

      //Ignore, respectively, anything that's not a keypress, key repeat
      //events, and anything that's out of range.
      if(evt->type != EV_KEY
      || evt->value == 2
      || evt->code < dev->button_map_start || evt->code > dev->button_map_end)
         return 0;

      out->action = dev->button_map[evt->code - dev->button_map_start];
      out->down = (evt->value != 0);
      break;

   case In_MOUSE:
      switch(evt->type)
      {
      case EV_KEY:
         if(evt->code < dev->button_map_start
         || evt->code > dev->button_map_end)
            return 0;

         out->action = dev->button_map[evt->code - dev->button_map_start];
         out->down = (evt->value != 0);
         break;

      case EV_REL:
         switch(evt->code)
         {
         case REL_X:
            out->action = In_MOUSE_AXIS_X;
            out->change = evt->value;
            break;

         case REL_Y:
            out->action = In_MOUSE_AXIS_Y;
            out->change = evt->value;
            break;

         case REL_WHEEL:
            out->action = In_MOUSE_AXIS_Z;
            out->change = evt->value;
            break;

         default:
            return 0;
         }
         break;

      default:
         return 0;
      }
      break;

   case In_JOYSTICK:
      switch(evt->type)
      {
      case EV_KEY:
         if(evt->code < dev->button_map_start
         || evt->code > dev->button_map_end)
            return 0;

         out->action = dev->button_map[evt->code - dev->button_map_start];
         out->down = (evt->value != 0);
         break;

      case EV_ABS:
         if(evt->code < dev->axis_map_start
         || evt->code > dev->axis_map_end)
            return 0;

         out->action = dev->axis_map[evt->code - dev->axis_map_start];
         //TODO: the range should be [In_JOY_AXIS_MIN,In_JOY_AXIS_MAX], not
         //just whatever evdev reports
         out->value = evt->value;
         break;

      default:
         return 0;
      }
      break;
   }

   out->time = g_start_adj
             + (g_freq_ratio
                * ((Po_time_t)evt->time.tv_sec*1000000 + evt->time.tv_usec));

   return 1;
}

/* Directly sends the events to the receiver, returning whatever it returns.
 */
static inline int Send(const receiver * restrict receiver,
                       In_event * restrict events, size_t num_events)
{
   if(receiver->is_callback)
      return receiver->callback(events, num_events, receiver->param);
   return Ds_Enqueue_In(receiver->dump, events, num_events);
}

/* Takes polled events waiting in the device's buffer and sends them to the
 * receiver.  If the receiver is a queue, returns how many events were enqueued
 * (0 on error or no events); if a callback, returns whatever the callback
 * returns.
 */
In_PRIVATE int SendEvents(device * restrict dev,
                          const receiver * restrict receiver)
{
   assert(dev->buf_events > 0);

   int real_events = 0;
   In_event events[dev->buf_events];

   for(int i = 0; i < dev->buf_events; ++i)
   {
      if(Translate(dev, &dev->buffer[i], &events[real_events]))
         ++real_events;
   }

   int sent = Send(receiver, events, real_events);
   dev->buf_events = 0;
   return sent;
}

static inline int CompareTimevals(const struct timeval * restrict tv1,
                                  const struct timeval * restrict tv2)
{
   if(tv1->tv_sec != tv2->tv_sec)
      return tv1->tv_sec - tv2->tv_sec;

   return tv1->tv_usec - tv2->tv_usec;
}

/* Sends polled events from all the devices' buffers to the receiver.  If the
 * receiver is a queue, returns the total number of events put into the queue
 * (0 only on error, as you shouldn't call this function if there are no
 * events); if a callback, returns whatever the callback returns.  Requires a
 * little pre-processing to get the total number of events, and a list of
 * devices that have polled events waiting in their buffers.
 */
In_PRIVATE int SendEventsFromAll(int total_events,
                                 device * restrict devices[], int num_devices,
                                 const receiver * restrict receiver)
{
   assert(total_events > 0);
   assert(num_devices > 0);

   int cur[num_devices]; //a list of current buffer indices for each device
   memset(cur, 0, sizeof(cur));

   int real_events = 0;
   In_event events[total_events]; //events to output

   for(int evt = 0; evt < total_events; ++evt)
   {
      struct timeval min = {0};
      int which = -1;

      //find the device with the lowest timestamp comin' up next
      for(int i = 0; i < num_devices; ++i)
      {
         if(cur[i] < devices[i]->buf_events
         && (which < 0 ||
             CompareTimevals(&devices[i]->buffer[cur[i]].time, &min) < 0))
         {
            min = devices[i]->buffer[cur[i]].time;
            which = i;
         }
      }

      assert(which >= 0);

      if(Translate(devices[which], &devices[which]->buffer[cur[which]++],
                   &events[real_events]))
      {
         ++real_events;
      }
   }

   //set it so there's no more events
   for(int i = 0; i < num_devices; ++i)
   {
      assert(cur[i] == devices[i]->buf_events);
      devices[i]->buf_events = 0;
   }

   return Send(receiver, events, real_events);
}

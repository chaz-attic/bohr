/******************************************************************************
* Indium - a portable low-level input library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#include "internal.h"
#include <bohr/in.h>

#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <stropts.h>
#include <linux/input.h>


//Define a vector that holds our devices.
#define Ds_VECTOR_BEHAVIOR 2
#define Ds_VECTOR_TYPE device
#define Ds_VECTOR_SUFFIX _dev
#include <bohr/ds_vector.h>


//Used by evdev code to help with the bit arrays returned by its ioctl()s.
#define test_bit(array, bit) ((array)[(bit)/8] & (1<<((bit)%8)))


//A map of KEY_* constants (the index) to In_KEY_* constants (the value).
static In_action g_keyboard_map[] =
{
   /* KEY_ESC:1 */                  In_KEY_ESC,
   /* KEY_1:2 */                    In_KEY_1,
   /* KEY_2:3 */                    In_KEY_2,
   /* KEY_3:4 */                    In_KEY_3,
   /* KEY_4:5 */                    In_KEY_4,
   /* KEY_5:6 */                    In_KEY_5,
   /* KEY_6:7 */                    In_KEY_6,
   /* KEY_7:8 */                    In_KEY_7,
   /* KEY_8:9 */                    In_KEY_8,
   /* KEY_9:10 */                   In_KEY_9,
   /* KEY_0:11 */                   In_KEY_0,
   /* KEY_MINUS:12 */               In_KEY_MINUS,
   /* KEY_EQUAL:13 */               In_KEY_EQUALS,
   /* KEY_BACKSPACE:14 */           In_KEY_BACKSPACE,
   /* KEY_TAB:15 */                 In_KEY_TAB,
   /* KEY_Q:16 */                   In_KEY_Q,
   /* KEY_W:17 */                   In_KEY_W,
   /* KEY_E:18 */                   In_KEY_E,
   /* KEY_R:19 */                   In_KEY_R,
   /* KEY_T:20 */                   In_KEY_T,
   /* KEY_Y:21 */                   In_KEY_Y,
   /* KEY_U:22 */                   In_KEY_U,
   /* KEY_I:23 */                   In_KEY_I,
   /* KEY_O:24 */                   In_KEY_O,
   /* KEY_P:25 */                   In_KEY_P,
   /* KEY_LEFTBRACE:26 */           In_KEY_LBRACKET,
   /* KEY_RIGHTBRACE:27 */          In_KEY_RBRACKET,
   /* KEY_ENTER:28 */               In_KEY_ENTER,
   /* KEY_LEFTCTRL:29 */            In_KEY_LCTRL,
   /* KEY_A:30 */                   In_KEY_A,
   /* KEY_S:31 */                   In_KEY_S,
   /* KEY_D:32 */                   In_KEY_D,
   /* KEY_F:33 */                   In_KEY_F,
   /* KEY_G:34 */                   In_KEY_G,
   /* KEY_H:35 */                   In_KEY_H,
   /* KEY_J:36 */                   In_KEY_J,
   /* KEY_K:37 */                   In_KEY_K,
   /* KEY_L:38 */                   In_KEY_L,
   /* KEY_SEMICOLON:39 */           In_KEY_SEMICOLON,
   /* KEY_APOSTROPHE:40 */          In_KEY_QUOTE,
   /* KEY_GRAVE:41 */               In_KEY_BACKQUOTE,
   /* KEY_LEFTSHIFT:42 */           In_KEY_LSHIFT,
   /* KEY_BACKSLASH:43 */           In_KEY_BACKSLASH,
   /* KEY_Z:44 */                   In_KEY_Z,
   /* KEY_X:45 */                   In_KEY_X,
   /* KEY_C:46 */                   In_KEY_C,
   /* KEY_V:47 */                   In_KEY_V,
   /* KEY_B:48 */                   In_KEY_B,
   /* KEY_N:49 */                   In_KEY_N,
   /* KEY_M:50 */                   In_KEY_M,
   /* KEY_COMMA:51 */               In_KEY_COMMA,
   /* KEY_DOT:52 */                 In_KEY_PERIOD,
   /* KEY_SLASH:53 */               In_KEY_SLASH,
   /* KEY_RIGHTSHIFT:54 */          In_KEY_RSHIFT,
   /* KEY_KPASTERISK:55 */          In_KEY_PAD_STAR,
   /* KEY_LEFTALT:56 */             In_KEY_LALT,
   /* KEY_SPACE:57 */               In_KEY_SPACE,
   /* KEY_CAPSLOCK:58 */            In_KEY_CAPSLOCK,
   /* KEY_F1:59 */                  In_KEY_F1,
   /* KEY_F2:60 */                  In_KEY_F2,
   /* KEY_F3:61 */                  In_KEY_F3,
   /* KEY_F4:62 */                  In_KEY_F4,
   /* KEY_F5:63 */                  In_KEY_F5,
   /* KEY_F6:64 */                  In_KEY_F6,
   /* KEY_F7:65 */                  In_KEY_F7,
   /* KEY_F8:66 */                  In_KEY_F8,
   /* KEY_F9:67 */                  In_KEY_F9,
   /* KEY_F10:68 */                 In_KEY_F10,
   /* KEY_NUMLOCK:69 */             In_KEY_NUMLOCK,
   /* KEY_SCROLLLOCK:70 */          In_KEY_SCRLOCK,
   /* KEY_KP7:71 */                 In_KEY_PAD_7,
   /* KEY_KP8:72 */                 In_KEY_PAD_8,
   /* KEY_KP9:73 */                 In_KEY_PAD_9,
   /* KEY_KPMINUS:74 */             In_KEY_PAD_MINUS,
   /* KEY_KP4:75 */                 In_KEY_PAD_4,
   /* KEY_KP5:76 */                 In_KEY_PAD_5,
   /* KEY_KP6:77 */                 In_KEY_PAD_6,
   /* KEY_KPPLUS:78 */              In_KEY_PAD_PLUS,
   /* KEY_KP1:79 */                 In_KEY_PAD_1,
   /* KEY_KP2:80 */                 In_KEY_PAD_2,
   /* KEY_KP3:81 */                 In_KEY_PAD_3,
   /* KEY_KP0:82 */                 In_KEY_PAD_0,
   /* KEY_KPDOT:83 */               In_KEY_PAD_PERIOD,
   /* (undefined):84 */             In_UNKNOWN_ACTION,
   /* KEY_ZENKAKUHANKAKU:85 */      In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_102ND:86 */               In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_F11:87 */                 In_KEY_F11,
   /* KEY_F12:88 */                 In_KEY_F12,
   /* KEY_RO:89 */                  In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_KATAKANA:90 */            In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_HIRAGANA:91 */            In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_HENKAN:92 */              In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_KATAKANAHIRAGANA:93 */    In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_MUHENKAN:94 */            In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_KPJPCOMMA:95 */           In_KEY_PAD_COMMA,  //FIXME: is this right?
   /* KEY_KPENTER:96 */             In_KEY_PAD_ENTER,
   /* KEY_RIGHTCTRL:97 */           In_KEY_RCTRL,
   /* KEY_KPSLASH:98 */             In_KEY_PAD_SLASH,
   /* KEY_SYSRQ:99 */               In_KEY_SYSRQ,
   /* KEY_RIGHTALT:100 */           In_KEY_RALT,
   /* KEY_LINEFEED:101 */           In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_HOME:102 */               In_KEY_HOME,
   /* KEY_UP:103 */                 In_KEY_UP,
   /* KEY_PAGEUP:104 */             In_KEY_PGUP,
   /* KEY_LEFT:105 */               In_KEY_LEFT,
   /* KEY_RIGHT:106 */              In_KEY_RIGHT,
   /* KEY_END:107 */                In_KEY_END,
   /* KEY_DOWN:108 */               In_KEY_DOWN,
   /* KEY_PAGEDOWN:109 */           In_KEY_PGDN,
   /* KEY_INSERT:110 */             In_KEY_INS,
   /* KEY_DELETE:111 */             In_KEY_DEL,
   /* KEY_MACRO:112 */              In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_MUTE:113 */               In_KEY_MEDIA_MUTE,
   /* KEY_VOLUMEDOWN:114 */         In_KEY_MEDIA_VOLDN,
   /* KEY_VOLUMEUP:115 */           In_KEY_MEDIA_VOLUP,
   /* KEY_POWER:116 */              In_UNKNOWN_ACTION, //In_KEY_SYS_POWER,
   /* KEY_KPEQUAL:117 */            In_KEY_PAD_EQUALS,
   /* KEY_KPPLUSMINUS:118 */        In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_PAUSE:119 */              In_KEY_BREAK,
   /* (undefined):120 */            In_UNKNOWN_ACTION,
   /* KEY_KPCOMMA:121 */            In_KEY_PAD_COMMA,
   /* KEY_HANGEUL:122 */            In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_HANJA:123 */              In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_YEN:124 */                In_UNKNOWN_ACTION, //FIXME: what?
   /* KEY_LEFTMETA:125 */           In_KEY_LMETA,
   /* KEY_RIGHTMETA:126 */          In_KEY_RMETA,
   /* KEY_COMPOSE:127 */            In_KEY_COMPOSE,

   //TODO: finish me

   //* KEY_STOP:128 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_AGAIN:129 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_PROPS:130 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_UNDO:131 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_FRONT:132 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_COPY:133 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_OPEN:134 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_PASTE:135 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_FIND:136 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_CUT:137 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_HELP:138 */In_UNKNOWN_ACTION, //In_KEY_APP_HELP,
   //* KEY_MENU:139 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_CALC:140 */In_UNKNOWN_ACTION, //In_KEY_APP_CALC,
   //* KEY_SETUP:141 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_SLEEP:142 */In_UNKNOWN_ACTION, //In_KEY_SYS_SLEEP,
   //* KEY_WAKEUP:143 */In_UNKNOWN_ACTION, //In_KEY_SYS_WAKE,
   //* KEY_FILE:144 */In_UNKNOWN_ACTION, //In_KEY_APP_FILEBROWSER,
   //* KEY_SENDFILE:145 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_DELETEFILE:146 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_XFER:147 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_PROG1:148 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_PROG2:149 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_WWW:150 */In_UNKNOWN_ACTION, //In_KEY_APP_BROWSER,
   //* KEY_MSDOS:151 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_COFFEE:152 */In_UNKNOWN_ACTION, //In_KEY_APP_LOCK,
   //* KEY_DIRECTION:153 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_CYCLEWINDOWS:154 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_MAIL:155 */In_UNKNOWN_ACTION, //In_KEY_APP_MAIL,
   //* KEY_BOOKMARKS:156 */In_UNKNOWN_ACTION, //In_KEY_WEB_BOOKMARKS,
   //* KEY_COMPUTER:157 */In_UNKNOWN_ACTION, //FIXME: what?
   //* KEY_BACK:158 */In_UNKNOWN_ACTION, //In_KEY_WEB_BACK,
   //* KEY_FORWARD:159 */In_UNKNOWN_ACTION, //In_KEY_WEB_FORWARD

   /* KEY_CLOSECD:160 */
   /* KEY_EJECTCD:161 */
   /* KEY_EJECTCLOSECD:162 */
   /* KEY_NEXTSONG:163 */
   /* KEY_PLAYPAUSE:164 */
   /* KEY_PREVIOUSSONG:165 */
   /* KEY_STOPCD:166 */
   /* KEY_RECORD:167 */
   /* KEY_REWIND:168 */
   /* KEY_PHONE:169 */
   /* KEY_ISO:170 */
   /* KEY_CONFIG:171 */
   /* KEY_HOMEPAGE:172 */
   /* KEY_REFRESH:173 */
   /* KEY_EXIT:174 */
   /* KEY_MOVE:175 */
   /* KEY_EDIT:176 */
   /* KEY_SCROLLUP:177 */
   /* KEY_SCROLLDOWN:178 */
   /* KEY_KPLEFTPAREN:179 */
   /* KEY_KPRIGHTPAREN:180 */
   /* KEY_NEW:181 */
   /* KEY_REDO:182 */
   /* KEY_F13:183 */
   /* KEY_F14:184 */
   /* KEY_F15:185 */
   /* KEY_F16:186 */
   /* KEY_F17:187 */
   /* KEY_F18:188 */
   /* KEY_F19:189 */
   /* KEY_F20:190 */
   /* KEY_F21:191 */
   /* KEY_F22:192 */
   /* KEY_F23:193 */
   /* KEY_F24:194 */
   /* (undefined):195 */
   /* (undefined):196 */
   /* (undefined):197 */
   /* (undefined):198 */
   /* (undefined):199 */
   /* KEY_PLAYCD:200 */
   /* KEY_PAUSECD:201 */
   /* KEY_PROG3:202 */
   /* KEY_PROG4:203 */
   /* (undefined):204 */
   /* KEY_SUSPEND:205 */
   /* KEY_CLOSE:206 */
   /* KEY_PLAY:207 */
   /* KEY_FASTFORWARD:208 */
   /* KEY_BASSBOOST:209 */
   /* KEY_PRINT:210 */
   /* KEY_HP:211 */
   /* KEY_CAMERA:212 */
   /* KEY_SOUND:213 */
   /* KEY_QUESTION:214 */
   /* KEY_EMAIL:215 */
   /* KEY_CHAT:216 */
   /* KEY_SEARCH:217 */
   /* KEY_CONNECT:218 */
   /* KEY_FINANCE:219 */
   /* KEY_SPORT:220 */
   /* KEY_SHOP:221 */
   /* KEY_ALTERASE:222 */
   /* KEY_CANCEL:223 */
   /* KEY_BRIGHTNESSDOWN:224 */
   /* KEY_BRIGHTNESSUP:225 */
   /* KEY_MEDIA:226 */
   /* KEY_SWITCHVIDEOMODE:227 */
   /* KEY_KBDILLUMTOGGLE:228 */
   /* KEY_KBDILLUMDOWN:229 */
   /* KEY_KBDILLUMUP:230 */
   /* KEY_SEND:231 */
   /* KEY_REPLY:232 */
   /* KEY_FORWARDMAIL:233 */
   /* KEY_SAVE:234 */
   /* KEY_DOCUMENTS:235 */
   /* KEY_BATTERY:236 */
   /* KEY_BLUETOOTH:237 */
   /* KEY_WLAN:238 */
   /* (undefined):239 */
   /* KEY_UNKNOWN:240 */
   /* KEY_VIDEO_NEXT:241 */
   /* KEY_VIDEO_PREV:242 */
   /* KEY_BRIGHTNESS_CYCLE:243 */
   /* KEY_BRIGHTNESS_ZERO:244 */
   /* KEY_DISPLAY_OFF:245 */

   //I only bothered to copy this from linux/input.h for at most 256 values...
   //if this gets serious, there are technically up to 512 key constants, so
   //good fucking luck.
};

//The value at gc_keyboard_map[0].
static int g_keyboard_map_start = KEY_ESC;

//The number of entries in gc_keyboard_map.
static int g_keyboard_map_end =
                KEY_ESC + sizeof(g_keyboard_map)/sizeof(g_keyboard_map[0]) - 1;

In_PRIVATE device * g_devices = NULL;
In_PRIVATE int g_num_devices = 0;


static void FreeDevices(device * restrict devices, int num_devices)
{
   for(int i = 0; i < num_devices; ++i)
   {
      if(devices[i].fd >= 0)
         close(devices[i].fd);
      if(devices[i].button_map && devices[i].button_map != g_keyboard_map)
         free(devices[i].button_map);
      if(devices[i].axis_map)
         free(devices[i].axis_map);
   }
}

static int GetDeviceInfo(const char * restrict filename, int fd,
                         device * restrict dev)
{
   unsigned char keycaps[(KEY_MAX / 8) + 1];

   if(ioctl(fd, EVIOCGBIT(EV_KEY, sizeof(keycaps)), keycaps) == -1)
   {
      fprintf(stderr, "ioctl(%s, EVIOCGBIT(EV_KEY)): %s\n", filename,
              strerror(errno));
      return 0;
   }

   if(test_bit(keycaps, BTN_MOUSE))
   {
      //PROBABLY a mouse...
      //TODO: support BTN_TOUCH (which has absolute axes), maybe BTN_DIGI?

      unsigned char relcaps[(REL_MAX / 8) + 1];

      if(ioctl(fd, EVIOCGBIT(EV_REL, sizeof(relcaps)), relcaps) == -1)
      {
         fprintf(stderr, "ioctl(%s, EVIOCGBIT(EV_REL)): %s\n", filename,
                 strerror(errno));
         return 0;
      }

      if(!test_bit(relcaps, REL_X) || !test_bit(relcaps, REL_Y))
      {
         fprintf(stderr, "%s has BTN_MOUSE, but lacks REL_X or REL_Y\n",
                 filename);
         return 0;
      }

      //8 is the number of buttons (apparently, anyway) that mice can have,
      //according to <linux/input.h>.  See BTN_MOUSE and the subsequent BTN_*
      //codes in that file.
      if(!(dev->button_map = (In_action *)malloc(8 * sizeof(In_action))))
      {
         perror("malloc()");
         return 0;
      }

      //the buttons are numbered BTN_LEFT through BTN_TASK
      dev->button_map_start = BTN_LEFT;
      dev->button_map_end = BTN_TASK;

      //we always report that the first three buttons are there, because we
      //want buttons 0, 1, and 2 to ALWAYS represent left, right, and middle
      for(int i = 0; i < 3; ++i)
         dev->button_map[i] = In_MOUSE_BUTTON(i);

      //Now, I'm not sure exactly how evdev reports other mouse buttons, so to
      //prevent us exposing mice that support, say, In_BUTTON_4 and
      //In_BUTTON_6, but skipping In_BUTTON_5, we keep the buttons sequential.
      int button_count = 3;
      for(int i = 3; i < 9; ++i)
      {
         if(test_bit(keycaps, i + BTN_LEFT))
            dev->button_map[i] = In_MOUSE_BUTTON(button_count++);
         else
            dev->button_map[i] = In_UNKNOWN_ACTION;
      }

      //provide at least x and y, maybe z axes
      int axis_count = 2;
      if(test_bit(relcaps, REL_WHEEL))
         ++axis_count;

      //(mouse devices don't need axis maps)

      dev->info.type = In_MOUSE;
      dev->info.buttons = button_count;
      dev->info.axes = axis_count;
      dev->info.hats = 0;
   }
   else if(test_bit(keycaps, BTN_JOYSTICK)
   || test_bit(keycaps, BTN_GAMEPAD))
   {
      //PROBABLY a joystick...
      //TODO: support BTN_WHEEL

      unsigned char abscaps[(ABS_MAX / 8) + 1];

      if(ioctl(fd, EVIOCGBIT(EV_ABS, sizeof(abscaps)), abscaps) == -1)
      {
         fprintf(stderr, "ioctl(%s, EVIOCGBIT(EV_ABS)): %s\n", filename,
                 strerror(errno));
         return 0;
      }

      //The number of values (inclusive) between BTN_TRIGGER (the first
      //BTN_JOYSTICK event) and BTN_THUMBR (the last BTN_GAMEPAD event) is 31;
      //the number of ABS_* values we care about (for now) is 6.
      if(!(dev->button_map = (In_action *)malloc(31 * sizeof(In_action)))
      || !(dev->axis_map = (In_action *)malloc(6 * sizeof(In_action))))
      {
         perror("malloc()");
         if(dev->button_map)
            free(dev->button_map);
         return 0;
      }

      //the buttons are numbered BTN_TRIGGER through BTN_THUMBR
      dev->button_map_start = BTN_TRIGGER;
      dev->button_map_end = BTN_THUMBR;

      //calculate whether each button is supported, such that they're all
      //reported sequentially
      int button_count = 0;
      for(int i = 0; i < 31; ++i)
      {
         if(test_bit(keycaps, i + BTN_TRIGGER))
            dev->button_map[i] = In_JOY_BUTTON(button_count++);
         else
            dev->button_map[i] = In_UNKNOWN_ACTION;
      }

      //the axes are numbered ABS_X through ABS_RZ
      dev->axis_map_start = ABS_X;
      dev->axis_map_end = ABS_RZ;

      //figure out which of x, y, and z axes we support
      int axis_count = 0;
      for(int i = 0; i < 6; ++i)
      {
         if(test_bit(abscaps, i + ABS_X))
         {
            //FIXME: this calculation might not work if <bohr/in.h> changes
            dev->axis_map[i] = In_JOY_AXIS_X + i;
            ++axis_count;
         }
         else
            dev->axis_map[i] = In_UNKNOWN_ACTION;
      }

      //TODO: use the EVIOCGABS ioctl to determine axis ranges, store them
      //(or calculations based on them) somewhere in the device struct

      //TODO: support hats... which would require rethinking my max 6 axes

      dev->info.type = In_JOYSTICK;
      dev->info.buttons = button_count;
      dev->info.axes = axis_count;
      dev->info.hats = 0;
   }
   else if(test_bit(keycaps, KEY_SPACE)
   || test_bit(keycaps, KEY_KPENTER))
   {
      //PROBABLY a keyboard... the test for keypad enter is for USB keypads and
      //the like.  I GUESS that works...

      dev->button_map = g_keyboard_map;
      dev->button_map_start = g_keyboard_map_start;
      dev->button_map_end = g_keyboard_map_end;

      dev->info.type = In_KEYBOARD;
      dev->info.buttons = 256; //why not?
      dev->info.axes = 0;
      dev->info.hats = 0;
   }
   else
      return 0;

   if(ioctl(fd, EVIOCGNAME(sizeof(dev->info.name)), dev->info.name) == -1)
   {
      fprintf(stderr, "ioctl(%s, EVIOCGNAME): %s\n",
              filename, strerror(errno));
      snprintf(dev->info.name, In_NAME_SIZE, "Unknown Device");
      dev->info.name[In_NAME_SIZE-1] = '\0';
   }

   return 1;
}

static int AddIfDevice(const char * restrict filename,
                       Ds_vector_dev * restrict devs)
{
   device dev;
   memset(&dev, 0, sizeof(dev));

   if((dev.fd = open(filename, O_RDONLY | O_NONBLOCK)) < 0)
   {
      perror(filename);
      return 1;
   }

   dev.info.index = devs->num;
   if(!GetDeviceInfo(filename, dev.fd, &dev))
   {
      close(dev.fd);
      return 1;
   }

   if(!Ds_InsertVectorItems_dev(devs, &dev, 1, Ds_VECTOR_END))
   {
      perror("realloc()");
      close(dev.fd);
      return 0;
   }

   return 1;
}

static int FindAllDevices(Ds_vector_dev * restrict devs)
{
   DIR * dir;
   struct dirent * file;

   //evdev uses devices at /dev/input/event*

   if(!(dir = opendir("/dev/input")))
   {
      perror("/dev/input");
      return 0;
   }

   while((file = readdir(dir)) != NULL)
   {
      if(file->d_name[0] == 'e'
      && file->d_name[1] == 'v'
      && file->d_name[2] == 'e'
      && file->d_name[3] == 'n'
      && file->d_name[4] == 't')
      {
         char filename[NAME_MAX+1];
         snprintf(filename, NAME_MAX, "/dev/input/%s", file->d_name);
         filename[NAME_MAX] = '\0';

         if(!AddIfDevice(filename, devs))
            continue;
      }
   }

   closedir(dir);
   return 1;
}

In_PRIVATE int BuildDeviceList()
{
   Ds_vector_dev devs = Ds_VECTOR_INIT;

   int success = 0;
   do
   {
      if(!Ds_InitVector_dev(&devs, 16))
      {
         perror("malloc()");
         break;
      }

      if(!FindAllDevices(&devs))
         break;

      if(devs.num && !Ds_ResizeVector_dev(&devs, devs.num))
         break;

      g_devices = devs.buf;
      g_num_devices = devs.num;

      success = 1;
   } while(0);

   if(!success)
   {
      FreeDevices(devs.buf, devs.num);
      Ds_FreeVector_dev(&devs);
   }

   return success;
}

In_PRIVATE void FreeDeviceList()
{
   if(g_devices)
   {
      FreeDevices(g_devices, g_num_devices);
      free(g_devices);
      g_devices = NULL;
   }
   g_num_devices = 0;
}

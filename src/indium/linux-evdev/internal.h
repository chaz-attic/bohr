/******************************************************************************
* Indium - a portable low-level input library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#ifndef __internal_h__
#define __internal_h__

#include <linux/input.h>


//Controls export behavior.
#define In_PUBLIC __attribute__ ((visibility ("default")))
#define In_PRIVATE __attribute__ ((visibility ("hidden")))

//Nix non-critical C99 keywords in compilers that don't support them.
#if((!defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
 && !defined(restrict))
#  define restrict
#endif


#include <bohr/in.h>


//Maximum number of events we can gather for a device from evdev at once.
#define EVDEV_BUFFER_SIZE 32


//Our internal structure for devices.
typedef struct device
{
   int fd; //the device file's descriptor

   struct input_event buffer[EVDEV_BUFFER_SIZE]; //buffer to hold input events
   int                buf_events;                //num gathered but not dumped

   In_action * button_map;       //any button mappings
   int         button_map_start; //evdev event code at button_map[0]
   int         button_map_end;   //ditto at [sizeof(button_map)-1]

   In_action * axis_map;       //any axis mappings
   int         axis_map_start; //evdev event code at axis_map[0]
   int         axis_map_end;   //ditto at [sizeof(axis_map)-1]
   //TODO: need some way of holding multipliers/offsets for each axis, so we
   //can force the range of absolute axes to [In_JOY_AXIS_MIN,In_JOY_AXIS_MAX]

   In_device_info info; //info about the device

} device;

//Defines how we pass In_events back to the caller.
typedef struct receiver
{
   int is_callback; //if nonzero, we use callback/param, else, dump

   union
   {
      Ds_queue_In * dump;

      struct
      {
         In_callback_fn   callback;
         void           * param;
      };
   };
} receiver;


//Look to devices.c for these:
int BuildDeviceList();
void FreeDeviceList();

//In poll.c:
int InitPollTimers(void);
int GatherEvents(device * restrict dev);
int SendEvents(device * restrict dev, const receiver * restrict receiver);
int SendEventsFromAll(int total_events,
                      device * restrict devices[], int num_devices,
                      const receiver * restrict receiver);


#endif

/******************************************************************************
* Indium - a portable low-level input library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#include "internal.h"
#include <bohr/in.h>
#include <bohr/po_time.h>

#include <stddef.h>
#include <string.h>
#include <assert.h>
#include <windows.h>
#include <dinput.h>


static Po_time_t g_freq_ratio = Po_TIME_C(0); //ratio of Po:win timers
static Po_time_t g_start_adj  = Po_TIME_C(0); //0 win time, in Po time


/* Sets up our time globals.  Returns 0/nonzero on failure/success.
 */
In_PRIVATE int InitPollTimers(void)
{
   Po_time_t freq = Po_GetFrequency();
   if(!freq)
      return 0;

   g_freq_ratio = (Po_time_t)((double)freq / 1000.0 + 0.5);

   DWORD win_now;
   timeBeginPeriod(1);
   g_start_adj = Po_GetTime();
   win_now = timeGetTime();
   timeEndPeriod(1);

   g_start_adj -= (g_freq_ratio * win_now);

   return 1;
}

/* Polls the device for events that have happened, and saves them in the
 * device's buffer.  Returns 0/nonzero on failure/success.
 */
In_PRIVATE int GatherEvents(device * restrict dev)
{
   int success = 0;

   do
   {
      if(FAILED(IDirectInputDevice8_Poll(dev->device)))
      {
         if(FAILED(IDirectInputDevice8_Acquire(dev->device)))
            break;
         if(FAILED(IDirectInputDevice8_Poll(dev->device)))
            break;
      }

      DWORD bufsize = DI_BUFFER_SIZE;
      if(FAILED(IDirectInputDevice8_GetDeviceData(dev->device,
                                                  sizeof(DIDEVICEOBJECTDATA),
                                                  dev->buffer,
                                                  &bufsize,
                                                  0)))
      {
         if(FAILED(IDirectInputDevice8_Acquire(dev->device)))
            break;
         bufsize = DI_BUFFER_SIZE;
         if(FAILED(IDirectInputDevice8_GetDeviceData(dev->device,
                                                    sizeof(DIDEVICEOBJECTDATA),
                                                     dev->buffer,
                                                     &bufsize,
                                                     0)))
         {
            break;
         }
      }

      dev->buf_events = (int)bufsize;

      success = 1;
   } while(0);

   return success;
}

/* Turns a dwOfs (and dwData) DirectInput variables to native Indium data.
 */
static inline void DiOfsToAction(DWORD dwOfs,
                                 DWORD dwData,
                                 In_device_type type,
                                 In_event * restrict evt_out)
{
   In_action a = In_UNKNOWN_ACTION;

   switch(type)
   {
   case In_KEYBOARD:
      switch(dwOfs)
      {
      case DIK_0:             a = In_KEY_0;                 break;
      case DIK_1:             a = In_KEY_1;                 break;
      case DIK_2:             a = In_KEY_2;                 break;
      case DIK_3:             a = In_KEY_3;                 break;
      case DIK_4:             a = In_KEY_4;                 break;
      case DIK_5:             a = In_KEY_5;                 break;
      case DIK_6:             a = In_KEY_6;                 break;
      case DIK_7:             a = In_KEY_7;                 break;
      case DIK_8:             a = In_KEY_8;                 break;
      case DIK_9:             a = In_KEY_9;                 break;
      case DIK_A:             a = In_KEY_A;                 break;
      //TODO: support DIK_ABNT_C1 and DIK_ABNT_C2?
      case DIK_ADD:           a = In_KEY_PAD_PLUS;          break;
      case DIK_APOSTROPHE:    a = In_KEY_QUOTE;             break;
      case DIK_APPS:          a = In_KEY_MENU;              break;
      //case DIK_AT:            a = In_KEY_PC98_AT;           break;
      //case DIK_AX:            a = In_KEY_J_AX;              break;
      case DIK_B:             a = In_KEY_B;                 break;
      case DIK_BACK:          a = In_KEY_BACKSPACE;         break;
      case DIK_BACKSLASH:     a = In_KEY_BACKSLASH;         break;
      case DIK_C:             a = In_KEY_C;                 break;
      //case DIK_CALCULATOR:    a = In_KEY_APP_CALC;          break;
      case DIK_CAPITAL:       a = In_KEY_CAPSLOCK;          break;
      //case DIK_COLON:         a = In_KEY_PC98_COLON;        break;
      case DIK_COMMA:         a = In_KEY_COMMA;             break;
      //case DIK_CONVERT:       a = In_KEY_J_CONVERT;         break;
      case DIK_D:             a = In_KEY_D;                 break;
      case DIK_DECIMAL:       a = In_KEY_PAD_PERIOD;        break;
      case DIK_DELETE:        a = In_KEY_DEL;               break;
      case DIK_DIVIDE:        a = In_KEY_PAD_SLASH;         break;
      case DIK_DOWN:          a = In_KEY_DOWN;              break;
      case DIK_E:             a = In_KEY_E;                 break;
      case DIK_END:           a = In_KEY_END;               break;
      case DIK_EQUALS:        a = In_KEY_EQUALS;            break;
      case DIK_ESCAPE:        a = In_KEY_ESC;               break;
      case DIK_F:             a = In_KEY_F;                 break;
      case DIK_F1:            a = In_KEY_F1;                break;
      case DIK_F2:            a = In_KEY_F2;                break;
      case DIK_F3:            a = In_KEY_F3;                break;
      case DIK_F4:            a = In_KEY_F4;                break;
      case DIK_F5:            a = In_KEY_F5;                break;
      case DIK_F6:            a = In_KEY_F6;                break;
      case DIK_F7:            a = In_KEY_F7;                break;
      case DIK_F8:            a = In_KEY_F8;                break;
      case DIK_F9:            a = In_KEY_F9;                break;
      case DIK_F10:           a = In_KEY_F10;               break;
      case DIK_F11:           a = In_KEY_F11;               break;
      case DIK_F12:           a = In_KEY_F12;               break;
      case DIK_F13:           a = In_KEY_F13;               break;
      case DIK_F14:           a = In_KEY_F14;               break;
      case DIK_F15:           a = In_KEY_F15;               break;
      case DIK_G:             a = In_KEY_G;                 break;
      case DIK_GRAVE:         a = In_KEY_BACKQUOTE;         break;
      case DIK_H:             a = In_KEY_H;                 break;
      case DIK_HOME:          a = In_KEY_HOME;              break;
      case DIK_I:             a = In_KEY_I;                 break;
      case DIK_INSERT:        a = In_KEY_INS;               break;
      case DIK_J:             a = In_KEY_J;                 break;
      case DIK_K:             a = In_KEY_K;                 break;
      //case DIK_KANA:          a = In_KEY_J_KANA;            break;
      //case DIK_KANJI:         a = In_KEY_J_KANJI;           break;
      case DIK_L:             a = In_KEY_L;                 break;
      case DIK_LBRACKET:      a = In_KEY_LBRACKET;          break;
      case DIK_LCONTROL:      a = In_KEY_LCTRL;             break;
      case DIK_LEFT:          a = In_KEY_LEFT;              break;
      case DIK_LMENU:         a = In_KEY_LALT;              break;
      case DIK_LSHIFT:        a = In_KEY_LSHIFT;            break;
      case DIK_LWIN:          a = In_KEY_LWIN;              break;
      case DIK_M:             a = In_KEY_M;                 break;
      //case DIK_MAIL:          a = In_KEY_APP_MAIL;          break;
      case DIK_MEDIASELECT:   a = In_KEY_MEDIA_SELECT;      break;
      case DIK_MEDIASTOP:     a = In_KEY_MEDIA_STOP;        break;
      case DIK_MINUS:         a = In_KEY_MINUS;             break;
      case DIK_MULTIPLY:      a = In_KEY_PAD_STAR;          break;
      case DIK_MUTE:          a = In_KEY_MEDIA_MUTE;        break;
      //case DIK_MYCOMPUTER:    a = In_KEY_APP_FILEBROWSER;   break;
      case DIK_N:             a = In_KEY_N;                 break;
      case DIK_NEXT:          a = In_KEY_PGDN;              break;
      case DIK_NEXTTRACK:     a = In_KEY_MEDIA_NEXT;        break;
      //case DIK_NOCONVERT:     a = In_KEY_J_NOCONVERT;       break;
      case DIK_NUMLOCK:       a = In_KEY_NUMLOCK;           break;
      case DIK_NUMPAD0:       a = In_KEY_PAD_0;             break;
      case DIK_NUMPAD1:       a = In_KEY_PAD_1;             break;
      case DIK_NUMPAD2:       a = In_KEY_PAD_2;             break;
      case DIK_NUMPAD3:       a = In_KEY_PAD_3;             break;
      case DIK_NUMPAD4:       a = In_KEY_PAD_4;             break;
      case DIK_NUMPAD5:       a = In_KEY_PAD_5;             break;
      case DIK_NUMPAD6:       a = In_KEY_PAD_6;             break;
      case DIK_NUMPAD7:       a = In_KEY_PAD_7;             break;
      case DIK_NUMPAD8:       a = In_KEY_PAD_8;             break;
      case DIK_NUMPAD9:       a = In_KEY_PAD_9;             break;
      case DIK_NUMPADCOMMA:   a = In_KEY_PAD_COMMA;         break;
      case DIK_NUMPADENTER:   a = In_KEY_PAD_ENTER;         break;
      case DIK_NUMPADEQUALS:  a = In_KEY_PAD_EQUALS;        break;
      case DIK_O:             a = In_KEY_O;                 break;
      //case DIK_OEM_102:       a = In_KEY_102;               break;
      case DIK_P:             a = In_KEY_P;                 break;
      case DIK_PAUSE:         a = In_KEY_BREAK;             break;
      case DIK_PERIOD:        a = In_KEY_PERIOD;            break;
      case DIK_PLAYPAUSE:     a = In_KEY_MEDIA_PLAY;        break;
      //case DIK_POWER:         a = In_KEY_SYS_POWER;         break;
      case DIK_PREVTRACK:     a = In_KEY_MEDIA_PREV;        break;
      case DIK_PRIOR:         a = In_KEY_PGUP;              break;
      case DIK_Q:             a = In_KEY_Q;                 break;
      case DIK_R:             a = In_KEY_R;                 break;
      case DIK_RBRACKET:      a = In_KEY_RBRACKET;          break;
      case DIK_RCONTROL:      a = In_KEY_RCTRL;             break;
      case DIK_RETURN:        a = In_KEY_ENTER;             break;
      case DIK_RIGHT:         a = In_KEY_RIGHT;             break;
      case DIK_RMENU:         a = In_KEY_RALT;              break;
      case DIK_RSHIFT:        a = In_KEY_RSHIFT;            break;
      case DIK_RWIN:          a = In_KEY_RWIN;              break;
      case DIK_S:             a = In_KEY_S;                 break;
      case DIK_SCROLL:        a = In_KEY_SCRLOCK;           break;
      case DIK_SEMICOLON:     a = In_KEY_SEMICOLON;         break;
      case DIK_SLASH:         a = In_KEY_SLASH;             break;
      //case DIK_SLEEP:         a = In_KEY_SYS_SLEEP;         break;
      case DIK_SPACE:         a = In_KEY_SPACE;             break;
      //case DIK_STOP:          a = In_KEY_PC98_STOP;         break;
      case DIK_SUBTRACT:      a = In_KEY_PAD_MINUS;         break;
      case DIK_SYSRQ:         a = In_KEY_SYSRQ;             break;
      case DIK_T:             a = In_KEY_T;                 break;
      case DIK_TAB:           a = In_KEY_TAB;               break;
      case DIK_U:             a = In_KEY_U;                 break;
      //case DIK_UNDERLINE:     a = In_KEY_PC98_UNDERSCORE;   break;
      //case DIK_UNLABELED:     a = In_KEY_J_UNLABELED;       break;
      case DIK_UP:            a = In_KEY_UP;                break;
      case DIK_V:             a = In_KEY_V;                 break;
      case DIK_VOLUMEDOWN:    a = In_KEY_MEDIA_VOLDN;       break;
      case DIK_VOLUMEUP:      a = In_KEY_MEDIA_VOLUP;       break;
      case DIK_W:             a = In_KEY_W;                 break;
      //case DIK_WAKE:          a = In_KEY_SYS_WAKE;          break;
      //case DIK_WEBBACK:       a = In_KEY_WEB_BACK;          break;
      //case DIK_WEBFAVORITES:  a = In_KEY_WEB_FAVORITES;     break;
      //case DIK_WEBFORWARD:    a = In_KEY_WEB_FORWARD;       break;
      //case DIK_WEBHOME:       a = In_KEY_WEB_HOME;          break;
      //case DIK_WEBREFRESH:    a = In_KEY_WEB_REFRESH;       break;
      //case DIK_WEBSEARCH:     a = In_KEY_WEB_SEARCH;        break;
      //case DIK_WEBSTOP:       a = In_KEY_WEB_STOP;          break;
      case DIK_X:             a = In_KEY_X;                 break;
      case DIK_Y:             a = In_KEY_Y;                 break;
      //case DIK_YEN:           a = In_KEY_J_YEN;             break;
      case DIK_Z:             a = In_KEY_Z;                 break;
      }
      break;

   case In_MOUSE:
      switch(dwOfs)
      {
      case DIMOFS_X:          a = In_MOUSE_AXIS_X;          break;
      case DIMOFS_Y:          a = In_MOUSE_AXIS_Y;          break;
      case DIMOFS_Z:          a = In_MOUSE_AXIS_Z;          break;
      default: a = In_MOUSE_BUTTON(dwOfs - DIMOFS_BUTTON0); break;
      }
      break;

   default:
      switch(dwOfs)
      {
      case DIJOFS_X:          a = In_JOY_AXIS_X;            break;
      case DIJOFS_Y:          a = In_JOY_AXIS_Y;            break;
      case DIJOFS_Z:          a = In_JOY_AXIS_Z;            break;
      case DIJOFS_RX:         a = In_JOY_ROT_X;             break;
      case DIJOFS_RY:         a = In_JOY_ROT_Y;             break;
      case DIJOFS_RZ:         a = In_JOY_ROT_Z;             break;
      case DIJOFS_SLIDER(0):  a = In_JOY_SLIDER_0;          break;
      case DIJOFS_SLIDER(1):  a = In_JOY_SLIDER_1;          break;
      case DIJOFS_POV(0):     a = In_JOY_HAT_0;             break;
      case DIJOFS_POV(1):     a = In_JOY_HAT_1;             break;
      case DIJOFS_POV(2):     a = In_JOY_HAT_2;             break;
      case DIJOFS_POV(3):     a = In_JOY_HAT_3;             break;
      default: a = In_JOY_BUTTON(dwOfs - DIJOFS_BUTTON0);   break;
      }
      break;
   }

   evt_out->action = a;

   if(In_IS_BUTTON_ACTION(a))
      evt_out->down = ((dwData & 0x80) != 0);
   else if(In_IS_MOUSE_ACTION(a))
      evt_out->change = dwData;
   else
      evt_out->value = dwData;
}

/* Translates a DIDEVICEOBJECTDATA to an In_event.
 */
static inline void Translate(const DIDEVICEOBJECTDATA * restrict didod,
                             int index,
                             In_device_type type,
                             In_event * restrict evt_out)
{
   evt_out->from = index;
   DiOfsToAction(didod->dwOfs, didod->dwData, type, evt_out);

   //TODO: 50-day wrap code?

   evt_out->time = g_start_adj + (g_freq_ratio * didod->dwTimeStamp);
}

/* Directly sends the events to the receiver, returning whatever it returns.
 */
static inline int Send(const receiver * restrict receiver,
                       In_event * restrict events, size_t num_events)
{
   if(receiver->is_callback)
      return receiver->callback(events, num_events, receiver->param);
   return Ds_Enqueue_In(receiver->dump, events, num_events);
}

/* Takes polled events waiting in the device's buffer and sends them to the
 * receiver.  If the receiver is a queue, returns how many events were enqueued
 * (0 on error or no events); if a callback, returns whatever the callback
 * returns.
 */
In_PRIVATE int SendEvents(device * restrict dev,
                          const receiver * restrict receiver)
{
   assert(dev->buf_events > 0);

   In_event events[dev->buf_events];

   for(int i = 0; i < dev->buf_events; ++i)
      Translate(&dev->buffer[i], dev->info.index, dev->info.type, &events[i]);

   int sent = Send(receiver, events, dev->buf_events);
   dev->buf_events = 0;
   return sent;
}

/* Sends polled events from all the devices' buffers to the receiver.  If the
 * receiver is a queue, returns the total number of events put into the queue
 * (0 only on error, as you shouldn't call this function if there are no
 * events); if a callback, returns whatever the callback returns.  Requires a
 * little pre-processing to get the total number of events, and a list of
 * devices that have polled events waiting in their buffers.
 */
In_PRIVATE int SendEventsFromAll(int total_events,
                                 device * restrict devices[], int num_devices,
                                 const receiver * restrict receiver)
{
   assert(total_events > 0);
   assert(num_devices > 0);

   int cur[num_devices]; //a list of current buffer indices for each device
   memset(cur, 0, sizeof(cur));

   In_event events[total_events]; //events to output

   for(int evt = 0; evt < total_events; ++evt)
   {
      DWORD min;
      int which = -1;

      //find the device with the lowest sequence number comin' up next
      for(int i = 0; i < num_devices; ++i)
      {
         if(cur[i] < devices[i]->buf_events
         && (which < 0 ||
             DISEQUENCE_COMPARE(devices[i]->buffer[cur[i]].dwSequence,<,min)))
         {
            min = devices[i]->buffer[cur[i]].dwSequence;
            which = i;
         }
      }

      assert(which >= 0);

      Translate(&devices[which]->buffer[cur[which]++],
                devices[which]->info.index,
                devices[which]->info.type,
                &events[evt]);
   }

   //set it so there's no more events
   for(int i = 0; i < num_devices; ++i)
   {
      assert(cur[i] == devices[i]->buf_events);
      devices[i]->buf_events = 0;
   }

   return Send(receiver, events, total_events);
}

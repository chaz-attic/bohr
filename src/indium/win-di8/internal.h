/******************************************************************************
* Indium - a portable low-level input library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#ifndef __internal_h__
#define __internal_h__

#define DIRECTINPUT_VERSION 0x0800

#include <windows.h>
#include <dinput.h>


//Controls export behavior.
#define In_PUBLIC  __declspec(dllexport)
#define In_PRIVATE

//Nix non-critical C99 keywords in compilers that don't support them.
#if((!defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
 && !defined(restrict))
#  define restrict
#endif


#include <bohr/in.h>


//How big we set each device's buffer, for gathering DI events.
#define DI_BUFFER_SIZE 32


//A single DI device, with everything we need to know about it.
typedef struct device
{
   LPDIRECTINPUTDEVICE8 device;                 //the actual DI device
   DIDEVICEOBJECTDATA   buffer[DI_BUFFER_SIZE]; //buffer to hold input events
   int                  buf_events;             //num gathered but not dumped
   In_device_info       info;                   //info about device

} device;

//Defines how we pass In_events back to the caller.
typedef struct receiver
{
   int is_callback; //if nonzero, we use callback/param, else, dump

   union
   {
      Ds_queue_In * dump;

      struct
      {
         In_callback_fn   callback;
         void           * param;
      };
   };
} receiver;


//The following functions are in init.c:
int EnterDi(HINSTANCE inst);
void ExitDi(void);

//In poll.c:
int InitPollTimers(void);
int GatherEvents(device * restrict dev);
int SendEvents(device * restrict dev, const receiver * restrict receiver);
int SendEventsFromAll(int total_events,
                      device * restrict devices[], int num_devices,
                      const receiver * restrict receiver);


#endif

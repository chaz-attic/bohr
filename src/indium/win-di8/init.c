/******************************************************************************
* Indium - a portable low-level input library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#include "internal.h"
#include <bohr/in.h>

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <dinput.h>


//A structure holding information we need during DI device enumeration.
typedef struct di_callback_device
{
   char name[In_NAME_SIZE]; //instance name
   GUID guid;               //instance guid

} di_callback_device;

//Define a vector that holds di_callback_devices.
#define Ds_VECTOR_BEHAVIOR 2
#define Ds_VECTOR_TYPE di_callback_device
#define Ds_VECTOR_SUFFIX _dicd
#include <bohr/ds_vector.h>

//Holds all the context we need between calls to our DI device enumeration
//callback.
typedef struct di_callback_context
{
   int            success; //whether the entire operation succeeded
   Ds_vector_dicd dicds;   //a list of di_callback_devices

} di_callback_context;

/* Initializes a di_callback_context.
 */
static inline int InitDiCallbackContext(di_callback_context * restrict dicc)
{
   dicc->success = 1;
   return Ds_InitVector_dicd(&dicc->dicds, 32);
}

/* Frees a di_callback_context.
 */
static inline void FreeDiCallbackContext(di_callback_context * restrict dicc)
{
   Ds_FreeVector_dicd(&dicc->dicds);
}


//Eek!  Globals!
In_PRIVATE device * g_devices     = NULL; //our device list
In_PRIVATE int      g_num_devices = 0;    //how many devices in the list

static LPDIRECTINPUT8 g_di = NULL; //the DirectInput context


/* Frees a device * array.
 */
static void FreeDevices(device * restrict devs, int num)
{
   for(int i = 0; i < num; ++i)
   {
      if(devs[i].device)
      {
         IDirectInputDevice8_Unacquire(devs[i].device);
         IDirectInputDevice8_Release(devs[i].device);
      }
   }

   free(devs);
}

/* Creates a device * array from a Ds_vector of di_callback_devices.
 */
static device * InitDevicesFromDiCallbackDevices(
   const Ds_vector_dicd * restrict dicds)
{
   device * devs;
   if(!(devs = (device *)calloc(dicds->num, sizeof(device))))
      return NULL;

   int i;
   for(i = 0; i < dicds->num; ++i)
   {
      DIDEVCAPS dicaps;

      if(FAILED(IDirectInput8_CreateDevice(g_di,
                                           &dicds->buf[i].guid,
                                           &devs[i].device,
                                           NULL)))
      {
         break;
      }

      dicaps.dwSize = sizeof(dicaps);
      if(FAILED(IDirectInputDevice8_GetCapabilities(devs[i].device, &dicaps)))
         break;

      memcpy(devs[i].info.name, dicds->buf[i].name,
             In_NAME_SIZE * sizeof(char));
      switch(dicaps.dwDevType & 0xff)
      {
      case DI8DEVTYPE_KEYBOARD:      devs[i].info.type = In_KEYBOARD; break;
      case DI8DEVTYPE_MOUSE:
      case DI8DEVTYPE_SCREENPOINTER: devs[i].info.type = In_MOUSE;    break;
      default:                       devs[i].info.type = In_JOYSTICK; break;
      }
      devs[i].info.index   = i;
      devs[i].info.buttons = dicaps.dwButtons;
      devs[i].info.axes    = dicaps.dwAxes;
      devs[i].info.hats    = dicaps.dwPOVs;
   }
   if(i < dicds->num)
   {
      FreeDevices(devs, dicds->num);
      return NULL;
   }

   return devs;
}

/* Our callback to enumerate devices given to us by DirectInput.
 */
static BOOL CALLBACK DiEnumDevicesCallback(LPCDIDEVICEINSTANCE restrict lpddi,
                                           LPVOID restrict pvRef)
{
   di_callback_context * ctx = (di_callback_context *)pvRef;

   if(lpddi)
   {
      di_callback_device dicd;

#if(In_NAME_SIZE > MAX_PATH)
#  error In_NAME_SIZE must be <= MAX_PATH
#endif

#ifdef UNICODE
      if(!WideCharToMultiByte(CP_UTF8, 0,
                              lpddi->tszInstanceName, -1,
                              dicd.name, In_NAME_SIZE,
                              NULL, NULL))
      {
         ctx->success = 0;
         return DIENUM_STOP;
      }
      dicd.name[In_NAME_SIZE-1] = '\0';
#else
      memcpy(dicd.name, lpddi->tszInstanceName,
             (In_NAME_SIZE-1) * sizeof(char));
      dicd.name[In_NAME_SIZE-1] = '\0';
#endif
      memcpy(&dicd.guid, &lpddi->guidInstance, sizeof(GUID));

      if(!Ds_InsertVectorItems_dicd(&ctx->dicds, &dicd, 1, Ds_VECTOR_END))
      {
         ctx->success = 0;
         return DIENUM_STOP;
      }
   }

   return DIENUM_CONTINUE;
}

/* Opens DirectInput and builds a list of devices.  You'll have to ExitDi() and
 * re-EnterDi() to refresh the list of devices.  Returns 0/nonzero on failure/
 * success.
 */
In_PRIVATE int EnterDi(HINSTANCE inst)
{
   di_callback_context dicc;
   int success = 0;

   do
   {
      if(!InitDiCallbackContext(&dicc))
         break;

      if(FAILED(DirectInput8Create(inst,
                                   DIRECTINPUT_VERSION,
                                   &IID_IDirectInput8,
                                   (void **)&g_di,
                                   NULL)))
      {
         break;
      }

      if(FAILED(IDirectInput8_EnumDevices(g_di,
                                          DI8DEVCLASS_KEYBOARD,
                                          DiEnumDevicesCallback,
                                          &dicc,
                                          DIEDFL_ATTACHEDONLY)))
      {
         break;
      }
      if(FAILED(IDirectInput8_EnumDevices(g_di,
                                          DI8DEVCLASS_POINTER,
                                          DiEnumDevicesCallback,
                                          &dicc,
                                          DIEDFL_ATTACHEDONLY)))
      {
         break;
      }
      if(FAILED(IDirectInput8_EnumDevices(g_di,
                                          DI8DEVCLASS_GAMECTRL,
                                          DiEnumDevicesCallback,
                                          &dicc,
                                          DIEDFL_ATTACHEDONLY)))
      {
         break;
      }
      if(!dicc.success)
         break;

      if(!(g_devices = InitDevicesFromDiCallbackDevices(&dicc.dicds)))
         break;
      g_num_devices = dicc.dicds.num;

      success = 1;
   } while(0);

   FreeDiCallbackContext(&dicc);
   if(!success)
      ExitDi();

   return success;
}

/* Closes DirectInput and frees the list of devices.
 */
In_PRIVATE void ExitDi()
{
   if(g_devices)
   {
      FreeDevices(g_devices, g_num_devices);
      g_devices = NULL;
   }
   g_num_devices = 0;
   if(g_di)
   {
      IDirectInput8_Release(g_di);
      g_di = NULL;
   }
}

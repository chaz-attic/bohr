/******************************************************************************
* Indium - a portable low-level input library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#include "internal.h"
#include <bohr/in.h>

#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <windows.h>
#include <dinput.h>


extern device * g_devices;       //from init.c
extern int      g_num_devices;   //me too

static int      g_ref_count = 0; //how many times In_Init() has been called


/* Returns the version of the Indium interface this library supports.  Compare
 * with In_HEADER_VERSION.
 */
In_PUBLIC uint32_t In_GetVersion(void)
{
   return In_HEADER_VERSION;
}

/* Returns a human-readable name of this implementation of Indium, in the
 * following format:
 *    OS[/Subsystem][ (comment)]
 * where each part in []s is optional.
 */
In_PUBLIC const char * In_GetImplName(void)
{
#ifdef UNICODE
   return "WindowsNT/DirectInput8 (Bohr v0.1)";
#else
   return "Windows/DirectInput8 (Bohr v0.1)";
#endif
}

/* Reference-counted initialization.  You must call In_Quit() the same number
 * of times that you call this, regardless of if it succeeds.  Returns 0/
 * nonzero on failure/success.
 */
In_PUBLIC int In_Init(void)
{
   if(++g_ref_count == 1)
   {
      if(!InitPollTimers())
         return 0;
      return EnterDi(GetModuleHandle(NULL));
   }
   return 1;
}

/* Reference-counted cleanup.  Call this function the same number of times you
 * call In_Init().
 */
In_PUBLIC void In_Quit(void)
{
   if(--g_ref_count == 0)
      ExitDi();
}

/* Refreshes the list of devices.  Returns 0/nonzero on failure/success.
 */
In_PUBLIC int In_Refresh(void)
{
   //TODO: this can leave the library in an unusable state
   //TODO: what happens if you refresh when devices are open?  ERRORS!
   ExitDi();
   return EnterDi(GetModuleHandle(NULL));
}

/* Returns the number of devices currently in the list.
 */
In_PUBLIC int In_GetNumDevices(void)
{
   return g_num_devices;
}

/* Returns information about the device with the given index.  Returns 0/
 * nonzero on failure/success.
 */
In_PUBLIC int In_GetDeviceInfo(int index, In_device_info * restrict info_out)
{
   if(index < 0 || index >= g_num_devices || !info_out)
      return 0;

   memcpy(info_out, &g_devices[index].info, sizeof(In_device_info));
   return 1;
}

/* Returns the index of the first device in the list with the given type.
 * Returns -1 if there aren't any devices with that type.
 */
static inline int FindByType(In_device_type type)
{
   int i;

   for(i = 0; i < g_num_devices; ++i)
   {
      if(g_devices[i].info.type == type)
         return i;
   }

   return -1;
}

/* Returns the index of the device with the given name, or a negative number if
 * the named device isn't found.  Specify "In_KEYBOARD", "In_MOUSE", or
 * "In_JOYSTICK" to return the index of the first device found with that type
 * (or negative if there aren't any devices of that type).
 */
In_PUBLIC int In_FindDevice(const char * restrict name)
{
   int n = -1;

   if(name)
   {
      if(!strcmp(name, "In_KEYBOARD"))
         n = FindByType(In_KEYBOARD);
      else if(!strcmp(name, "In_MOUSE"))
         n = FindByType(In_MOUSE);
      else if(!strcmp(name, "In_JOYSTICK"))
         n = FindByType(In_JOYSTICK);
      else
      {
         for(n = g_num_devices-1; n >= 0; --n)
         {
            if(!strcmp(name, g_devices[n].info.name))
               break;
         }
      }
   }

   return n;
}

/* Returns a LPCDIDATAFORMAT from an In_device_type.
 */
static inline LPCDIDATAFORMAT InTypeToDiDataFormat(In_device_type type)
{
   LPCDIDATAFORMAT ret;

   switch(type)
   {
   case In_KEYBOARD: ret = &c_dfDIKeyboard;  break;
   case In_MOUSE:    ret = &c_dfDIMouse2;    break;
   default:          ret = &c_dfDIJoystick2; break;
   }

   return ret;
}

/* Opens the device with the given index.  Once open, you can get events for
 * the device.  Returns 0/nonzero on failure/success.
 */
In_PUBLIC int In_OpenDevice(int index)
{
   if(index < 0 || index >= g_num_devices || g_devices[index].info.is_open)
      return 0;

   int success = 0;

   do
   {
      if(FAILED(IDirectInputDevice8_SetDataFormat(
                   g_devices[index].device,
                   InTypeToDiDataFormat(g_devices[index].info.type))))
      {
         break;
      }

      DWORD dwFlags = DISCL_FOREGROUND | DISCL_NONEXCLUSIVE;
      if(g_devices[index].info.type == In_KEYBOARD)
         dwFlags |= DISCL_NOWINKEY;
      if(FAILED(IDirectInputDevice8_SetCooperativeLevel(
                   g_devices[index].device,
                   GetForegroundWindow(),
                   dwFlags)))
      {
         break;
      }

      DIPROPDWORD dipdw =
      {
         {
            sizeof(DIPROPDWORD),
            sizeof(DIPROPHEADER),
            0,
            DIPH_DEVICE
         },
         DI_BUFFER_SIZE
      };
      if(FAILED(IDirectInputDevice8_SetProperty(g_devices[index].device,
                                                DIPROP_BUFFERSIZE,
                                                &dipdw.diph)))
      {
         break;
      }

      if(g_devices[index].info.type == In_JOYSTICK)
      {
         DIPROPRANGE dipr =
         {
            {
               sizeof(DIPROPRANGE),
               sizeof(DIPROPHEADER),
               0,
               DIPH_DEVICE
            },
            In_JOY_AXIS_MIN,
            In_JOY_AXIS_MAX
         };
         if(FAILED(IDirectInputDevice8_SetProperty(g_devices[index].device,
                                                   DIPROP_RANGE,
                                                   &dipr.diph)))
         {
            break;
         }
      }

      if(FAILED(IDirectInputDevice8_Acquire(g_devices[index].device)))
         break;

      g_devices[index].info.is_open = 1;

      success = 1;
   } while(0);

   if(!success)
      In_CloseDevice(index);

   return success;
}

/* Closes the device with the given index.
 */
In_PUBLIC void In_CloseDevice(int index)
{
   if(index >= 0 && index < g_num_devices && g_devices[index].info.is_open)
   {
      IDirectInputDevice8_Unacquire(g_devices[index].device);
      g_devices[index].info.is_open = 0;
   }
}

/* Wrapper for In_PollDevice() and In_PollDeviceCallback().
 */
static inline int PollDevice(int index, const receiver * restrict receiver)
{
   if(index < 0
   || index >= g_num_devices
   || !g_devices[index].info.is_open
   || !GatherEvents(&g_devices[index]))
      return -1;

   if(!g_devices[index].buf_events)
      return 0;
   return SendEvents(&g_devices[index], receiver);
}

/* Polls the device with the given index for events.  The device must be in the
 * open state.  Events are put in order into the dump queue (retrieve them with
 * Ds_Dequeue_In()).  Returns how many events were put into the queue
 * (including 0), or a negative number on failure.
 */
In_PUBLIC int In_PollDevice(int index, Ds_queue_In * restrict dump)
{
   //Since this struct involves a nameless union, we can't do fancy inline
   //initialization, in GCC anyway.
   receiver r;
   r.is_callback = 0;
   r.dump = dump;

   return PollDevice(index, &r);
}

/* Same as In_PollDevice(), but calls a callback instead of using a queue.
 * Returns whatever the callback returns.
 */
In_PUBLIC int In_PollDeviceCallback(int index,
                                    In_callback_fn callback,
                                    void * restrict param)
{
   //Here, too.
   receiver r;
   r.is_callback = 1;
   r.callback = callback;
   r.param = param;

   return PollDevice(index, &r);
}

/* Wrapper for In_PollOpenDevices() and In_PollOpenDevicesCallback().
 */
static inline int PollOpenDevices(const receiver * restrict receiver)
{
   device * open_devices[g_num_devices]; //open devices with gathered data
   int num_open_devices = 0;

   //find which devices are open and we successfully gather data from
   for(int i = 0; i < g_num_devices; ++i)
   {
      if(g_devices[i].info.is_open)
      {
         if(!GatherEvents(&g_devices[i]))
            break;
         open_devices[num_open_devices++] = &g_devices[i];
      }
   }

   //find out how many events there were in total
   int total_events = 0;
   for(int i = 0; i < num_open_devices; ++i)
      total_events += open_devices[i]->buf_events;

   if(total_events <= 0)
      return 0;

   return SendEventsFromAll(total_events, open_devices, num_open_devices,
                            receiver);
}

/* Polls all currently open devices for events.  Events are put in order of
 * occurrence (not necessarily by device) into the dump queue (retrieve them
 * with Ds_Dequeue_In()).  Returns the total number of events put into the
 * queue (including 0), or a negative number on failure.
 */
In_PUBLIC int In_PollOpenDevices(Ds_queue_In * restrict dump)
{
   receiver r;
   r.is_callback = 0;
   r.dump = dump;

   return PollOpenDevices(&r);
}

/* Same as In_PollOpenDevices(), except calls a callback instead of inserting
 * the events into a queue.  Returns whatever the callback returns.
 */
In_PUBLIC int In_PollOpenDevicesCallback(In_callback_fn callback,
                                         void * restrict param)
{
   receiver r;
   r.is_callback = 1;
   r.callback = callback;
   r.param = param;

   return PollOpenDevices(&r);
}

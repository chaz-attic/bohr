/******************************************************************************
* Indium - a portable low-level input library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/

#include <bohr/in.h>
#include <bohr/w.h>
#include <bohr/po_time.h>
#include <bohr/po_sync.h>
#include <stdio.h>


static void PrintEvent(const In_event * restrict e)
{
   const char * s;

   printf(" - device %3d: ", e->from);
   if(In_IS_KEY_ACTION(e->action))
   {
      printf("key   ");

      switch(e->action)
      {
      case In_KEY_A:                s = "A";                break;
      case In_KEY_B:                s = "B";                break;
      case In_KEY_C:                s = "C";                break;
      case In_KEY_D:                s = "D";                break;
      case In_KEY_E:                s = "E";                break;
      case In_KEY_F:                s = "F";                break;
      case In_KEY_G:                s = "G";                break;
      case In_KEY_H:                s = "H";                break;
      case In_KEY_I:                s = "I";                break;
      case In_KEY_J:                s = "J";                break;
      case In_KEY_K:                s = "K";                break;
      case In_KEY_L:                s = "L";                break;
      case In_KEY_M:                s = "M";                break;
      case In_KEY_N:                s = "N";                break;
      case In_KEY_O:                s = "O";                break;
      case In_KEY_P:                s = "P";                break;
      case In_KEY_Q:                s = "Q";                break;
      case In_KEY_R:                s = "R";                break;
      case In_KEY_S:                s = "S";                break;
      case In_KEY_T:                s = "T";                break;
      case In_KEY_U:                s = "U";                break;
      case In_KEY_V:                s = "V";                break;
      case In_KEY_W:                s = "W";                break;
      case In_KEY_X:                s = "X";                break;
      case In_KEY_Y:                s = "Y";                break;
      case In_KEY_Z:                s = "Z";                break;
      case In_KEY_0:                s = "0";                break;
      case In_KEY_1:                s = "1";                break;
      case In_KEY_2:                s = "2";                break;
      case In_KEY_3:                s = "3";                break;
      case In_KEY_4:                s = "4";                break;
      case In_KEY_5:                s = "5";                break;
      case In_KEY_6:                s = "6";                break;
      case In_KEY_7:                s = "7";                break;
      case In_KEY_8:                s = "8";                break;
      case In_KEY_9:                s = "9";                break;
      case In_KEY_QUOTE:            s = "QUOTE";            break;
      case In_KEY_BACKQUOTE:        s = "BACKQUOTE";        break;
      case In_KEY_SLASH:            s = "SLASH";            break;
      case In_KEY_BACKSLASH:        s = "BACKSLASH";        break;
      case In_KEY_MINUS:            s = "MINUS";            break;
      case In_KEY_EQUALS:           s = "EQUALS";           break;
      case In_KEY_BACKSPACE:        s = "BACKSPACE";        break;
      case In_KEY_SPACE:            s = "SPACE";            break;
      case In_KEY_ENTER:            s = "ENTER";            break;
      case In_KEY_TAB:              s = "TAB";              break;
      case In_KEY_CAPSLOCK:         s = "CAPSLOCK";         break;
      case In_KEY_SEMICOLON:        s = "SEMICOLON";        break;
      case In_KEY_COMMA:            s = "COMMA";            break;
      case In_KEY_PERIOD:           s = "PERIOD";           break;
      case In_KEY_LBRACKET:         s = "LBRACKET";         break;
      case In_KEY_RBRACKET:         s = "RBRACKET";         break;
      case In_KEY_LSHIFT:           s = "LSHIFT";           break;
      case In_KEY_RSHIFT:           s = "RSHIFT";           break;
      case In_KEY_LCTRL:            s = "LCTRL";            break;
      case In_KEY_RCTRL:            s = "RCTRL";            break;
      case In_KEY_LALT:             s = "LALT";             break;
      case In_KEY_RALT:             s = "RALT";             break;
      case In_KEY_LWIN:             s = "LWIN";             break;
      case In_KEY_RWIN:             s = "RWIN";             break;
      case In_KEY_MENU:             s = "MENU";             break;
      case In_KEY_F1:               s = "F1";               break;
      case In_KEY_F2:               s = "F2";               break;
      case In_KEY_F3:               s = "F3";               break;
      case In_KEY_F4:               s = "F4";               break;
      case In_KEY_F5:               s = "F5";               break;
      case In_KEY_F6:               s = "F6";               break;
      case In_KEY_F7:               s = "F7";               break;
      case In_KEY_F8:               s = "F8";               break;
      case In_KEY_F9:               s = "F9";               break;
      case In_KEY_F10:              s = "F10";              break;
      case In_KEY_F11:              s = "F11";              break;
      case In_KEY_F12:              s = "F12";              break;
      case In_KEY_ESC:              s = "ESC";              break;
      case In_KEY_SYSRQ:            s = "SYSRQ";            break;
      case In_KEY_SCRLOCK:          s = "SCRLOCK";          break;
      case In_KEY_BREAK:            s = "BREAK";            break;
      case In_KEY_INS:              s = "INS";              break;
      case In_KEY_DEL:              s = "DEL";              break;
      case In_KEY_HOME:             s = "HOME";             break;
      case In_KEY_END:              s = "END";              break;
      case In_KEY_PGUP:             s = "PGUP";             break;
      case In_KEY_PGDN:             s = "PGDN";             break;
      case In_KEY_UP:               s = "UP";               break;
      case In_KEY_LEFT:             s = "LEFT";             break;
      case In_KEY_DOWN:             s = "DOWN";             break;
      case In_KEY_RIGHT:            s = "RIGHT";            break;
      case In_KEY_NUMLOCK:          s = "NUMLOCK";          break;
      case In_KEY_PAD_ENTER:        s = "PAD_ENTER";        break;
      case In_KEY_PAD_PLUS:         s = "PAD_PLUS";         break;
      case In_KEY_PAD_MINUS:        s = "PAD_MINUS";        break;
      case In_KEY_PAD_STAR:         s = "PAD_STAR";         break;
      case In_KEY_PAD_SLASH:        s = "PAD_SLASH";        break;
      case In_KEY_PAD_PERIOD:       s = "PAD_PERIOD";       break;
      case In_KEY_PAD_0:            s = "PAD_0";            break;
      case In_KEY_PAD_1:            s = "PAD_1";            break;
      case In_KEY_PAD_2:            s = "PAD_2";            break;
      case In_KEY_PAD_3:            s = "PAD_3";            break;
      case In_KEY_PAD_4:            s = "PAD_4";            break;
      case In_KEY_PAD_5:            s = "PAD_5";            break;
      case In_KEY_PAD_6:            s = "PAD_6";            break;
      case In_KEY_PAD_7:            s = "PAD_7";            break;
      case In_KEY_PAD_8:            s = "PAD_8";            break;
      case In_KEY_PAD_9:            s = "PAD_9";            break;
      case In_KEY_MEDIA_PLAY:       s = "MEDIA_PLAY";       break;
      case In_KEY_MEDIA_STOP:       s = "MEDIA_STOP";       break;
      case In_KEY_MEDIA_NEXT:       s = "MEDIA_NEXT";       break;
      case In_KEY_MEDIA_PREV:       s = "MEDIA_PREV";       break;
      case In_KEY_MEDIA_MUTE:       s = "MEDIA_MUTE";       break;
      case In_KEY_MEDIA_VOLUP:      s = "MEDIA_VOLUP";      break;
      case In_KEY_MEDIA_VOLDN:      s = "MEDIA_VOLDN";      break;
      case In_KEY_MEDIA_SELECT:     s = "MEDIA_SELECT";     break;
/*
      case In_KEY_WEB_HOME:         s = "WEB_HOME";         break;
      case In_KEY_WEB_SEARCH:       s = "WEB_SEARCH";       break;
      case In_KEY_WEB_FAVORITES:    s = "WEB_FAVORITES";    break;
      case In_KEY_WEB_REFRESH:      s = "WEB_REFRESH";      break;
      case In_KEY_WEB_STOP:         s = "WEB_STOP";         break;
      case In_KEY_WEB_FORWARD:      s = "WEB_FORWARD";      break;
      case In_KEY_WEB_BACK:         s = "WEB_BACK";         break;
      case In_KEY_SYS_POWER:        s = "SYS_POWER";        break;
      case In_KEY_SYS_SLEEP:        s = "SYS_SLEEP";        break;
      case In_KEY_SYS_WAKE:         s = "SYS_WAKE";         break;
      case In_KEY_APP_CALC:         s = "APP_CALC";         break;
      case In_KEY_APP_MYCOMPUTER:   s = "APP_MYCOMPUTER";   break;
      case In_KEY_APP_MAIL:         s = "APP_MAIL";         break;
      case In_KEY_102:              s = "102";              break;
      case In_KEY_J_YEN:            s = "J_YEN";            break;
      case In_KEY_J_KANA:           s = "J_KANA";           break;
      case In_KEY_J_KANJI:          s = "J_KANJI";          break;
      case In_KEY_J_CONVERT:        s = "J_CONVERT";        break;
      case In_KEY_J_NOCONVERT:      s = "J_NOCONVERT";      break;
      case In_KEY_J_AX:             s = "J_AX";             break;
      case In_KEY_J_UNLABELED:      s = "J_UNLABELED";      break;
      case In_KEY_ABNT_C1:          s = "ABNT_C1";          break;
      case In_KEY_ABNT_C2:          s = "ABNT_C2";          break;
      case In_KEY_PC98_PAD_EQUALS:  s = "PC98_PAD_EQUALS";  break;
      case In_KEY_PC98_PAD_COMMA:   s = "PC98_PAD_COMMA";   break;
      case In_KEY_PC98_AT:          s = "PC98_AT";          break;
      case In_KEY_PC98_COLON:       s = "PC98_COLON";       break;
      case In_KEY_PC98_UNDERSCORE:  s = "PC98_UNDERSCORE";  break;
      case In_KEY_PC98_STOP:        s = "PC98_STOP";        break;
      case In_KEY_PC98_F13:         s = "PC98_F13";         break;
      case In_KEY_PC98_F14:         s = "PC98_F14";         break;
      case In_KEY_PC98_F15:         s = "PC98_F15";         break;
//*/
      default:                      s = "unknown";          break;
      }

      printf("%-20s %-8s  ", s, (e->down ? "pressed" : "released"));
   }
   else if(In_IS_MOUSE_ACTION(e->action))
   {
      printf("mouse ");

      switch(e->action)
      {
      case In_MOUSE_AXIS_X:   s = "AXIS_X";  break;
      case In_MOUSE_AXIS_Y:   s = "AXIS_Y";  break;
      case In_MOUSE_AXIS_Z:   s = "AXIS_Z";  break;
      default:                s = NULL;      break;
      }

      if(s)
         printf("%-6s               %10d", s, e->change);
      else
      {
         printf("BUTTON_%-2d            %-8s  ",
                In_GET_MOUSE_BUTTON(e->action),
                (e->down ? "pressed" : "released"));
      }
   }
   else
   {
      printf("joy   ");
      switch(e->action)
      {
      case In_JOY_AXIS_X:     s = "AXIS_X";     break;
      case In_JOY_AXIS_Y:     s = "AXIS_Y";     break;
      case In_JOY_AXIS_Z:     s = "AXIS_Z";     break;
      case In_JOY_ROT_X:      s = "ROT_X";      break;
      case In_JOY_ROT_Y:      s = "ROT_Y";      break;
      case In_JOY_ROT_Z:      s = "ROT_Z";      break;
      case In_JOY_SLIDER_0:   s = "SLIDER_0";   break;
      case In_JOY_SLIDER_1:   s = "SLIDER_1";   break;
      case In_JOY_HAT_0:      s = "HAT_0";      break;
      case In_JOY_HAT_1:      s = "HAT_1";      break;
      case In_JOY_HAT_2:      s = "HAT_2";      break;
      case In_JOY_HAT_3:      s = "HAT_3";      break;
      default:                s = NULL;         break;
      }

      if(s)
         printf("%-8s             %10d", s, e->value);
      else
      {
         printf("BUTTON_%-3d           %-8s  ",
                In_GET_JOY_BUTTON(e->action),
                (e->down ? "pressed" : "released"));
      }
   }

   printf(" at t=%20"PRIdPo_TIME"\n", e->time);
}

int main(int argc, char * argv[])
{
   Ds_queue_In q = Ds_QUEUE_INIT;
   W_window win = NULL;
   int rc = 1;

   do
   {
      if(!W_Init())
      {
         fprintf(stderr, "testinputevents: test failed: W_Init() failed\n");
         break;
      }
      if(!In_Init())
      {
         fprintf(stderr, "testinputevents: test failed: In_Init() failed\n");
         break;
      }

      if(!Ds_InitQueue_In(&q, 32, 0))
      {
         fprintf(stderr, "testinputevents: test failed: Ds_InitQueue_In() "
                         "failed\n");
         break;
      }

      printf("testinputevents: info: creating a window; this "
             "window will have the contents\n");
      printf("  of whatever was behind it when it popped up "
             "and do no drawing of its own;\n");
      printf("  this is normal--it's not broken; hit a few "
             "keys and try some other input\n");
      printf("  devices, and press Q or ESC to exit\n");

      W_window win = W_CreateWindow(0, 0, NULL, NULL, "Input Grabber", NULL,
                                    0, NULL, NULL);
      if(!win)
      {
         fprintf(stderr, "testinputevents: test failed: W_CreateWindow() "
                         "failed\n");
         break;
      }

      int i;
      for(i = In_GetNumDevices()-1; i >= 0; --i)
      {
         if(!In_OpenDevice(i))
         {
            fprintf(stderr, "testinputevents: test failed: In_OpenDevice() "
                            "failed for device %d\n", i);
            break;
         }
      }
      if(i >= 0)
         break;

      int done = 0;
      while(!done)
      {
         if(In_PollOpenDevices(&q) < 0)
         {
            fprintf(stderr, "testinputevents: test failed: "
                            "In_PollOpenDevices() failed\n");
            break;
         }

         if(q.num)
         {
            In_event events[q.num];
            size_t num_events = Ds_Dequeue_In(&q, events, q.num, 0, 0);
            for(i = 0; i < num_events; ++i)
            {
               PrintEvent(&events[i]);

               if((events[i].action == In_KEY_ESC ||
                   events[i].action == In_KEY_Q)
               && !events[i].down)
               {
                  printf("testinputevents: quitting...\n");
                  done = 1;
                  break;
               }
            }
         }

         static int frames = 0;
         if(!(++frames % 1000))
         {
            printf("                                                        "
                   "t=%20"PRIdPo_TIME"\n", Po_GetTime());
         }

         Po_Sleep(1);
      }
      if(!done)
         break;

      rc = 0;
   } while(0);

   if(rc == 0)
      printf("testinputevents: test succeeded\n");

   W_FreeWindow(win);
   Ds_FreeQueue_In(&q);
   In_Quit();
   W_Quit();
   return rc;
}

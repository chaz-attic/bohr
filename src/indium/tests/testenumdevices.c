/******************************************************************************
* Indium - a portable low-level input library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/

#include <bohr/in.h>
#include <stdio.h>


static void PrintDeviceInfo(const In_device_info * restrict info)
{
   printf(" * device %d (%s): %s\n", info->index,
                                     (info->type == In_KEYBOARD ?
                                      "In_KEYBOARD" :
                                      (info->type == In_MOUSE ?
                                       "In_MOUSE" :
                                       (info->type == In_JOYSTICK ?
                                        "In_JOYSTICK" :
                                        "unknown type"))),
                                     info->name);
   printf("    buttons: %d, axes: %d, POV hats: %d\n\n", info->buttons,
                                                         info->axes,
                                                         info->hats);
}

int main(int argc, char * argv[])
{
   int rc = 1;

   do
   {
      if(!In_Init())
      {
         fprintf(stderr, "testenumdevices: test failed: In_Init() failed\n");
         break;
      }

      int devices;
      if(!(devices = In_GetNumDevices()))
      {
         fprintf(stderr, "testenumdevices: WARNING: no devices attached "
                         "to the system were detected;\n");
         fprintf(stderr, " this probably means Indium isn't functioning, "
                         "but not necessarily\n");
      }
      else
      {
         printf("testenumdevices: found %d devices; information:\n", devices);

         int i;
         for(i = 0; i < devices; ++i)
         {
            In_device_info info;
            if(!In_GetDeviceInfo(i, &info))
            {
               fprintf(stderr, "testenumdevices: test failed: "
                               "In_GetDeviceInfo() failed for "
                               "device index %d\n", i);
               break;
            }
            PrintDeviceInfo(&info);
         }
         if(i < devices)
            break;
      }

      rc = 0;
   } while(0);

   if(rc == 0)
      printf("testenumdevices: test succeeded\n");

   In_Quit();
   return rc;
}


/******************************************************************************
* Polonium - a portability library
* Po_compile - tests whether Polonium will compile
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


/* Compiler   Command
 * ---------  -----------------------------------------------------------------
 * GCC        gcc -std=gnu99 -o po_compile -D_XOPEN_SOURCE=600 po_compile.c -I../../include -Wall -lpthread -ldl
 * MinGW/GCC  gcc -std=gnu99 -o po_compile -D_XOPEN_SOURCE=600 po_compile.c -I../../include -Wall
 *
 * Usage: (none)
 */


#include <bohr/po.h>


int main(void)
{
   return 0;
}

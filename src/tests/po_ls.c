/******************************************************************************
* Polonium - a portability library
* Po_ls - a limited 'ls' program implemented with Polonium
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


/* Compiler   Command
 * ---------  -----------------------------------------------------------------
 * GCC        gcc -std=gnu99 -o po_ls po_ls.c -I../include
 * MinGW/GCC  gcc -std=gnu99 -o po_ls po_ls.c -I../include
 *
 * Usage: po_ls [pattern]
 * Output should be similar to 'ls -apU1 [pattern]'.  [pattern], if given, must
 * be in the form "[<directory>/]<file>" (quotes included) where <file> can
 * contain wildcards.  (There is a difference between ls' and Polonium's
 * matching algorithm, so one may report some entries the other does not.)
 */


#include <bohr/po_dir.h>
#include <stdio.h>


int main(int argc, char * argv[])
{
   Po_findfiles find = Po_FINDFILES_INIT;

   find.pattern = (argc >= 2 ? argv[1] : "*");
   while(Po_FindFiles(&find))
      printf("%s%s\n", find.match, (find.match_dir ? "/" : ""));

   return 0;
}

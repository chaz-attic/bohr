/******************************************************************************
* Darmstadtium - a library of data structures
* Ds_compile - tests whether Darmstadtium will compile
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


/* Compiler   Command
 * ---------  -----------------------------------------------------------------
 * GCC        gcc -std=gnu99 -o ds_compile ds_compile.c -I../../include -Wall -D_XOPEN_SOURCE=500
 * MinGW/GCC  gcc -std=gnu99 -o ds_compile ds_compile.c -I../../include -Wall -D_XOPEN_SOURCE=500
 *
 * Usage: (none)
 */


#include <bohr/ds.h>


int main(void)
{
   return 0;
}

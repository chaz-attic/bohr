/******************************************************************************
* Polonium - a portability library
* Po_home - uses Polonium to find home directory
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


/* Compiler   Command
 * ---------  -----------------------------------------------------------------
 * GCC        gcc -std=gnu99 -o po_home po_home.c -I../include
 * MinGW/GCC  gcc -std=gnu99 -o po_home po_home.c -I../include
 *
 * Usage: po_home
 * Prints the user's home directory.
 */


#include <bohr/po_dir.h>
#include <stdio.h>


int main(void)
{
   puts(Po_GetHomeDirectory());
   return 0;
}

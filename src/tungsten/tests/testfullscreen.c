/******************************************************************************
* Tungsten - a portable OpenGL window library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#include <stdio.h>
#include <stdlib.h>

#include <bohr/w.h>


static void PrintMode(W_mode * m, FILE * f);
static void WaitASec(void);


int main(int argc, char * argv[])
{
   W_mode mode = {0, 640, 480, 16, 0, 1, 0, 0, 0, 0, 0, 0, 0};

   fputs("testfullscreen: info: going to 640x480x16 fullscreen for 2 seconds\n", stdout);

   if(!W_CreateWindow("Test Window", NULL, 0, &mode, NULL))
   {
      fputs("testfullscreen: test failed: W_CreateWindow failed for mode:\n  ", stderr);
      PrintMode(&mode, stderr);
      fputs("\n", stderr);
      return 1;
   }

   WaitASec();

   W_DestroyWindow();
   fputs("testfullscreen: test succeeded\n", stdout);
   return 0;
}

static void PrintMode(W_mode * m, FILE * f)
{
   fprintf(f, "dev:%d %dx%dx%d ", m->adapter, m->width, m->height, m->color_bits);
   if(m->fullscreen)
      fprintf(f, "%dHz ", m->frequency);
   else
      fputs("windowed ", f);
   fprintf(f, "pal:%c ", (m->palette ? 'y' : 'n'));
   fprintf(f, "2xbuf:%c ", (m->double_buffer ? 'y' : 'n'));
   fprintf(f, "al:%d de:%d ac:%d st:%d ax:%d", m->alpha_bits, m->depth_bits, m->accum_bits, m->stencil_bits, m->aux_buffers);
}

#ifdef _WIN32
#  include <windows.h>
#else
#  include <time.h>
#endif

static void WaitASec(void)
{
#ifdef _WIN32
   Sleep(2000);
#else
   struct timespec ts;
   ts.tv_sec = 2;
   ts.tv_nsec = 0;
   nanosleep(&ts, NULL);
#endif
}

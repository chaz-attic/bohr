/******************************************************************************
* Tungsten - a portable OpenGL window library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#include <bohr/w.h>
#include <bohr/po_sync.h>
#include <stdio.h>
#include <stdlib.h>


static volatile int done = 0;


static void PrintModeAndFormat(int fullscreen, const W_mode * restrict mode,
                               const W_format * restrict format, FILE * f)
{
   if(fullscreen)
      fprintf(f, "%dx%dx%dx%d", mode->width, mode->height, mode->bpp,
              mode->refresh);
   else
      fprintf(f, "%dx%d (win)", mode->width, mode->height);

   fprintf(f, ", rgba:%d.%d.%d.%d, d:%d, a:%d, s:%d, db:%s, aux:%d\n",
           format->red_bits, format->green_bits, format->blue_bits,
           format->alpha_bits, format->depth_bits, format->accum_bits,
           format->stencil_bits, (format->double_buffer ? "yes" : "no"),
           format->aux_buffers);
}

static int OnEvent(W_action action, void * restrict action_params,
                   void * restrict app_param)
{
   W_create_params * cp = (W_create_params *)action_params;
   W_activate_params * ap = (W_activate_params *)action_params;
   W_resize_params * rp = (W_resize_params *)action_params;

   switch(action)
   {
   case W_CREATE:
      printf("testwin: info: event: W_CREATE, mode:\n  ");
      PrintModeAndFormat(cp->fullscreen, &cp->mode, &cp->format, stdout);
      break;

   case W_PAINT:
      printf("testwin: info: event: W_PAINT\n");
      break;

   case W_RESIZE:
      printf("testwin: info: event: W_RESIZE, dimensions: %dx%d\n",
             rp->width, rp->height);
      break;

   case W_ACTIVATE:
      printf("testwin: info: event: W_ACTIVATE: %s, %s\n",
             (ap->focus ? "has focus" : "lost focus"),
             (ap->hidden ? "hidden" : "not hidden"));
      break;

   case W_CLOSE:
      printf("testwin: info: event: W_CLOSE\n");
      done = 1;
      break;

   case W_SCREENSAVER:
      printf("testwin: info: event: W_SCREENSAVER, preventing\n");
      return 0;

   default:
      printf("testwin: info: event: unknown\n");
      break;
   }

   return 1;
}

int main(int argc, char * argv[])
{
   if(!W_Init())
   {
      fprintf(stderr, "testwin: test failed: W_Init() failed\n");
      return 1;
   }

   printf("testwin: info: usage: %s [fullscreen] [width] [height] [bpp] "
          "[refresh]\n\n", argv[0]);

   int fullscreen = 0;
   W_mode mode = {.width = 640, .height = 480, .bpp = 16};

   if(argc >= 2)
      fullscreen = (atoi(argv[1]) != 0);
   if(argc >= 3)
      mode.width = atoi(argv[2]);
   if(argc >= 4)
      mode.height = atoi(argv[3]);
   if(argc >= 5)
      mode.bpp = atoi(argv[4]);
   if(argc >= 6)
      mode.refresh = atoi(argv[5]);

   W_window win = W_CreateWindow(0, fullscreen, &mode, NULL, "Test Window",
                                 NULL, 0, OnEvent, NULL);
   if(!win)
   {
      fprintf(stderr, "testwin: test failed: W_CreateWindow() failed\n");
      return 1;
   }

   while(!done)
      Po_Sleep(1);

   W_FreeWindow(win);
   W_Quit();
   printf("testwin: test succeeded\n");
   return 0;
}

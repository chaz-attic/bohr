/******************************************************************************
* Tungsten - a portable OpenGL window library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/

#include <bohr/w.h>
#include <stdio.h>


int main(int argc, char * argv[])
{
   if(!W_Init())
   {
      fprintf(stderr, "testenum: test failed: W_Init() failed\n");
      return 1;
   }

   int num_monitors;
   if(!(num_monitors = W_GetNumMonitors()))
   {
      fprintf(stderr, "testenum: test failed: W_GetNumMonitors() failed\n");
      return 1;
   }
   for(int i = 0; i < num_monitors; ++i)
   {
      W_monitor_info info;
      if(!W_GetMonitorInfo(i, &info))
      {
         fprintf(stderr, "testenum: test failed: W_GetMonitorInfo() "
                         "failed for monitor %d\n", i);
         return 1;
      }
      printf("%2d: %s on %s%s%s\n", i, info.monitor_name, info.adapter_name,
                                  (info.primary ? " (primary)" : ""),
                                  (info.windowable ? " (windowable)" : ""));
      int num_modes;
      if(!(num_modes = W_GetNumModes(i)))
      {
         fprintf(stderr, "testenum: test failed: W_GetNumModes() "
                         "failed for monitor %d\n", i);
         return 1;
      }
      for(int j = 0; j < num_modes; ++j)
      {
         W_mode mode;
         if(!W_GetMode(i, j, &mode))
         {
            fprintf(stderr, "testenum: test failed: W_GetMode() failed "
                            "for monitor %d, mode %d\n", i, j);
            return 1;
         }
         printf("  %dx%dx%dx%d\n", mode.width, mode.height,
                                   mode.bpp, mode.refresh);
      }
   }

   W_Quit();
   return 0;
}

/******************************************************************************
* Tungsten - a portable OpenGL window library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#include "internal.h"
#include <bohr/w.h>

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <X11/Xlib.h>
#include <GL/glx.h>


//The W_window struct, context passed around as a window handle.
struct window_context
{
   GLXFBConfig fb_config; //the buffer format
   Window      wnd;       //the window id itself
   Colormap    cmap;      //the window's colormap
   GLXWindow   glxwnd;    //the GLX window
   pthread_t   thread;    //the thread the window runs in

   W_callback_fn   notify;           //the user's callback
   void          * notify_app_param; //its param
};

//Holds information passed into the TungstenWindowThread() function.
typedef struct window_thread_params
{
   volatile int * p_ready;
   volatile int * p_ok;

   Window wnd;

   int        fullscreen;
   W_mode   * mode;
   W_format * format;

   W_callback_fn   notify;
   void          * notify_app_param;

} window_thread_params;


static int g_ref_count = 0; //how many times W_Init() has been called

static Display * g_display = NULL; //our one connection to the X server


/* Returns the version of the Tungsten interface this library supports.
 * Compare with W_HEADER_VERSION.
 */
W_PUBLIC uint32_t W_GetVersion(void)
{
   return W_HEADER_VERSION;
}

/* Returns a human-readable name of this implementation of Tungsten, in the
 * following format:
 *    OS[/Subsystem][ (comment)]
 * where each part in []s is optional.
 */
W_PUBLIC const char * W_GetImplName(void)
{
   return "POSIX/Xlib (Bohr v0.0 alpha)";
}

/* Reference-counted initialization.  You must call W_Quit() the same number of
 * times that you call this, regardless of if it succeeds.  Returns 0/nonzero
 * on failure/success.
 */
W_PUBLIC int W_Init(void)
{
   if(++g_ref_count == 1)
   {
      if(!XInitThreads())
         return 0;
      if(!(g_display = XOpenDisplay(NULL)))
         return 0;
   }
   return 1;
}

/* Reference-counted cleanup.  Call this function the same number of times you
 * call W_Init().
 */
W_PUBLIC void W_Quit(void)
{
   if(--g_ref_count == 0)
   {
      if(g_display)
      {
         XCloseDisplay(g_display);
         g_display = NULL;
      }
   }
}

/* Returns the number of monitors attached to the system.  Returns 0 on
 * failure.
 */
W_PUBLIC int W_GetNumMonitors(void)
{
   return ScreenCount(g_display);
}

/* Returns information about the monitor with the given index.  Returns 0/
 * nonzero on failure/success.
 */
W_PUBLIC int W_GetMonitorInfo(int monitor,
                              W_monitor_info * restrict info_out)
{
   if(!info_out || monitor < 0 || monitor >= ScreenCount(g_display))
      return 0;

   //FIXME: this is garbage; I need some way of actually getting this info--it
   //looks like the xrandr extension may be the way to go about this
   snprintf(info_out->monitor_name, W_NAME_SIZE, "Screen %d", monitor);
   info_out->monitor_name[W_NAME_SIZE-1] = '\0';
   snprintf(info_out->adapter_name, W_NAME_SIZE, "Unknown Adapter");
   info_out->adapter_name[W_NAME_SIZE-1] = '\0';
   info_out->primary = (monitor == DefaultScreen(g_display));
   info_out->windowable = 1;

   return 1;
}

/* Returns the number of modes available for the monitor with the given index.
 * Returns 0 on failure.
 */
W_PUBLIC int W_GetNumModes(int monitor)
{
   if(monitor < 0 || monitor >= ScreenCount(g_display))
      return 0;

   //FIXME: also garbage... we need a list of modes, not just the current
   return 1;
}

/* Returns the mode with the given index for the monitor with the given index.
 * Returns 0/nonzero on failure/success.
 */
W_PUBLIC int W_GetMode(int monitor,
                       int mode_index, W_mode * restrict mode_out)
{
   if(!mode_out || monitor < 0 || monitor >= ScreenCount(g_display)
   || mode_index < 0 || mode_index >= 1)
      return 0;

   //FIXME: more garbage
   mode_out->width = DisplayWidth(g_display, monitor);
   mode_out->height = DisplayHeight(g_display, monitor);
   mode_out->bpp = DisplayPlanes(g_display, monitor);
   mode_out->refresh = 0;
   return 1;
}

/* Refreshes the list of monitors/modes.  After you call this function, you'll
 * need to do your monitor/mode enumeration over again.  Returns 0/nonzero on
 * failure/success.
 */
W_PUBLIC int W_RefreshMonitors(void)
{
   //TODO: is there anything to do?
   return 1;
}

static void * TungstenWindowThread(void * restrict param)
{
   window_thread_params * params = (window_thread_params *)param;

   W_create_params cp;
   cp.fullscreen = params->fullscreen;
   memcpy(&cp.mode, params->mode, sizeof(W_mode));
   memcpy(&cp.format, params->format, sizeof(W_format));

   //Copy these here so that when params goes out of scope in the function that
   //creates this thread (it's stack local there), we still have these values.
   Window wnd = *(Window volatile *)&params->wnd;
   W_callback_fn notify = *(W_callback_fn volatile *)&params->notify;
   void * notify_app_param = *(void * volatile *)&params->notify_app_param;

   int ok = 1;
   if(notify)
      ok = notify(W_CREATE, &cp, notify_app_param);
   *params->p_ok = ok;
   *params->p_ready = 1;
   //And, any time now, the creating function can terminate.

   if(!ok)
      return NULL;

   XMapWindow(g_display, wnd);

   int hidden = 0;
   int focus = 1;
   while(1)
   {
      XEvent evt;

      XWindowEvent(g_display, wnd, ~0, &evt);

      switch(evt.type)
      {
      case Expose:
         if(notify && !notify(W_PAINT, NULL, notify_app_param))
            ; //TODO: something?
         break;

      case MapNotify:
      case UnmapNotify:
      case FocusIn:
      case FocusOut:
         if(evt.type == MapNotify || evt.type == UnmapNotify)
            hidden = (evt.type == UnmapNotify);
         if(evt.type == FocusIn || evt.type == FocusOut)
            focus = (evt.type == FocusIn);
         if(notify)
         {
            W_activate_params ap =
            {
               .focus = focus,
               .hidden = hidden
            };
            notify(W_ACTIVATE, &ap, notify_app_param);
         }
         break;

      case ConfigureNotify:
         if(notify)
         {
            W_resize_params rp =
            {
               .width = ((XConfigureEvent *)&evt)->width,
               .height = ((XConfigureEvent *)&evt)->height
            };
            notify(W_RESIZE, &rp, notify_app_param);
         }
         break;

      case DestroyNotify:
         printf("DestroyNotify event\n");
         break;

      case ReparentNotify:
         //ignore
         break;

      default:
         printf("Other event: %d\n", evt.type);
         break;
      }
   }

   return NULL;
}

static int CreateWindowAndColormap(int monitor, W_mode * restrict mode,
                                   GLXFBConfig fb_config,
                                   Window * restrict wnd_out,
                                   Colormap * restrict cmap_out)
{
   XVisualInfo * vi = NULL;
   Colormap cmap = None;
   Window wnd = None;

   int success = 0;
   do
   {
      vi = glXGetVisualFromFBConfig(g_display, fb_config);
      if(!vi)
         break;

      cmap = XCreateColormap(g_display, RootWindow(g_display, monitor),
                                      vi->visual, AllocNone);
      if(!cmap)
         break;

      XSetWindowAttributes swa =
      {
         .colormap = cmap,
         .event_mask = ExposureMask | FocusChangeMask | StructureNotifyMask
      };
      wnd = XCreateWindow(g_display, RootWindow(g_display, monitor),
                          0, 0, mode->width, mode->height, 0,
                          vi->depth, InputOutput, vi->visual,
                          CWColormap | CWEventMask, &swa);
      if(!wnd)
         break;

      Window dummy1;
      int dummy2, dummy3, dummy4;
      XGetGeometry(g_display, wnd, &dummy1, &dummy2, &dummy3,
                   (unsigned int *)&mode->width, (unsigned int *)&mode->height,
                   (unsigned int *)&dummy4, (unsigned int *)&mode->bpp);
      mode->refresh = 0;

      success = 1;
   } while(0);

   if(success)
   {
      *wnd_out = wnd;
      *cmap_out = cmap;
   }
   else
   {
      if(wnd)
         XDestroyWindow(g_display, wnd);
      if(cmap)
         XFreeColormap(g_display, cmap);
   }

   if(vi)
      XFree(vi);

   return success;
}

static GLXFBConfig GetBestFbConfig(int monitor, W_format * restrict format)
{
   int attribs[] =
   {
      GLX_RENDER_TYPE, GLX_RGBA_BIT, //TODO: palettes?
      GLX_RED_SIZE, format->red_bits, //ok if 0
      GLX_GREEN_SIZE, format->green_bits, //ok if 0
      GLX_BLUE_SIZE, format->blue_bits, //ok if 0
      GLX_ALPHA_SIZE, format->alpha_bits, //ok if 0
      GLX_DEPTH_SIZE, format->depth_bits,
      GLX_ACCUM_RED_SIZE, format->accum_bits/4,
      GLX_ACCUM_GREEN_SIZE, format->accum_bits/4,
      GLX_ACCUM_BLUE_SIZE, format->accum_bits/4,
      GLX_ACCUM_ALPHA_SIZE, format->accum_bits/4,
      GLX_STENCIL_SIZE, format->stencil_bits,
      GLX_DOUBLEBUFFER, (format->double_buffer ? True : False),
      GLX_AUX_BUFFERS, format->aux_buffers,
      GLX_CONFIG_CAVEAT, GLX_NONE, //TODO: is this the right thing?
      None
   };
   int num_configs = 0;
   GLXFBConfig * configs = glXChooseFBConfig(g_display, monitor,
                                             attribs, &num_configs);
   if(!configs || !num_configs)
      return NULL;

   GLXFBConfig best = configs[0];

   glXGetFBConfigAttrib(g_display, best, GLX_RED_SIZE, &format->red_bits);
   glXGetFBConfigAttrib(g_display, best, GLX_GREEN_SIZE, &format->green_bits);
   glXGetFBConfigAttrib(g_display, best, GLX_BLUE_SIZE, &format->blue_bits);
   glXGetFBConfigAttrib(g_display, best, GLX_ALPHA_SIZE, &format->alpha_bits);
   glXGetFBConfigAttrib(g_display, best, GLX_DEPTH_SIZE, &format->depth_bits);
   int ra, ga, ba, aa;
   glXGetFBConfigAttrib(g_display, best, GLX_ACCUM_RED_SIZE, &ra);
   glXGetFBConfigAttrib(g_display, best, GLX_ACCUM_GREEN_SIZE, &ga);
   glXGetFBConfigAttrib(g_display, best, GLX_ACCUM_BLUE_SIZE, &ba);
   glXGetFBConfigAttrib(g_display, best, GLX_ACCUM_ALPHA_SIZE, &aa);
   format->accum_bits = ra + ga + ba + aa;
   glXGetFBConfigAttrib(g_display, best, GLX_STENCIL_SIZE, &format->stencil_bits);
   glXGetFBConfigAttrib(g_display, best, GLX_DOUBLEBUFFER, &format->double_buffer);
   glXGetFBConfigAttrib(g_display, best, GLX_AUX_BUFFERS, &format->aux_buffers);

   //TODO: determine if freeing this memory invalidates what would be our
   //return value...
   XFree(configs);
   return best;
}

/* Creates a new Tungsten window, which will run in its own thread, on the
 * given monitor, with all the given whatever.  If fullscreen, the only parts
 * of mode that matter are the width and height.  Use NULL for mode or format
 * for some sensible defaults.
 */
W_PUBLIC W_window W_CreateWindow(int monitor,
                                 int fullscreen,
                                 W_mode * restrict mode,
                                 W_format * restrict format,
                                 const char * restrict title,
                                 const char * restrict icon_file,
                                 int hide_cursor,
                                 W_callback_fn notify,
                                 void * restrict notify_app_param)
{
   W_window win = NULL;

   int success = 0;
   do
   {
      if(monitor < 0 || monitor >= ScreenCount(g_display))
         break;

      //TODO: support fullscreen
      fullscreen = 0;

      //make some defaults, so the caller can pass NULL
      W_mode default_mode =
      {
         .width = 640,
         .height = 480,
         .bpp = 24,
         .refresh = 0
      };

      W_format default_format =
      {
         .red_bits = 8,
         .green_bits = 8,
         .blue_bits = 8,
         .alpha_bits = 8,
         .depth_bits = 16,
         .accum_bits = 0,
         .stencil_bits = 0,
         .double_buffer = 1,
         .aux_buffers = 0
      };

      if(!mode)
         mode = &default_mode;
      if(!format)
         format = &default_format;

      //start off with a zeroed window struct, and set a few initial things
      if(!(win = (W_window)calloc(1, sizeof(struct window_context))))
         break;
      win->notify = notify;
      win->notify_app_param = notify_app_param;

      if(!(win->fb_config = GetBestFbConfig(monitor, format)))
         break;

      if(!CreateWindowAndColormap(monitor, mode, win->fb_config,
                                  &win->wnd, &win->cmap))
         break;

      XStoreName(g_display, win->wnd, (title ? title : ""));

      //TODO: set icon, hide cursor, etc.

      if(!(win->glxwnd = glXCreateWindow(g_display, win->fb_config, win->wnd,
                                         NULL)))
         break;

      volatile int ready = 0;
      volatile int ok = 0;

      window_thread_params params =
      {
         .p_ready          = &ready,
         .p_ok             = &ok,
         .wnd              = win->wnd,
         .fullscreen       = fullscreen,
         .mode             = mode,
         .format           = format,
         .notify           = notify,
         .notify_app_param = notify_app_param
      };
      if(pthread_create(&win->thread, NULL, TungstenWindowThread, &params))
         break;

      while(!ready)
      {
         struct timespec ts = {.tv_sec = 0, .tv_nsec = 100000000}; //100ms
         nanosleep(&ts, NULL);
      }
      if(!ok)
         break;

      success = 1;
   } while(0);

   if(!success)
   {
      //TODO: what else?
      if(win)
      {
         free(win);
         win = NULL;
      }
   }

   return win;
}

/* Closes and frees resources associated with the window.  Note that the window
 * can already have been closed, such as through user interaction; in that case
 * this still must be called to free all resources.  Note that you MUST NOT
 * call this function from within the callback procedure for your window, or
 * the thread will deadlock and nothing will be freed.  Instead, call it from
 * anywhere else, such as from the thread that called W_CreateWindow().
 */
W_PUBLIC void W_FreeWindow(W_window restrict win)
{
   if(win)
   {
      pthread_join(win->thread, NULL);

      if(win->glxwnd)
         glXDestroyWindow(g_display, win->glxwnd);
      if(win->wnd)
         XDestroyWindow(g_display, win->wnd);
      if(win->cmap)
         XFreeColormap(g_display, win->cmap);

      free(win);
   }
}

//TODO: need some way of switching modes in existing windows

/* Fills id_out with the platform-specific window handle/id for the given
 * W_window.  For Windows, this is an HWND: id_out must point to an HWND and
 * size_id_out must be sizeof(HWND).  I haven't figured out other platforms
 * yet.  This is mostly for other platform-specific Bohr modules, not really
 * for applications to use.  Returns 0/nonzero on failure/success.
 */
W_PUBLIC int W_GetPlatformWindowId(W_window restrict win,
                                   size_t size_id_out, void * restrict id_out)
{
   if(!win || size_id_out != sizeof(Window) || !id_out)
      return 0;

   memcpy(id_out, &win->wnd, sizeof(Window));
   return 1;
}

/* Posts an event to the given window.  The action must be either W_PAINT or
 * W_CLOSE, and action_params is ignored--for now.  In the future, you may be
 * able to resize the window with this function too.  Returns 0/nonzero on
 * failure/success (success being that the event was successfully posted).  The
 * event will be processed in another thread at a slightly later time.
 */
W_PUBLIC int W_PostEvent(W_window restrict win,
                         W_action action, const void * restrict action_params)
{
   //TODO
   return 0;
}

/* Creates, sets for the current thread, and returns a GL rendering context.
 * You must call this function from every thread that will be calling any
 * OpenGL functions, before it calls any OpenGL functions.  Each thread can be
 * attached to only one window at a time; I assume that multiple threads can be
 * attached to the same window at the same time.  Returns NULL on failure.
 */
W_PUBLIC W_gl_context W_AttachGl(W_window restrict win)
{
   if(win && win->fb_config && win->glxwnd)
   {
      //TODO: support palettes?
      GLXContext ctx = glXCreateNewContext(g_display, win->fb_config,
                                           GLX_RGBA_TYPE, NULL, True);
      if(ctx && !glXMakeContextCurrent(g_display, win->glxwnd, win->glxwnd,
                                       ctx))
      {
         glXDestroyContext(g_display, ctx);
         ctx = NULL;
      }
      return ctx;
   }

   return NULL;
}

/* Detaches the current thread from the given W_gl_context, and frees it.
 */
W_PUBLIC void W_DetachGl(W_gl_context restrict ctx)
{
   if(ctx)
   {
      glXMakeContextCurrent(g_display, None, None, NULL);
      glXDestroyContext(g_display, ctx);
   }
}

/* For double-buffered window formats, flips the back buffer (which all OpenGL
 * calls render to) with the front buffer.  You should flush any OpenGL calls
 * to the same window in other threads before calling this.  Note that you must
 * only call this for double-buffered formats--calling it on a non-double-
 * buffered format has undefined results.
 */
W_PUBLIC int W_Flip(W_window restrict win)
{
   if(win && win->glxwnd)
   {
      glXSwapBuffers(g_display, win->glxwnd);
      return 1;
   }

   return 0;
}

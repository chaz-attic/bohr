/******************************************************************************
* Tungsten - a portable OpenGL window library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#ifndef __internal_h__
#define __internal_h__

#include <GL/glx.h>


//Controls export behavior.
#define W_PUBLIC __attribute__ ((visibility ("default")))
#define W_PRIVATE __attribute__ ((visibility ("hidden")))

//Nix non-critical C99 keywords in compilers that don't support them.
#if((!defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
 && !defined(restrict))
#  define restrict
#endif


//Define the W_window as a pointer to our window_context struct, defined in
//tungsten.c.  It must stay sizeof(void *).
typedef struct window_context * W_window;
#define _W_WINDOW_DEFINED

//We'll define W_gl_context here as a GLXContext.  I'm assuming that GLXContext
//is sizeof(void *), 'cause if not, it could cause problems.
typedef GLXContext W_gl_context;
#define _W_GL_CONTEXT_DEFINED


#endif

/******************************************************************************
* Tungsten - a portable OpenGL window library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#include "internal.h"
#include <bohr/w.h>

#include <stddef.h>
#include <string.h>
#include <windows.h>

//MinGW apparently misses these definitions.
#ifndef DISPLAY_DEVICE_ACTIVE
#define DISPLAY_DEVICE_ACTIVE 1
#endif
#ifndef DISPLAY_DEVICE_ATTACHED
#define DISPLAY_DEVICE_ATTACHED 2
#endif


//The size in TCHARs of device ID strings as returned by Windows.
#define DEVICE_ID_SIZE 32

//Holds everything we need to know about monitors.
typedef struct win_monitor_info
{
   TCHAR device_id[DEVICE_ID_SIZE]; //Windows' "device name" for the device

   W_monitor_info w_info; //the stuff Tungsten needs to expose to the user

   int      num_modes; //how many modes the device has
   W_mode * modes;     //all the modes

} win_monitor_info;


//Define a vector that holds win_monitor_infos.
#define Ds_VECTOR_BEHAVIOR 2
#define Ds_VECTOR_TYPE win_monitor_info
#define Ds_VECTOR_SUFFIX _wmi
#include <bohr/ds_vector.h>

//Define a vector that holds W_modes.
#define Ds_VECTOR_BEHAVIOR 2
#define Ds_VECTOR_TYPE W_mode
#define Ds_VECTOR_SUFFIX _m
#include <bohr/ds_vector.h>


static int                g_num_monitors  = 0;    //how many monitors
static win_monitor_info * g_monitor_infos = NULL; //info on all of 'em


/* Reorders the list of infos if necessary so that the first monitor is always
 * the primary one.  Makes it easy to ignore the monitor enumeration subsystem.
 */
static inline void EnsurePrimaryIsFirst(win_monitor_info * restrict infos,
                                        int num)
{
   if(!infos[0].w_info.primary)
   {
      for(int i = 1; i < num; ++i)
      {
         if(infos[i].w_info.primary)
         {
            win_monitor_info temp;
            memcpy(&temp, &infos[0], sizeof(win_monitor_info));
            memcpy(&infos[0], &infos[i], sizeof(win_monitor_info));
            memcpy(&infos[i], &temp, sizeof(win_monitor_info));
            break;
         }
      }
   }
}

/* Initializes the enumeration subsystem.  Returns 0/nonzero on failure/
 * success.
 */
W_PRIVATE int InitEnumeration(void)
{
   //Windows has a RETARDED way of enumerating monitors/video cards.  WOW.  See
   //if you can follow this code.  Good luck.

   //set up a vector to hold what we find
   Ds_vector_wmi infos = Ds_VECTOR_INIT;
   if(!Ds_InitVector_wmi(&infos, 4))
      return 0;

   //This enumerates "display devices", which are actually monitors, but named
   //as the video card they're attached to.
   DISPLAY_DEVICE dd = {.cb = sizeof(DISPLAY_DEVICE)};
   for(int i = 0; EnumDisplayDevices(NULL, i, &dd, 0); ++i)
   {
      //ignore devices we don't care about
      if(dd.StateFlags & DISPLAY_DEVICE_MIRRORING_DRIVER)
         continue;

      //TODO: support "independent displays" by not requiring displays to be
      //attached to the desktop.  Independent displays would only support
      //fullscreen rendering, since the window manager doesn't go there.  It'd
      //work (well, if it works) by using CreateDC() to create a DC for the
      //device itself, and passing that to wglCreateContext().  This might be
      //weird since there wouldn't actually be a window created, so it wouldn't
      //get any messages, etc.  Somethin' to think about, anyway.  See "Using
      //Multiple Monitors as Independent Displays"
      //<http://msdn2.microsoft.com/en-us/library/ms534607(VS.85).aspx>.
      if(!(dd.StateFlags & DISPLAY_DEVICE_ATTACHED_TO_DESKTOP))
         continue;

      //Now, we've got a valid "device", but to figure out the name of the
      //monitor it represents, we have to enumerate POTENTIAL monitors attached
      //to it.  What the fuck?
      int found_monitor = 0;
      DISPLAY_DEVICE dd_monitor = {.cb = sizeof(DISPLAY_DEVICE)};
      for(int j = 0;
          EnumDisplayDevices(dd.DeviceName, j, &dd_monitor, 0);
          ++j)
      {
         //The actual monitor apparently has these flags set--is this
         //documented anywhere, like the docs for "DISPLAY_DEVICE"
         //<http://msdn2.microsoft.com/en-us/library/ms533254(VS.85).aspx>?
         //No.  You gotta google around to figure it out.
         if((dd_monitor.StateFlags & DISPLAY_DEVICE_ACTIVE)
         && (dd_monitor.StateFlags & DISPLAY_DEVICE_ATTACHED))
         {
            found_monitor = 1;
            break;
         }
      }

      //set up the info, so we can add it to our vector
      win_monitor_info info;
      memcpy(info.device_id, dd.DeviceName, DEVICE_ID_SIZE * sizeof(TCHAR));

//compile-time sanity checks
#if(W_NAME_SIZE != 128)
#  error W_NAME_SIZE must be exactly 128
#endif
#if(DEVICE_ID_SIZE != 32)
#  error DEVICE_ID_SIZE must be exactly 32
#endif

      if(!dd.DeviceString[0])
         memcpy(dd.DeviceString, TEXT("Default Device"), 15 * sizeof(TCHAR));
      if(!found_monitor || !dd_monitor.DeviceString[0])
         memcpy(dd_monitor.DeviceString,
                TEXT("Default Monitor"), 16 * sizeof(TCHAR));

      //set up the w_info device names
#ifdef UNICODE
      if(!WideCharToMultiByte(CP_UTF8, 0,
                              dd_monitor.DeviceString, -1,
                              info.w_info.monitor_name, W_NAME_SIZE,
                              NULL, NULL)
      || !WideCharToMultiByte(CP_UTF8, 0,
                              dd.DeviceString, -1,
                              info.w_info.adapter_name, W_NAME_SIZE,
                              NULL, NULL))
      {
         Ds_FreeVector_wmi(&infos);
         return 0;
      }
      info.w_info.monitor_name[W_NAME_SIZE-1] = '\0';
      info.w_info.adapter_name[W_NAME_SIZE-1] = '\0';
#else
      memcpy(info.w_info.monitor_name,
             dd_monitor.DeviceString,
             W_NAME_SIZE * sizeof(char));
      memcpy(info.w_info.adapter_name,
             dd.DeviceString,
             W_NAME_SIZE * sizeof(char));
#endif

      //and the w_info flags
      info.w_info.primary = ((dd.StateFlags &
                              DISPLAY_DEVICE_PRIMARY_DEVICE) != 0);

      //always true until we allow "independent displays":
      info.w_info.windowable = 1;

      //set up modes information as blank
      info.num_modes = 0;
      info.modes = NULL;

      //now, our info struct is ready to go, so add it to the vector
      if(!Ds_InsertVectorItems_wmi(&infos, &info, 1, Ds_VECTOR_END))
      {
         Ds_FreeVector_wmi(&infos);
         return 0;
      }
   }

   //fail if we didn't find anything or we fail to shrink the buffer down to
   //just the right size
   if(!infos.num || !Ds_ResizeVector_wmi(&infos, infos.num))
   {
      Ds_FreeVector_wmi(&infos);
      return 0;
   }

   //save our list as the global list
   g_num_monitors = infos.num;
   g_monitor_infos = infos.buf;

   //double check that monitor[0] is primary
   EnsurePrimaryIsFirst(g_monitor_infos, g_num_monitors);

   return 1;
}

/* Frees the enumeration subsystem.
 */
W_PRIVATE void QuitEnumeration(void)
{
   if(g_monitor_infos)
   {
      for(int i = 0; i < g_num_monitors; ++i)
      {
         if(g_monitor_infos[i].modes)
            free(g_monitor_infos[i].modes);
      }
      free(g_monitor_infos);
      g_monitor_infos = NULL;
   }
   g_num_monitors = 0;
}

/* Returns the top left corner of the given monitor for window creation.  The
 * coords returned are only valid RIGHT THEN, and especially may be invalid if
 * you change any monitor's resolution after calling this.
 */
W_PRIVATE int GetMonitorTopLeftCoords(int monitor,
                                      int * restrict x, int * restrict y)
{
   if(monitor < 0 || monitor >= g_num_monitors)
      return 0;

   DEVMODE dm =
   {
      .dmSize = sizeof(DEVMODE),
      .dmDriverExtra = 0,
   };
   //for some reason, GCC didn't like me putting members of anonymous structs
   //in the initializer list, so we initialize dmPosition separately
   dm.dmPosition.x = 0;
   dm.dmPosition.y = 0;
   if(!EnumDisplaySettingsEx(g_monitor_infos[monitor].device_id,
                             ENUM_CURRENT_SETTINGS, &dm, 0))
      return 0;

   *x = dm.dmPosition.x;
   *y = dm.dmPosition.y;
   return 1;
}

/* Goes into a fullscreen mode for the given monitor.
 */
W_PRIVATE int GoFullscreen(int monitor, const W_mode * restrict mode)
{
   if(monitor < 0 || monitor >= g_num_monitors)
      return 0;

   DEVMODE dm =
   {
      .dmSize        = sizeof(DEVMODE),
      .dmDriverExtra = 0,
      .dmPelsWidth   = mode->width,
      .dmPelsHeight  = mode->height,
      .dmBitsPerPel  = mode->bpp,
      .dmFields      = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL
   };
   if(mode->refresh)
   {
      dm.dmDisplayFrequency = mode->refresh;
      dm.dmFields |= DM_DISPLAYFREQUENCY;
   }

   return (ChangeDisplaySettingsEx(g_monitor_infos[monitor].device_id,
                                   &dm, NULL, CDS_FULLSCREEN,
                                   NULL) == DISP_CHANGE_SUCCESSFUL);
}

/* Returns from fullscreen mode.
 */
W_PRIVATE void ReturnFromFullscreen(int monitor)
{
   if(monitor >= 0 && monitor < g_num_monitors)
   {
      ChangeDisplaySettingsEx(g_monitor_infos[monitor].device_id,
                              NULL, NULL, 0, NULL);
   }
}

/* Returns 0/nonzero if the monitor is not windowable/is windowable.
 */
W_PRIVATE int IsMonitorWindowable(int monitor)
{
   if(monitor < 0 || monitor >= g_num_monitors)
      return 0;

   return g_monitor_infos[monitor].w_info.windowable;
}

/* Returns the number of monitors attached to the system.  Returns 0 on
 * failure.
 */
W_PUBLIC int W_GetNumMonitors(void)
{
   return g_num_monitors;
}

/* Returns information about the monitor with the given index.  Returns 0/
 * nonzero on failure/success.
 */
W_PUBLIC int W_GetMonitorInfo(int monitor,
                              W_monitor_info * restrict info_out)
{
   if(monitor < 0 || monitor >= g_num_monitors || !info_out)
      return 0;

   memcpy(info_out, &g_monitor_infos[monitor].w_info, sizeof(W_monitor_info));
   return 1;
}

/* Makes sure that there are some modes in the info struct.
 */
static int CacheModes(win_monitor_info * restrict info)
{
   if(info->modes)
      return 1;

   Ds_vector_m modes = Ds_VECTOR_INIT;
   if(!Ds_InitVector_m(&modes, 32))
      return 0;

   DEVMODE dm = {.dmSize = sizeof(DEVMODE), .dmDriverExtra = 0};
   for(int i = 0; EnumDisplaySettings(info->device_id, i, &dm); ++i)
   {
      //ignore modes with weird flags
      if(dm.dmDisplayFlags & DM_GRAYSCALE || dm.dmDisplayFlags & DM_INTERLACED)
         continue;

      W_mode mode;
      mode.width   = dm.dmPelsWidth;
      mode.height  = dm.dmPelsHeight;
      mode.bpp     = dm.dmBitsPerPel;
      mode.refresh = (dm.dmDisplayFrequency == 1 ?
                      0 :
                      dm.dmDisplayFrequency);

      if(!Ds_InsertVectorItems_m(&modes, &mode, 1, Ds_VECTOR_END))
      {
         Ds_FreeVector_m(&modes);
         return 0;
      }
   }

   if(!modes.num || !Ds_ResizeVector_m(&modes, modes.num))
   {
      Ds_FreeVector_m(&modes);
      return 0;
   }

   info->num_modes = modes.num;
   info->modes = modes.buf;
   return 1;
}

/* Returns the number of modes available for the monitor with the given index.
 * Returns 0 on failure.
 */
W_PUBLIC int W_GetNumModes(int monitor)
{
   if(monitor < 0 || monitor >= g_num_monitors
   || !CacheModes(&g_monitor_infos[monitor]))
      return 0;

   return g_monitor_infos[monitor].num_modes;
}

/* Returns the mode with the given index for the monitor with the given index.
 * Returns 0/nonzero on failure/success.
 */
W_PUBLIC int W_GetMode(int monitor,
                       int mode_index, W_mode * restrict mode_out)
{
   if(monitor < 0 || monitor >= g_num_monitors || mode_index < 0 || !mode_out
   || !CacheModes(&g_monitor_infos[monitor])
   || mode_index >= g_monitor_infos[monitor].num_modes)
      return 0;

   memcpy(mode_out, &g_monitor_infos[monitor].modes[mode_index],
          sizeof(W_mode));
   return 1;
}

/* Refreshes the list of monitors/modes.  After you call this function, you'll
 * need to do your monitor/mode enumeration over again.  Returns 0/nonzero on
 * failure/success.
 */
W_PUBLIC int W_RefreshMonitors(void)
{
   QuitEnumeration();
   return InitEnumeration();
}

/******************************************************************************
* Tungsten - a portable OpenGL window library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#ifndef __internal_h__
#define __internal_h__

#define _WIN32_WINNT 0x0500 //for EnumDisplaySettingsEx()

#include <windows.h>


//Controls export behavior.
#define W_PUBLIC  __declspec(dllexport)
#define W_PRIVATE

//Nix non-critical C99 keywords in compilers that don't support them.
#if((!defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
 && !defined(restrict))
#  define restrict
#endif


//Define the W_window as a pointer to our window_context struct, defined in
//tungsten.c.  It must stay sizeof(void *).
typedef struct window_context * W_window;
#define _W_WINDOW_DEFINED

//We'll define W_gl_context internally to be an HGLRC handle from Windows.
typedef HGLRC W_gl_context;
#define _W_GL_CONTEXT_DEFINED


//Declare this here so the compiler doesn't whine about it in parameter lists
//below.
struct W_mode;


//Defined in monitors.c:
int InitEnumeration(void);
void QuitEnumeration(void);
int GetMonitorTopLeftCoords(int monitor, int * restrict x, int * restrict y);
int GoFullscreen(int monitor, const struct W_mode * restrict mode);
void ReturnFromFullscreen(int monitor);
int IsMonitorWindowable(int monitor);


#endif

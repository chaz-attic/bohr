/******************************************************************************
* Tungsten - a portable OpenGL window library
* Part of the Bohr Game Libraries <https://github.com/chazomaticus/bohr>
* Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
******************************************************************************/


#include "internal.h"
#include <bohr/w.h>

#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <windows.h>
#include <process.h>


//The window class name to use.  Try to keep window class names unique, if you
//implement another Tungsten module in Windows.
#define WINDOW_CLASS_NAME TEXT("TungstenNativeBohr")

//A "user" window message we use to invalidate the window's client area, so the
//W_PAINT action is triggered.
#define WM_USER_PAINT (WM_USER + 1)

//A "user" window message used internally here to send windows a W_RESIZE in
//the correct order regardless of whether they're in a fullscreen mode.  This
//kind of a weird thing on Windows' part; all relevant sections are labeled
//with the phrase "FULLSCREEN WM_SIZE HACK".  Here, the window's width will be
//in wParam, height in lParam.
#define WM_USER_SIZE (WM_USER + 2)


//Holds anything we need to keep track of per-window that needs to be
//accessible from the window proc.  A pointer is allocated as extra window
//bytes for each HWND of our window class.
typedef struct window_extra
{
   W_callback_fn   notify;           //the user's callback
   void          * notify_app_param; //its param
   int             closed;           //whether the user has closed the window

} window_extra;

//The W_window struct, context passed around as a window handle.
struct window_context
{
   HWND   hWnd;    //the window itself
   HDC    hDC;     //the window's DC
   HANDLE hThread; //the thread the window runs in

   window_extra extra; //information shared in TungstenWindowProc()
};

//Holds information passed into the TungstenWindowThread() function.
typedef struct window_thread_params
{
   HWND         * p_hWnd;           //pointer to window handle
   HDC          * p_hDC;            //pointer to DC handle
   window_extra * p_extra;          //pointer to the extra bytes for the window
   HANDLE         hCreateDoneEvent; //gets signaled after window creation

   int monitor; //all are the same as passed to W_CreateWindow()
   int fullscreen;
   W_mode * mode;
   W_format * format;
   const char * title;
   const char * icon_file;
   int hide_cursor;
   W_callback_fn notify;
   void * notify_app_param;

} window_thread_params;

//Context necessary for the window thread.  Holds information between window
//creation/destruction.
typedef struct window_thread_context
{
   HWND  * p_hWnd; //pointer to the window handle
   HDC   * p_hDC;  //pointer to DC handle

   HICON hIconBig; //big icon
   HICON hIconSm;  //small icon

   int monitor;     //what monitor it was on
   int fullscreen;  //whether they went fullscreen
   int hide_cursor; //whether they wanted the cursor hidden

} window_thread_context;


static int g_ref_count = 0; //how many times W_Init() has been called


/* Returns the version of the Tungsten interface this library supports.
 * Compare with W_HEADER_VERSION.
 */
W_PUBLIC uint32_t W_GetVersion(void)
{
   return W_HEADER_VERSION;
}

/* Returns a human-readable name of this implementation of Tungsten, in the
 * following format:
 *    OS[/Subsystem][ (comment)]
 * where each part in []s is optional.
 */
W_PUBLIC const char * W_GetImplName(void)
{
#ifdef UNICODE
   return "WindowsNT/native (Bohr v0.1)";
#else
   return "Windows/native (Bohr v0.1)";
#endif
}

/* The window callback procedure for Tungsten windows.
 */
static LRESULT CALLBACK TungstenWindowProc(HWND hWnd, UINT uMsg,
                                           WPARAM wParam, LPARAM lParam)
{
   window_extra * ctx = (window_extra *)GetWindowLongPtr(hWnd, 0);

   switch(uMsg)
   {
   case WM_PAINT:
      assert(ctx != NULL);

      if(ctx->notify && !ctx->notify(W_PAINT, NULL, ctx->notify_app_param))
         return 0;
      break;

   case WM_SIZE:
      //FULLSCREEN WM_SIZE HACK: turns out, if you go fullscreen, Windows sends
      //a WM_SIZE message BEFORE CreateWindow() returns, which would cause ctx
      //to be NULL here.  So, we don't assert() that it's not NULL, we just
      //ignore this message if it is, and do some magic right after
      //CreateWindow() returns so that the application gets the messages in the
      //same order regardless.
      if(!ctx)
         break;

      if(ctx->notify && wParam != SIZE_MINIMIZED)
      {
         W_resize_params rp =
         {
            .width = LOWORD(lParam),
            .height = HIWORD(lParam)
         };

         ctx->notify(W_RESIZE, &rp, ctx->notify_app_param);
      }
      break;

   case WM_ACTIVATE:
      assert(ctx != NULL);

      //only process this if they didn't just close the window
      if(ctx->notify && !ctx->closed)
      {
         W_activate_params ap =
         {
            //it has focus if it's not being inactivated, and it's hidden only
            //if it's being minimized
            .focus = (LOWORD(wParam) != WA_INACTIVE),
            .hidden = (HIWORD(wParam) != 0)
         };

         ctx->notify(W_ACTIVATE, &ap, ctx->notify_app_param);
      }
      break;

   case WM_CLOSE:
      assert(ctx != NULL);

      if(ctx->notify && !ctx->notify(W_CLOSE, NULL, ctx->notify_app_param))
         return 0;

      //set the closed flag, so it doesn't get a spurious W_ACTIVATE event
      ctx->closed = 1;
      break;

   case WM_SYSCOMMAND:
      assert(ctx != NULL);

      switch(wParam & 0xfff0)
      {
      case SC_SCREENSAVE:
      case SC_MONITORPOWER:
         if(ctx->notify
         && !ctx->notify(W_SCREENSAVER, NULL, ctx->notify_app_param))
            return 0;
         break;
      }
      break;

   case WM_DESTROY:
      PostQuitMessage(0);
      return 0;

   case WM_USER_PAINT:
      InvalidateRect(hWnd, NULL, FALSE);
      return 0;

   //FULLSCREEN WM_SIZE HACK: since we block fullscreen windows' first WM_SIZE
   //event, since ctx will be NULL, we send the window this message after it's
   //created, to maintain the same message order regardless.
   case WM_USER_SIZE:
      assert(ctx != NULL);

      if(ctx->notify)
      {
         W_resize_params rp =
         {
            .width = wParam,
            .height = lParam
         };

         ctx->notify(W_RESIZE, &rp, ctx->notify_app_param);
      }
      return 0;
   }

   return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

/* Registers a window class for our OpenGL windows.
 */
static int RegisterWindowClass(void)
{
   return (RegisterClassEx(&(WNDCLASSEX){sizeof(WNDCLASSEX),
        /* don't change these styles: */ CS_HREDRAW | CS_VREDRAW | CS_OWNDC,
                                         TungstenWindowProc,
                                         0,
                                         sizeof(window_extra *),
                                         GetModuleHandle(NULL),
                                         NULL,
                                         LoadCursor(NULL, IDC_ARROW),
                                         NULL, NULL,
                                         WINDOW_CLASS_NAME,
                                         NULL}) != 0);
}

/* Reference-counted initialization.  You must call W_Quit() the same number of
 * times that you call this, regardless of if it succeeds.  Returns 0/nonzero
 * on failure/success.
 */
W_PUBLIC int W_Init(void)
{
   if(++g_ref_count == 1)
   {
      if(!RegisterWindowClass())
         return 0;
      return InitEnumeration();
   }
   return 1;
}

/* Unregisters the window class.
 */
static void UnregisterWindowClass(void)
{
   UnregisterClass(WINDOW_CLASS_NAME, GetModuleHandle(NULL));
}

/* Reference-counted cleanup.  Call this function the same number of times you
 * call W_Init().
 */
W_PUBLIC void W_Quit(void)
{
   if(--g_ref_count == 0)
   {
      QuitEnumeration();
      UnregisterWindowClass();
   }
}

/* Undoes everything done by CreateTungstenWindow(), below.  Called from the
 * thread running the window.
 */
static void DestroyTungstenWindow(window_thread_context * restrict ctx)
{
   if(ctx->p_hWnd && *ctx->p_hWnd)
   {
      if(ctx->p_hDC && *ctx->p_hDC)
      {
         ReleaseDC(*ctx->p_hWnd, *ctx->p_hDC);
         *ctx->p_hDC = NULL;
      }
      DestroyWindow(*ctx->p_hWnd);
      *ctx->p_hWnd = NULL;
   }
   if(ctx->hide_cursor)
      ShowCursor(TRUE);
   if(ctx->fullscreen)
      ReturnFromFullscreen(ctx->monitor);
   if(ctx->hIconBig)
      DestroyIcon(ctx->hIconBig);
   if(ctx->hIconSm)
      DestroyIcon(ctx->hIconSm);
}

/* The main "message pump" loop for threads running Tungsten windows.
 */
static int TungstenWindowLoop(void)
{
   MSG msg;
   BOOL bRet;

   while((bRet = GetMessage(&msg, NULL, 0, 0)) != 0)
   {
      if(bRet == -1)
      {
         msg.wParam = 1;
         break;
      }

      TranslateMessage(&msg);
      DispatchMessage(&msg);
   }

   return msg.wParam;
}

/* A wrapper around SetPixelFormat() and its ilk.  Sets the pixel format for
 * the DC, as close as possible to desired, and returns the actual format that
 * it's going to use.
 */
static int SetFormatWrapper(HDC hDC, W_format * restrict format)
{
   PIXELFORMATDESCRIPTOR pfd =
   {
      .nSize        = sizeof(PIXELFORMATDESCRIPTOR),
      .nVersion     = 1,
      .dwFlags      = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL,
      .iPixelType   = PFD_TYPE_RGBA, //TODO: support palettes?
      .cColorBits   = format->red_bits
                    + format->green_bits
                    + format->blue_bits,
      .cAlphaBits   = format->alpha_bits,
      .cDepthBits   = format->depth_bits,
      .cAccumBits   = format->accum_bits,
      .cStencilBits = format->stencil_bits,
      .cAuxBuffers  = format->aux_buffers,
      .iLayerType   = PFD_MAIN_PLANE
   };
   if(format->double_buffer)
      pfd.dwFlags |= PFD_DOUBLEBUFFER;
   if(!format->depth_bits)
      pfd.dwFlags |= PFD_DEPTH_DONTCARE;

   //set it as close as possible to what was desired
   int format_index;
   if(!(format_index = ChoosePixelFormat(hDC, &pfd))
   || !SetPixelFormat(hDC, format_index, &pfd))
      return 0;

   //it may not have been set with exactly what we wanted, so we have to read
   //what was actually set
   if(DescribePixelFormat(hDC, format_index, sizeof(pfd), &pfd))
   {
      format->red_bits      = pfd.cRedBits;
      format->green_bits    = pfd.cGreenBits;
      format->blue_bits     = pfd.cBlueBits;
      format->alpha_bits    = pfd.cAlphaBits;
      format->depth_bits    = pfd.cDepthBits;
      format->accum_bits    = pfd.cAccumBits;
      format->stencil_bits  = pfd.cStencilBits;
      format->double_buffer = ((pfd.dwFlags & PFD_DOUBLEBUFFER) != 0);
      format->aux_buffers   = pfd.cAuxBuffers;
   }

   return 1;
}

/* A wrapper around loading and setting the icon for the window.
 */
static int SetIconWrapper(HINSTANCE hInst, HWND hWnd,
                          const char * restrict icon_file,
                          HICON * restrict p_hIconBig,
                          HICON * restrict p_hIconSm)
{
#ifdef UNICODE
   TCHAR t_icon_file[MAX_PATH];
   if(!MultiByteToWideChar(CP_UTF8, 0,
                           icon_file, -1,
                           t_icon_file, MAX_PATH))
   {
      return 0;
   }
   t_icon_file[MAX_PATH-1] = TEXT('\0');
#else
#  define t_icon_file icon_file
#endif

   *p_hIconBig = (HICON)LoadImage(hInst, t_icon_file, IMAGE_ICON,
                                  GetSystemMetrics(SM_CXICON),
                                  GetSystemMetrics(SM_CYICON),
                                  LR_LOADFROMFILE);
   *p_hIconSm = (HICON)LoadImage(hInst, t_icon_file, IMAGE_ICON,
                                 GetSystemMetrics(SM_CXSMICON),
                                 GetSystemMetrics(SM_CYSMICON),
                                 LR_LOADFROMFILE);

#ifndef UNICODE
#  undef t_icon_file
#endif

   if(!*p_hIconBig || !*p_hIconSm)
   {
      if(*p_hIconBig)
         DestroyIcon(*p_hIconBig);
      if(*p_hIconSm)
         DestroyIcon(*p_hIconSm);
      return 0;
   }

   //I guess this is how you set icons in Windows?
   SendMessage(hWnd, WM_SETICON, ICON_BIG, (LPARAM)*p_hIconBig);
   SendMessage(hWnd, WM_SETICON, ICON_SMALL, (LPARAM)*p_hIconSm);

   return 1;
}

/* A wrapper around actually creating the window.  It handles figuring out the
 * window coordinates and styles and everything.
 */
static HWND CreateWindowWrapper(HINSTANCE hInst, int monitor, int fullscreen,
                                const char * restrict title,
                                int * restrict width, int * restrict height)
{
#ifdef UNICODE
   TCHAR t_title[MAX_PATH];
   if(title)
   {
      if(!MultiByteToWideChar(CP_UTF8, 0,
                              title, -1,
                              t_title, MAX_PATH))
      {
         return NULL;
      }
      t_title[MAX_PATH-1] = TEXT('\0');
   }
   else
      t_title[0] = TEXT('\0');
#else
   LPCTSTR t_title;
   if(title)
      t_title = title;
   else
      t_title = TEXT("");
#endif

   //Wow, these styles names are TOTALLY MEANINGLESS to me.  Somehow, this
   //combination works magic.
   DWORD dwExStyle = WS_EX_APPWINDOW;
   DWORD dwStyle = WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
   if(fullscreen)
      dwStyle |= WS_POPUP;
   else
   {
      dwExStyle |= WS_EX_WINDOWEDGE;
      dwStyle |= WS_OVERLAPPEDWINDOW;
   }

   int left = 0, top = 0;
   GetMonitorTopLeftCoords(monitor, &left, &top);

   RECT rcWindow =
   {
      .left = left,
      .top = top,
      .right = left + *width,
      .bottom = top + *height
   };
   AdjustWindowRectEx(&rcWindow, dwStyle, FALSE, dwExStyle);

   HWND hWnd = CreateWindowEx(dwExStyle, WINDOW_CLASS_NAME, t_title, dwStyle,
                              left, top,
                              rcWindow.right - rcWindow.left,
                              rcWindow.bottom - rcWindow.top,
                              NULL, NULL, hInst, NULL);

   //if we didn't go fullscreen, we need to check the client size of the
   //created window, in case it wasn't the exact size we requested
   if(!fullscreen && hWnd)
   {
      //TODO: is this actually necessary, or will Windows always make it
      //exactly as big as you want it?
      RECT rcClient;
      if(GetClientRect(hWnd, &rcClient))
      {
         //I love how it's a client "rect", but the only information in it is
         //the width/height of the client area
         *width = rcClient.right;
         *height = rcClient.bottom;
      }

      //TODO: ALSO!  Get the screen's depth and refresh rate, to fill out the
      //mode, not like it really matters
   }

   return hWnd;
}

/* Uses the above wrappers to actually create the damned window.  This is
 * unfortunately heinous.  Why is it so hard to just make a damned window?
 */
static int CreateTungstenWindow(window_thread_params * restrict params,
                                window_thread_context * restrict ctx)
{
   //make sure the context is all 0s, so DestroyTungstenWindow() doesn't
   //mess anything up
   memset(ctx, 0, sizeof(window_thread_context));

   HINSTANCE hInst = GetModuleHandle(NULL);

   //attempt to go fullscreen if requested; if it fails, we'll just revert to
   //windowed mode
   if(params->fullscreen && !GoFullscreen(params->monitor, params->mode))
      params->fullscreen = 0;

   if(params->hide_cursor)
      ShowCursor(FALSE);

   //now that we know whether we actually went fullscreen or hid the cursor,
   //set these variables in the context so they'll be properly undone by
   //DestroyTungstenWindow()
   ctx->monitor = params->monitor;
   ctx->fullscreen = params->fullscreen;
   ctx->hide_cursor = params->hide_cursor;

   HWND hWnd = CreateWindowWrapper(hInst,
                                   params->monitor,
                                   params->fullscreen,
                                   params->title,
                                   &params->mode->width,
                                   &params->mode->height);
   if(!hWnd)
      return 0;

   //if we made the window ok, save the value in the original W_window
   //struct and in the context
   *params->p_hWnd = hWnd;
   ctx->p_hWnd = params->p_hWnd;

   //set up the window_extra information for the window... Windows really
   //makes this awkward by not reserving a return value for failure, hence
   //the Set/GetLastError() shenanigans
   SetLastError(0);
   SetWindowLongPtr(hWnd, 0, (LONG_PTR)params->p_extra);
   if(GetLastError())
      return 0;

   if(params->icon_file
   && !SetIconWrapper(hInst, hWnd, params->icon_file,
                      &ctx->hIconBig, &ctx->hIconSm))
      return 0;

   HDC hDC = GetDC(hWnd);
   if(!hDC)
      return 0;

   //if we got the DC, set it up in the original W_window and the context
   *params->p_hDC = hDC;
   ctx->p_hDC = params->p_hDC;

   if(!SetFormatWrapper(hDC, params->format))
      return 0;

   //call the notification function with all the required information
   if(params->notify)
   {
      W_create_params cp;
      cp.fullscreen = params->fullscreen;
      memcpy(&cp.mode, params->mode, sizeof(W_mode));
      memcpy(&cp.format, params->format, sizeof(W_format));

      //if they return false, we'll close this window RIGHT NOW, so help me
      if(!params->notify(W_CREATE, &cp, params->notify_app_param))
         return 0;
   }

   //finally, show it, give it priority, and make sure it's got the focus
   ShowWindow(hWnd, SW_SHOW);
   SetForegroundWindow(hWnd);
   SetFocus(hWnd);

   //FULLSCREEN WM_SIZE HACK: if we went fullscreen, we now need to send the
   //window a W_RESIZE event, so we use our special WM_USER_SIZE message to do
   //the trick.
   if(params->fullscreen)
   {
      PostMessage(hWnd, WM_USER_SIZE,
                  params->mode->width, params->mode->height);
   }

   return 1;
}

/* The main thread procedure for, well Tungsten window threads.  Makes the
 * window, tells the parent thread when it's ready, then captures events until
 * it's quittin' time.
 */
static unsigned int __stdcall TungstenWindowThread(void * restrict param)
{
   window_thread_params * params = (window_thread_params *)param;
   window_thread_context ctx;
   unsigned int rc;

   rc = !CreateTungstenWindow(params, &ctx);
   SetEvent(params->hCreateDoneEvent);

   if(!rc)
      rc = TungstenWindowLoop();

   DestroyTungstenWindow(&ctx);
   return rc;
}

/* Creates a new Tungsten window, which will run in its own thread, on the
 * given monitor, with all the given whatever.  If fullscreen, the only parts
 * of mode that matter are the width and height.  Use NULL for mode or format
 * for some sensible defaults.
 */
W_PUBLIC W_window W_CreateWindow(int monitor,
                                 int fullscreen,
                                 W_mode * restrict mode,
                                 W_format * restrict format,
                                 const char * restrict title,
                                 const char * restrict icon_file,
                                 int hide_cursor,
                                 W_callback_fn notify,
                                 void * restrict notify_app_param)
{
   W_window win              = NULL;
   HANDLE   hCreateDoneEvent = NULL;

   do
   {
      //make some defaults, so the caller can pass NULL
      W_mode default_mode =
      {
         .width = 640,
         .height = 480,
         .bpp = 24,
         .refresh = 0
      };

      W_format default_format =
      {
         .red_bits = 8,
         .green_bits = 8,
         .blue_bits = 8,
         .alpha_bits = 8,
         .depth_bits = 16,
         .accum_bits = 0,
         .stencil_bits = 0,
         .double_buffer = 1,
         .aux_buffers = 0
      };

      if(!mode)
         mode = &default_mode;
      if(!format)
         format = &default_format;

      //start off with a zeroed window struct, and set a few initial things
      if(!(win = (W_window)calloc(1, sizeof(struct window_context))))
         break;
      win->extra.notify = notify;
      win->extra.notify_app_param = notify_app_param;

      //make an event that the child thread will signal when it's done making
      //the window
      if(!(hCreateDoneEvent = CreateEvent(NULL, TRUE, FALSE, NULL)))
         break;

      window_thread_params params =
      {
         .p_hWnd           = &win->hWnd,
         .p_hDC            = &win->hDC,
         .p_extra          = &win->extra,
         .hCreateDoneEvent = hCreateDoneEvent,
         .monitor          = monitor,
         .fullscreen       = fullscreen,
         .mode             = mode,
         .format           = format,
         .title            = title,
         .icon_file        = icon_file,
         .hide_cursor      = hide_cursor,
         .notify           = notify,
         .notify_app_param = notify_app_param
      };

      //Now we actually start the child thread, which does all the real work.
      //Note that the thread params are stack-local here--that's why we need
      //the event to say when the child thread is done with 'em (the child
      //thread only uses them for creation, so this works fine).
      if(!(win->hThread = (HANDLE)_beginthreadex(NULL, 0, TungstenWindowThread,
                                                 &params, 0, NULL)))
         break;

      //wait for the thread to get done creating the window, so it's safe to
      //move on
      if(WaitForSingleObject(hCreateDoneEvent, INFINITE) != WAIT_OBJECT_0)
      {
         CloseHandle(win->hThread);
         if(win->hWnd)
         {
            //prevent them from getting W_CLOSE before we close/destroy it
            win->extra.notify = NULL;
            PostMessage(win->hWnd, WM_CLOSE, 0, 0);
            win->hWnd = NULL;
         }
      }
   } while(0);

   if(hCreateDoneEvent)
      CloseHandle(hCreateDoneEvent);
   if(win && !win->hWnd)
   {
      //TODO: clean up the thread?
      free(win);
      win = NULL;
   }

   return win;
}

/* Closes and frees resources associated with the window.  Note that the window
 * can already have been closed, such as through user interaction; in that case
 * this still must be called to free all resources.  Note that you MUST NOT
 * call this function from within the callback procedure for your window, or
 * the thread will deadlock and nothing will be freed.  Instead, call it from
 * anywhere else, such as from the thread that called W_CreateWindow().
 */
W_PUBLIC void W_FreeWindow(W_window restrict win)
{
   if(win)
   {
      //set the callback to NULL, so they don't get (and potentially cancel)
      //the W_CLOSE event
      win->extra.notify = NULL;

      //tell the thing to close, and wait for it
      PostMessage(win->hWnd, WM_CLOSE, 0, 0);
      WaitForSingleObject(win->hThread, INFINITE);

      //clean up
      CloseHandle(win->hThread);
      win->hThread = NULL;

      //note: win->hWnd and win->hDC are cleaned up as the child thread exits

      free(win);
   }
}

//TODO: need some way of switching modes in existing windows

/* Fills id_out with the platform-specific window handle/id for the given
 * W_window.  For Windows, this is an HWND: id_out must point to an HWND and
 * size_id_out must be sizeof(HWND).  I haven't figured out other platforms
 * yet.  This is mostly for other platform-specific Bohr modules, not really
 * for applications to use.  Returns 0/nonzero on failure/success.
 */
W_PUBLIC int W_GetPlatformWindowId(W_window restrict win,
                                   size_t size_id_out, void * restrict id_out)
{
   if(!win || size_id_out != sizeof(HWND) || !id_out)
      return 0;

   memcpy(id_out, &win->hWnd, sizeof(HWND));
   return 1;
}

/* Posts an event to the given window.  The action must be either W_PAINT or
 * W_CLOSE, and action_params is ignored--for now.  In the future, you may be
 * able to resize the window with this function too.  Returns 0/nonzero on
 * failure/success (success being that the event was successfully posted).  The
 * event will be processed in another thread at a slightly later time.
 */
W_PUBLIC int W_PostEvent(W_window restrict win,
                         W_action action, const void * restrict action_params)
{
   if(win && win->hWnd)
   {
      switch(action)
      {
         case W_PAINT:
            return (PostMessage(win->hWnd, WM_USER_PAINT, 0, 0) != 0);

         case W_CLOSE:
            return (PostMessage(win->hWnd, WM_CLOSE, 0, 0) != 0);
      }
   }

   return 0;
}

/* Creates, sets for the current thread, and returns a GL rendering context.
 * You must call this function from every thread that will be calling any
 * OpenGL functions, before it calls any OpenGL functions.  Each thread can be
 * attached to only one window at a time; I assume that multiple threads can be
 * attached to the same window at the same time.  Returns NULL on failure.
 */
W_PUBLIC W_gl_context W_AttachGl(W_window restrict win)
{
   if(win && win->hWnd && win->hDC)
   {
      HGLRC hRC = wglCreateContext(win->hDC);
      if(hRC && !wglMakeCurrent(win->hDC, hRC))
      {
         wglDeleteContext(hRC);
         hRC = NULL;
      }
      return hRC;
   }

   return NULL;
}

/* Detaches the current thread from the given W_gl_context, and frees it.
 */
W_PUBLIC void W_DetachGl(W_gl_context restrict ctx)
{
   if(ctx)
   {
      wglMakeCurrent(NULL, NULL);
      wglDeleteContext(ctx);
   }
}

/* For double-buffered window formats, flips the back buffer (which all OpenGL
 * calls render to) with the front buffer.  You should flush any OpenGL calls
 * to the same window in other threads before calling this.  Note that you must
 * only call this for double-buffered formats--calling it on a non-double-
 * buffered format has undefined results.
 */
W_PUBLIC int W_Flip(W_window restrict win)
{
   if(win && win->hWnd && win->hDC)
      return (SwapBuffers(win->hDC) != FALSE);

   return 0;
}

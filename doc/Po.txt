= Polonium =

Polonium (Po) is a portability interface.

Polonium is a thin layer between applications and some native system calls.  It's carefully constructed to allow you to write portable applications that can do more than is defined by the C standard library.  It's targeted at game developers, but really could be useful for just about any other application.  It's meant to be trivially easy to use but still powerful enough to do whatever you need to get done.

Polonium, being more of a system call wrapper, avoids other sectors that might need a portability interface, like GUIs, input devices, etc.  Those will be covered in other libraries.

For speed reasons, and because the code is pretty minuscule, it's implemented as (by default) inline functions.

== Sub-Modules ==

Polonium is composed of the following sub-modules, which all work independently of each other:

 * [wiki:Docs/Po/Dir Po_dir]: finding files and directories
 * [wiki:Docs/Po/Lib Po_lib]: library loading
 * [wiki:Docs/Po/Sync Po_sync]: thread synchronization primitives
 * [wiki:Docs/Po/Thread Po_thread]: thread management
 * [wiki:Docs/Po/Time Po_time]: high resolution timers

== Headers ==

{{{
#!c
#include <bohr/po.h>
}}}

`#include`-ing `bohr/po.h` also `#include`s all sub-modules.  If you only need one or two sub-modules, they're in their own headers, named the same as the module name, all lower case, with a `.h` suffix.  This, for example, loads only [wiki:Docs/Po/Thread Po_thread] and [wiki:Docs/Po/Sync Po_sync]:

{{{
#!c
#include <bohr/po_thread.h>
#include <bohr/po_sync.h>
}}}

== Requirements ==

For technical reasons, a file `#include`-ing `bohr/po.h` must define `_GNU_SOURCE`, or `_XOPEN_SOURCE` to `500` or `600`, '''before any headers''' are `#include`d.  This is most easily accomplished by adding `-D_GNU_SOURCE` or `-D_XOPEN_SOURCE=500` (or `-D_XOPEN_SOURCE=600`) to the compiler command line.  Note that [wiki:Docs/Po/Sync Po_sync] is the only sub-module with this requirement, so if you don't include all of Polonium, ignore this paragraph.

In Windows, only the standard Windows libraries are required.

In Unix, if you include all of Polonium, you'll need to link with `libdl`, `libpthread`, and `librt` (add `-ldl -lpthread -lrt` to GCC's command line).  Note that [wiki:Docs/Po/Lib Po_lib] is the only sub-module that requires `libdl`, [wiki:Docs/Po/Thread Po_thread] and [wiki:Docs/Po/Sync Po_sync] are the only sub-modules that require `libpthread`, and [wiki:Docs/Po/Sync Po_sync] is the only one that requires `librt`, so if you don't include all of Polonium you can adjust your linker flags accordingly.

== Notes ==

Because Polonium is (by default) inline C99 code, any code that includes any of Polonium's headers must be compiled with a C99 compiler.  If you use GCC, this means adding `-std=gnu99` to your compile command line.

== Reference ==

=== Behavior ===

 * [wiki:Docs/Po/Po_INLINE Po_INLINE]: controls inlining and scope of functions


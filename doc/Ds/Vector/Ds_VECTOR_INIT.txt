= `Ds_VECTOR_INIT` =

{{{
#!c
#define Ds_VECTOR_INIT {NULL, 0, 0}
}}}

Initializes declarations of type [wiki:Docs/Ds/Vector/Ds_vector Ds_vector].

A [wiki:Docs/Ds/Vector/Ds_vector Ds_vector] set to this compound literal value is in the same state as if it'd been initialized with [wiki:Docs/Ds/Vector/Ds_InitVector Ds_InitVector()] with `cap` passed as `0`.


= `Ds_VECTOR_BEHAVIOR` =

{{{
#!c
#define Ds_VECTOR_BEHAVIOR 2
}}}

Controls how a [wiki:Docs/Ds/Vector/Ds_vector Ds_vector] grows when its buffer is too small.

This macro controls how [wiki:Docs/Ds/Vector Ds_vector] operates.  Thus, if you wish to override its default, define it on the compiler command line or in your application's source files above any included headers.

Before data can be added to a [wiki:Docs/Ds/Vector/Ds_vector Ds_vector] using [wiki:Docs/Ds/Vector/Ds_InsertVectorItems Ds_InsertVectorItems()], that function must ensure there's enough space in the vector.  This macro controls exactly how the [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]'s buffer will grow when it needs to.  Since growing is a potentially slow operation (O(''n'')), this lets you tailor the [wiki:Docs/Ds/Vector Ds_vector] library to suit your efficiency needs.

If this macro has the integer value `1`, [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]s will expand if necessary to exactly the capacity needed to hold all the data being inserted.  Using this value means it's a few instructions faster every time it grows, but depending on what you're inserting, can mean it has to grow more often, slowing things down overall.  If you're only inserting data into your vectors once, this is a good choice.

If this macro has the integer value `2`, [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]s double in capacity each time they grow (here, `0` capacity is treated as `1` for obvious reasons).  This is a good balance between not wasting too much space and not needing to grow too often.  It's the default.  In most generic cases, it's probably the right choice.

If this macro has the integer value `4`, [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]s quadruple in capacity each time they grow (again, `0` capacity is treated as `1`).  This potentially wastes the most space but has to grow the least often.  If you know you're going to be adding lots and lots of data to vectors over multiple calls to [wiki:Docs/Ds/Vector/Ds_InsertVectorItems Ds_InsertVectorItems()], this is probably the right choice.

This macro is `#undef`ined at the bottom of `bohr/ds_vector.h`, to make it easier to re-`#define` later.  This is in expectation that you may need to `#include` the header multiple times.  See [wiki:Docs/Ds/Vector/Ds_VECTOR_SUFFIX Ds_VECTOR_SUFFIX] for details on how that works.


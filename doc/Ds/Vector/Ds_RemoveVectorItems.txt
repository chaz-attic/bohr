= `Ds_RemoveVectorItems()` =

{{{
#!c
void Ds_RemoveVectorItems(
   Ds_vector * v,
   Ds_VECTOR_TYPE * items,
   size_t num,
   size_t pos
);
}}}

Removes data from a [wiki:Docs/Ds/Vector/Ds_vector Ds_vector], optionally copying it elsewhere.

== Parameters ==

`v` (in) should point to the [wiki:Docs/Ds/Vector/Ds_vector Ds_vector] from which to remove.  Passing `NULL` is an error.

`items` (out) can point to a buffer, or it can be `NULL`.  If it points to a buffer, that buffer should have space for at least `num` items (size in bytes should be at least `num * sizeof(Ds_VECTOR_TYPE)`), and the removed items are copied into the buffer as they're taken out of the [wiki:Docs/Ds/Vector/Ds_vector Ds_vector].  If it's `NULL`, the removed items aren't copied anywhere, but simply discarded.

`num` (in) should be the number of items to remove from the [wiki:Docs/Ds/Vector/Ds_vector Ds_vector].

`pos` (in) should be the first index in the [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]'s buffer from where items are to be removed.  Thus, `pos` of `0` means remove the first `num` of the items in `v`'s buffer.  A value of `pos` that falls at the end (or after) of the items in `v`'s buffer will remove the last `num` items.  You can use the special value [wiki:Docs/Ds/Vector/Ds_VECTOR_END Ds_VECTOR_END] (which is defined as the largest possible `size_t` value) to ensure you're removing from the end of the buffer.  Any value in between is also acceptable.

== Return Values ==

Returns nothing.

== Notes ==

If `items` is non-`NULL`, the appropriate section of `v`'s buffer is simply `memcpy()`ed into it.  Then of course, any trailing items in `v` are copied down so that everything is sequential.

The number of items in `v`'s buffer (`v->num`) is decremented by `num` as passed to this function (or, if you pass `num` greater than `v->num`, `v->num` is simply set to `0`).

This function assumes that the items in `v`'s buffer are stored sequentially, from `v->buf[0]` to `v->buf[v->num-1]`.  If this assumptions isn't valid, don't use this function.

This function doesn't modify `v`'s capacity (`v->cap`).


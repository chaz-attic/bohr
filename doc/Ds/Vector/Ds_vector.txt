= `Ds_vector` =

{{{
#!c
typedef struct Ds_vector
{
   Ds_VECTOR_TYPE * buf;
   size_t           num;
   size_t           cap;

} Ds_vector;
}}}

A dynamic array, or vector.

== Initialization ==

Initialize to the [wiki:Docs/Ds/Vector/Ds_VECTOR_INIT Ds_VECTOR_INIT] macro in declarations.

== Fields ==

`buf` is the buffer holding the memory for the array.  Its type is "pointer to a [wiki:Docs/Ds/Vector/Ds_VECTOR_TYPE Ds_VECTOR_TYPE]", however that's defined.  If you haven't messed it up, this buffer can hold `cap * sizeof(Ds_VECTOR_TYPE)` bytes.  `buf` may or may not be `NULL` if `cap` is `0`.

`num` holds the number of items currently in `buf`.  The significance of field is entirely application-defined, but correctly modified by [wiki:Docs/Ds/Vector/Ds_InsertVectorItems Ds_InsertVectorItems()] and [wiki:Docs/Ds/Vector/Ds_RemoveVectorItems Ds_RemoveVectorItems()].

`cap` is the size of the `buf` buffer, in [wiki:Docs/Ds/Vector/Ds_VECTOR_TYPE Ds_VECTOR_TYPE]s.  Thus, multiply by `sizeof(Ds_VECTOR_TYPE)` to get the size in bytes.

== Notes ==

Under most circumstances, the items held by these arrays will be indexed from `buf[0]` to `buf[num-1]`.  This is true if you use the [wiki:Docs/Ds/Vector/Ds_InsertVectorItems Ds_InsertVectorItems()] and [wiki:Docs/Ds/Vector/Ds_RemoveVectorItems Ds_RemoveVectorItems()] functions, but might not be true if you muck with the internals yourself (or in the case of [wiki:Docs/Ds/Hash/Ds_hash Ds_hash]es, where you should never touch the internals yourself anyway).

Feel free to muck with the internals, so long as you know what you're doing.  See documentation on the functions in this library for what each one assumes about the internals of `Ds_vector`s they accept as parameters.

Since `cap` is a `size_t`, the capacity of these vectors is limited only by the machine's word size and available memory.


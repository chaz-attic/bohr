= `Ds_queue` =

{{{
#!c
typedef struct Ds_queue
{
   Ds_QUEUE_TYPE * buf;
   size_t          num;
   size_t          cap;
   size_t          head;
   size_t          tail;
   Po_mutex        mtx;
   Po_cond         cnd;

} Ds_queue;
}}}

A thread-safe queue.

== Initialization ==

Initialize to the [wiki:Docs/Ds/Queue/Ds_QUEUE_INIT Ds_QUEUE_INIT] macro in declarations.

== Fields ==

`buf` is the memory buffer holding all the currently enqueued items.  Its type is "pointer to [wiki:Docs/Ds/Queue/Ds_QUEUE_TYPE Ds_QUEUE_TYPE]", however that's defined.  This buffer can hold `cap * sizeof(Ds_QUEUE_TYPE)` bytes.  `buf` may or may not be `NULL` if `cap` is `0`.  The buffer is treated as circular, and the [wiki:Docs/Ds/Queue/Ds_QUEUE_TYPE Ds_QUEUE_TYPE] items will always be contiguous.  The head of the queue, obviously enough, is at `buf[head]`.

`num` is the current number of [wiki:Docs/Ds/Queue/Ds_QUEUE_TYPE Ds_QUEUE_TYPE] items in the queue.

`cap` is the current size of the `buf` buffer, in [wiki:Docs/Ds/Queue/Ds_QUEUE_TYPE Ds_QUEUE_TYPE]s.  Thus, multiply by `sizeof(Ds_QUEUE_TYPE)` to get the size in bytes.

`head` holds the index of the head item.  The head is the item that will be dequeued first.

`tail` holds the index of the first item after the tail item.  This is the slot where the first enqueued item will go.  Remember that the `buf` buffer is circular, so `tail` can be less than `head`.  If they're equal, the queue may be empty or full; check `num`.

`mtx` is the [wiki:Docs/Po/Sync/Po_mutex Po_mutex] used for synchronizing multi-threaded access to all these fields from within the functions in this library.

`cnd` is the [wiki:Docs/Po/Sync/Po_cond Po_cond] condition variable used by [wiki:Docs/Ds/Queue/Ds_Dequeue Ds_Dequeue()] to wait until there are items in the queue.

== Notes ==

Under most circumstances, you should really treat this type as an opaque `struct`, and only use the functions in this library to manipulate variables of this type.  This is always a safe bet.

You should certainly never read or modify these fields directly from a multi-threaded program.  You'll almost certainly introduce extremely hard-to-find errors if you do so.

The only condition in which it's safe to use these fields directly is within single-threaded programs, and you don't use any of the functions in this library.  Basically, only if you know exactly what you're doing.

So really, treat this type as an opaque `struct` and only read or modify objects with this type with the functions in this library.


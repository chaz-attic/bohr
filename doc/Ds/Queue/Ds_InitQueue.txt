= `Ds_InitQueue()` =

{{{
#!c
int Ds_InitQueue(
   Ds_queue * restrict q,
   size_t cap,
   int broadcast
);
}}}

Initializes a [wiki:Docs/Ds/Queue/Ds_queue Ds_queue].

== Parameters ==

`q` (in) should point to the [wiki:Docs/Ds/Queue/Ds_queue Ds_queue] to initialize.  Passing `NULL` is an error.

`cap` (in) should be the initial capacity, in [wiki:Docs/Ds/Queue/Ds_QUEUE_TYPE Ds_QUEUE_TYPE]s, to give `q`.

`broadcast` (in) should be a boolean flag of whether [wiki:Docs/Ds/Queue/Ds_Enqueue Ds_Enqueue()], when passed `q`, should wake up one or all threads waiting in [wiki:Docs/Ds/Queue/Ds_Dequeue Ds_Dequeue()].  Specify `0` and enqueuing items will wake up only one waiting thread; specify nonzero and all waiting threads will be awakened when you enqueue items.

== Return Values ==

Returns nonzero if the function succeeds, or `0` if the function fails.

== Notes ==

If [wiki:Docs/Ds/Queue/Ds_QUEUE_BEHAVIOR Ds_QUEUE_BEHAVIOR] is `0`, you'll want `cap` to be nonzero, or else you'll never be able to put anything in your queue.

For efficiency's sake, you'll probably want `cap` to be as big as you think the queue will ever reasonably get.  Or whatever.

This function not only sets aside an initial buffer capacity, but also initializes the thread synchronization objects for `q`.

How many threads waiting on `q` in [wiki:Docs/Ds/Queue/Ds_Dequeue Ds_Dequeue()] will be awakened when you [wiki:Docs/Ds/Queue/Ds_Enqueue Ds_Enqueue()] items in it depends on the `broadcast` parameter given here.  This is due to a limitation in [wiki:Docs/Po/Sync Po_sync].  Read up on [wiki:Docs/Po/Sync/Po_cond Po_cond] condition variables for the reasoning, or just accept it and move on.


= `Ds_WAIT_FOREVER` =

{{{
#!c
#define Ds_WAIT_FOREVER ULONG_MAX
}}}

Tells [wiki:Docs/Ds/Queue/Ds_Dequeue Ds_Dequeue()] to wait without timing out until another thread posts items in the [wiki:Docs/Ds/Queue/Ds_queue Ds_queue].

== Notes ==

Pass to [wiki:Docs/Ds/Queue/Ds_Dequeue Ds_Dequeue()] in the `max_wait_ms` parameter in lieu of an actual millisecond value, and it won't time out, but wait indefinitely until there are items in the [wiki:Docs/Ds/Queue/Ds_queue Ds_queue].


= `Ds_FreeQueue()` =

{{{
#!c
void Ds_FreeQueue(
   Ds_queue * restrict q
);
}}}

Frees a [wiki:Docs/Ds/Queue/Ds_queue Ds_queue].

== Parameters ==

`q` (in) should point to the [wiki:Docs/Ds/Queue/Ds_queue Ds_queue] to free.  Passing `NULL` is an error.

== Return Values ==

Returns nothing.

== Notes ==

Use this function when you're done with the [wiki:Docs/Ds/Queue/Ds_queue Ds_queue]'s contents to free up memory.

Returns the [wiki:Docs/Ds/Queue/Ds_queue Ds_queue] to the same state as if it had just been initialized with [wiki:Docs/Ds/Queue/Ds_QUEUE_INIT Ds_QUEUE_INIT].


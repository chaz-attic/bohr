= `Ds_QueueSize()` =

{{{
#!c
size_t Ds_QueueSize(
   Ds_queue * restrict q
);
}}}

Returns the number of items in the [wiki:Docs/Ds/Queue/Ds_queue Ds_queue].

== Parameters ==

`q` (in) should point to the [wiki:Docs/Ds/Queue/Ds_queue Ds_queue] whose size you want to examine.  Passing `NULL` is an error.

== Return Values ==

Returns `q->num`, or `0` if there's an error.

== Notes ==

This function is a thread-safe way of retrieving `q`'s size in [wiki:Docs/Ds/Queue/Ds_QUEUE_TYPE Ds_QUEUE_TYPE]s.

If you've got lots of threads manipulating the same [wiki:Docs/Ds/Queue/Ds_queue Ds_queue], note that the value returned by this function is only accurate for the duration of this function call.  This means that by the time the function returns and you get the return value, the real size may have changed.  So, use this as more of an approximation of the size, unless you know exactly when and how other threads are working with the [wiki:Docs/Ds/Queue/Ds_queue Ds_queue].


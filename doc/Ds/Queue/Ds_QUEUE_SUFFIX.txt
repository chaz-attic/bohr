= `Ds_QUEUE_SUFFIX` =

{{{
#!c
#define Ds_QUEUE_SUFFIX
}}}

Controls the symbol names made visible to the source file that's including `bohr/ds_queue.h`.

This macro controls how [wiki:Docs/Ds/Queue Ds_queue] operates.  Thus, if you wish to override its default, define it on the compiler command line or in your application's source files above any included headers.

By means of some preprocessor magic, the tokens given as the definition of this macro are appended to the symbol names made visible by including `bohr/ds_queue.h`.  For example, if you define the suffix as `_a`, you'll have available `Ds_queue_a`s instead of `Ds_queue`s and need to use `Ds_InitQueue_a()` instead of `Ds_InitQueue()`, etc.  If you include the header with this macro left to its empty default, symbol names are as they appear in this documentation.

Symbols affected by this macro are all types and functions in the [wiki:Docs/Ds/Queue Ds_queue] library.  Not affected are all macros.

The reason this macro exists is to enable multiple inclusions of the `bohr/ds_queue.h` header presumably with different [wiki:Docs/Ds/Queue/Ds_QUEUE_BEHAVIOR Ds_QUEUE_BEHAVIOR]s and/or [wiki:Docs/Ds/Queue/Ds_QUEUE_TYPE Ds_QUEUE_TYPE]s.  In C++ templates, the name mangling is done behind your back, by the compiler.  In C, you have to mangle your own names; this macro allows you to do just that.  '''Important note: if you include the header more than once without using a different `Ds_QUEUE_SUFFIX` each time, you will get compiler errors.'''  You've been warned.

For example, if you want to work with two types of queue, one holding `int`s and the other holding `char *`s, the following code would do the trick:
{{{
#!c
#define Ds_QUEUE_TYPE int
#define Ds_QUEUE_SUFFIX _i
#include <bohr/ds_queue.h>

#define Ds_QUEUE_TYPE char *
#define Ds_QUEUE_SUFFIX _cp
#include <bohr/ds_queue.h>

Ds_queue_i  q1 = Ds_QUEUE_INIT;
Ds_queue_cp q2 = Ds_QUEUE_INIT;

//... inside some function

   int array_ints[10] = { /* ... */ };
   char * array_strings[15] = { /* ... */ };

   Ds_InitQueue_i(&q1, 10);  //reserve space for 10 ints in our int queue
   Ds_InitQueue_cp(&q2, 15); //and 15 char *s in our char * queue

   Ds_Enqueue_i(&q1, array_ints, 10);     //put our array data into the queues
   Ds_Enqueue_cp(&q2, array_strings, 15); //and let other threads sort 'em out

//...
}}}

This macro is `#undef`ined at the bottom of `bohr/ds_queue.h`, to make it easier to re-`#define` later.


= Ds_hash =

A hash table library.

Notably missing from this library is any actual hashing function.  It's left up the application to find an appropriate function for its needs.  This library just handles the hash tables--inserting into, removing from, searching, iterating over, and so on.  I can make some suggestions on general purpose string hash functions, though: check out [http://burtleburtle.net/bob/hash/ Bob Jenkins' hash functions] (particularly [http://burtleburtle.net/bob/c/lookup3.c lookup3.c]) and [http://www.azillionmonkeys.com/qed/hash.html Paul Hsieh's "super fast hash"].  They probably do whatever it is you need.

There are three, logically separate data types I talk about in this library (aside from the obvious hash table itself, [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]).  The first, an "item", is something you want to hash and put into a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table].  It can be a string, a `struct`--whatever you want.  In this library, you can mix item types and sizes, so you can insert variable-length strings for example.  The next, an "entry" ([wiki:Docs/Ds/Hash/Ds_hash_entry Ds_hash_entry]), is a bucket in the [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table].  It contains an item, and some information about the item and its place in the [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table].  Lastly, there's a "hash code" ([wiki:Docs/Ds/Hash/Ds_hash_t Ds_hash_t]), which is the computed hash value of an item.  As the previous paragraph explains, hash codes themselves are up to the application to generate using whatever hashing function you want.

The hash table structure, [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table], resolves collisions using linked list chains in each bucket.

== Headers ==

{{{
#!c
#include <bohr/ds_hash.h>
}}}

Or, if you want all of [wiki:Docs/Ds Darmstadtium]:
{{{
#!c
#include <bohr/ds.h>
}}}

== Requirements ==

Just the standard libraries are required.

== Reference ==

=== Behavior ===

 * [wiki:Docs/Ds/Hash/Ds_HASH_INLINE Ds_HASH_INLINE]: controls inlining and scope of functions

=== Types ===

 * [wiki:Docs/Ds/Hash/Ds_hash_compare_fn Ds_hash_compare_fn]: function to compare items stored in a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]
 * [wiki:Docs/Ds/Hash/Ds_hash_entry Ds_hash_entry]: an entry in a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]
 * [wiki:Docs/Ds/Hash/Ds_hash_t Ds_hash_t]: a hash code value
 * [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]: a hash table

=== Macros ===

 * [wiki:Docs/Ds/Hash/Ds_HASH_C Ds_HASH_C()]: declares a [wiki:Docs/Ds/Hash/Ds_hash_t Ds_hash_t] literal
 * [wiki:Docs/Ds/Hash/Ds_HASH_MAX Ds_HASH_MAX]: maximum value for a [wiki:Docs/Ds/Hash/Ds_hash_t Ds_hash_t]
 * [wiki:Docs/Ds/Hash/Ds_HASH_TABLE_INIT Ds_HASH_TABLE_INIT]: initializer for [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]s
 * [wiki:Docs/Ds/Hash/PRI*Ds_HASH PRI*Ds_HASH]: `printf()` specifiers to print [wiki:Docs/Ds/Hash/Ds_hash_t Ds_hash_t] values
 * [wiki:Docs/Ds/Hash/SCN*Ds_HASH SCN*Ds_HASH]: `scanf()` specifiers to read [wiki:Docs/Ds/Hash/Ds_hash_t Ds_hash_t] values

=== Functions ===

 * [wiki:Docs/Ds/Hash/Ds_FreeHashTable Ds_FreeHashTable()]: frees a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]
 * [wiki:Docs/Ds/Hash/Ds_InitHashTable Ds_InitHashTable()]: initializes a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]
 * [wiki:Docs/Ds/Hash/Ds_InsertHashItem Ds_InsertHashItem()]: inserts some data into a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]
 * [wiki:Docs/Ds/Hash/Ds_NextHashEntry Ds_NextHashEntry()]: iterates over entries in [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]s
 * [wiki:Docs/Ds/Hash/Ds_RemoveHashEntry Ds_RemoveHashEntry()]: removes an entry from a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]
 * [wiki:Docs/Ds/Hash/Ds_ResizeHashTable Ds_ResizeHashTable()]: resizes a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]
 * [wiki:Docs/Ds/Hash/Ds_SearchHashTable Ds_SearchHashTable()]: finds an item in a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table] based on its key

== Thread Safety ==

Each function in this library is entirely reentrant, but [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]s themselves aren't intrinsically safe to pass between threads that would modify their contents.  That is, this library does no mutual exclusion or other synchronization for you.  Luckily, it's a fairly trivial exercise to implement a wrapper around this library using mutices from [wiki:Docs/Po/Sync Po_sync] to make thread-safe [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]s.

== Examples ==

This simple example inserts some key/value items into a hash table, then searches for them.  Note that it assumes Bob Jenkins' [http://burtleburtle.net/bob/c/lookup3.c lookup3.c] will be linked in:
{{{
#!c
#include <bohr/ds_hash.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

//from http://burtleburtle.net/bob/c/lookup3.c:
uint32_t hashlittle(const void *key, size_t length, uint32_t initval);

//a key:value pair
#define KEY_SIZE 32
struct item
{
   char key[KEY_SIZE];
   int  value;
};

//our comparison function
int Compare(const void * restrict key, size_t key_size,
            const void * restrict item, size_t item_size)
{
   struct item * real_key = (struct item *)key;
   struct item * real_item = (struct item *)item;

   return strcmp(real_key->key, real_item->key);
}

int main(void)
{
   //a couple of items
   struct item a = {{"a"}, 22};
   struct item b = {{"b"}, 99};

   //we'll hash only their names so we can change their value without rehashing
   Ds_hash_t a_hash = (Ds_hash_t)hashlittle(a.key, 1, 0);
   Ds_hash_t b_hash = (Ds_hash_t)hashlittle(b.key, 1, 0);

   //initialize a hash table
   Ds_hash_table tab;
   if(!Ds_InitHashTable(&tab, 2))
   {
      printf("Error initializing the hash table!\n");
      return 1;
   }

   //insert our two items into the table
   if(!Ds_InsertHashItem(&tab, &a, sizeof(a), a_hash)
   || !Ds_InsertHashItem(&tab, &b, sizeof(b), b_hash))
   {
      printf("Error inserting a hash item!\n");
      return 1;
   }

   //search for our hash items (note that since inserting copies the entire
   //item into the table, searching for the same exact memory address we just
   //inserted isn't totally pointless, I swear)
   Ds_hash_entry * a_entry = Ds_SearchHashTable(&tab, &a, sizeof(a),
                                                a_hash, Compare);
   Ds_hash_entry * b_entry = Ds_SearchHashTable(&tab, &b, sizeof(b),
                                                b_hash, Compare);
   if(!a_entry || !b_entry)
   {
      printf("Error finding a hash item!\n");
      return 1;
   }

   //let's see what we found
   printf("Key 'a' is in bucket %zd with value %d\n",
          a_entry->bucket, ((struct item *)a_entry->item)->value);
   printf("Key 'b' is in bucket %zd with value %d\n",
          b_entry->bucket, ((struct item *)b_entry->item)->value);

   //clean up
   Ds_FreeHashTable(&tab);
   return 0;
}
}}}


= `Ds_HASH_TABLE_INIT` =

{{{
#!c
#define Ds_HASH_TABLE_INIT Ds_VECTOR_INIT
}}}

Initializes declarations of type [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table].

Since [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]s are just specialized [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]s, their initialization macros are identical.


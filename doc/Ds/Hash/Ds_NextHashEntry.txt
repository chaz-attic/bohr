= `Ds_NextHashEntry()` =

{{{
#!c
Ds_hash_entry * Ds_NextHashEntry(
   const Ds_hash_table * restrict table,
   const Ds_hash_entry * restrict prev
);
}}}

Iterates over all the [wiki:Docs/Ds/Hash/Ds_hash_entry Ds_hash_entry] entries in a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table].

== Parameters ==

`table` (in) should point to the [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table] over which to iterate.  Passing `NULL` is an error.

`prev` (in) should point to the [wiki:Docs/Ds/Hash/Ds_hash_entry Ds_hash_entry] returned from the previous call to this function.  Passing `NULL` causes this function to return the "first" [wiki:Docs/Ds/Hash/Ds_hash_entry Ds_hash_entry] in the `table`.

== Return Values ==

Returns the "next" (from `prev`; "first", if `prev` was `NULL`) [wiki:Docs/Ds/Hash/Ds_hash_entry Ds_hash_entry] in the `table`.  Returns `NULL` when `prev` was the "last" [wiki:Docs/Ds/Hash/Ds_hash_entry Ds_hash_entry] in the `table`; thus, returns `NULL` when there are no more entries.

== Notes ==

The ordering of [wiki:Docs/Ds/Hash/Ds_hash_entry Ds_hash_entry] entries within [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table]s can seem fairly arbitrary, and is defined by items' hash codes and also by the order items were added.

You can pass any [wiki:Docs/Ds/Hash/Ds_hash_entry Ds_hash_entry] in `prev`; this function doesn't care where it originated.  It's just that the "next" [wiki:Docs/Ds/Hash/Ds_hash_entry Ds_hash_entry] is really only a useful concept within the context of iterating using this function, due to the somewhat arbitrary ordering.

It's an error to modify the `table` in any way in the midst of a series of calls to this function.  At best, you can iterate over duplicate [wiki:Docs/Ds/Hash/Ds_hash_entry Ds_hash_entry] entries or skip some, and at worst, you'll cause a segfault.  If you modify the `table`, start the iteration over by passing `NULL` for `prev`.


= `Ds_HASH_INLINE` =

{{{
#!c
#define Ds_HASH_INLINE inline static
}}}

Controls inlining and scope of [wiki:Docs/Ds/Hash Ds_hash]'s functions.

Unless specified, this macro inherits from the default [wiki:Docs/Ds/Ds_INLINE Ds_INLINE].

This macro controls how [wiki:Docs/Ds/Hash Ds_hash] operates.  Thus, if you wish to override its default, define it on the compiler command line or in your application's source files above any included headers.

This (and its `Ds_*_INLINE` brethren) can be used to exert inlining/scope control over functions in individual [wiki:Docs/Ds Darmstadtium] sub-modules.  Thus, you can make some modules' functions `inline` and others' not.


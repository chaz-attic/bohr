= `Ds_ResizeHashTable()` =

{{{
#!c
int Ds_ResizeHashTable(
   Ds_hash_table * restrict table,
   size_t size
);
}}}

Resizes, that is changes the number of buckets in, a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table].

== Parameters ==

`table` (in) should point to the [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table] to resize.  Passing `NULL` is an error.

`size` (in) should be the new size, as in number of buckets, for `table`.  It '''MUST''' be a power of two.  Passing `0` is most certainly out.

== Return Values ==

Returns nonzero if it succeeds or `0` if it fails.

== Notes ==

This function can either grow or shrink the `table`, depending on whether `size` is passed as greater or less than the previous size (`table->cap`).  Either way, it's a fairly complicated operation (O(''n'')).  You'll have to balance search time, which gets worse the fuller the [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table] gets, with time taken to resize.  An easy guide to follow if you're adding to and removing from your tables often is to create the table with enough initial size to last you a while, and double its size when the table gets 3/4 full (and unless you're in a memory-constrained environment, don't bother to shrink the table).

The only reason this function can fail is because a call to `realloc()` fails.  On failure, if `size` was greater than the previous size (`table->cap`), the `table` won't have been modified in the slightest; unfortunately, on failure if `size` was less than the previous size, the table will be all messed up and unusable.  Tooooo baaaaad!  (I'm fairly certain `realloc()` has a hard time failing when you shrink the buffer with it, so I believe the last case is extremely unlikely.)

If you don't pass a power of two in `size`, strange errors will occur later on.  Don't be a foo', use powers of two.


= `Ds_hash_table` =

{{{
#!c
typedef Ds_vector Ds_hash_table;
}}}

A hash table.

== Initialization ==

Initialize variable declarations of this type with the [wiki:Docs/Ds/Hash/Ds_HASH_TABLE_INIT Ds_HASH_TABLE_INIT] macro.

== Fields ==

(See [wiki:Docs/Ds/Vector/Ds_vector Ds_vector], though note that [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]'s fields are special-purposed here.)

`buf` is an array of pointers to [wiki:Docs/Ds/Hash/Ds_hash_entry Ds_hash_entry].  Don't mess with this.  Each element in the array is considered a "bucket".

`num` holds the number of hash items being stored in the array.  Each "bucket" can hold multiple hash items.  Never modify this value directly, but you can read it at your leisure.

`cap` holds the number of buckets in the `Ds_hash_table`.  `num` can actually be more than `cap`, due to it being possible to chain any number of items into one bucket.  Again, never modify this value directly, but feel free to read it at your leisure.

== Notes ==

`Ds_hash_table`s are a special-case version of [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]s.  Basically, ignore everything in the docs about [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]s; for all intents and purposes, these fields have the same names, but are used entirely differently.  You should never modify the internals of a `Ds_hash_table` yourself, despite having permission to mess with internals of [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]s.


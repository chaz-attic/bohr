= `Ds_InitHashTable()` =

{{{
#!c
int Ds_InitHashTable(
   Ds_hash_table * restrict table,
   size_t size
);
}}}

Initializes a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table] with an initial number of buckets.

== Parameters ==

`table` (in) should point to the [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table] to initialize.  Passing `NULL` is an error.

`size` (in) should be the initial size, as in number of buckets, for `table`.  It '''MUST''' be a power of two.  This means that passing `0` is an error, but `1` is kosher.

== Return Values ==

Returns nonzero if it succeeds, or `0` if it fails.

== Notes ==

This function sets the initial size of a [wiki:Docs/Ds/Hash/Ds_hash_table Ds_hash_table].  You can resize the table later with [wiki:Docs/Ds/Hash/Ds_ResizeHashTable Ds_ResizeHashTable()].

If you don't pass a power of two in `size`, strange errors will occur later on.  Don't be a foo', use powers of two.

The only reason for this function to fail is if it fails to allocate a block of memory.


= Ds_vector =

A dynamic array library.

Through some careful preprocessor magic, you can define vectors that hold any specific type at compile time.  This mimics the behavior of C++ templates: the code will be generated and optimized differently for each type of vector you want.  You can use any number of different typed vectors at once.

This has the consequence that you have to be careful about when and how you `#include` `bohr/ds_vector.h` (or `bohr/ds.h`).  The file doesn't protect against multiple `#include`s, since it's perfectly legitimate to `#include` it twice using two different data types.  See [wiki:Docs/Ds/Vector/Ds_VECTOR_SUFFIX Ds_VECTOR_SUFFIX] for how to `#include` the file multiple times without causing compiler errors.  My advice is simply to only `#include` the file where the code needs to end up, probably in a `.c` file.

== Headers ==

{{{
#!c
#include <bohr/ds_vector.h>
}}}

Or, if you want all of [wiki:Docs/Ds Darmstadtium]:
{{{
#!c
#include <bohr/ds.h>
}}}

== Requirements ==

Just the standard libraries are required.

== Reference ==

=== Behavior ===

 * [wiki:Docs/Ds/Vector/Ds_VECTOR_BEHAVIOR Ds_VECTOR_BEHAVIOR]: controls how [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]s grow
 * [wiki:Docs/Ds/Vector/Ds_VECTOR_INLINE Ds_VECTOR_INLINE]: controls inlining and scope of functions
 * [wiki:Docs/Ds/Vector/Ds_VECTOR_SUFFIX Ds_VECTOR_SUFFIX]: controls symbol names made visible to the including file
 * [wiki:Docs/Ds/Vector/Ds_VECTOR_TYPE Ds_VECTOR_TYPE]: controls what type is stored in [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]s

=== Types ===

 * [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]: a dynamic array

=== Macros ===

 * [wiki:Docs/Ds/Vector/Ds_VECTOR_END Ds_VECTOR_END]: tells [wiki:Docs/Ds/Vector/Ds_InsertVectorItems Ds_InsertVectorItems()] and [wiki:Docs/Ds/Vector/Ds_RemoveVectorItems Ds_RemoveVectorItems()] to insert/remove from the end of the [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]
 * [wiki:Docs/Ds/Vector/Ds_VECTOR_INIT Ds_VECTOR_INIT]: initializer for [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]s

=== Functions ===

 * [wiki:Docs/Ds/Vector/Ds_FreeVector Ds_FreeVector()]: frees a [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]'s memory
 * [wiki:Docs/Ds/Vector/Ds_InitVector Ds_InitVector()]: initializes a [wiki:Docs/Ds/Vector/Ds_vector Ds_vector] with an initial capacity
 * [wiki:Docs/Ds/Vector/Ds_InsertVectorItems Ds_InsertVectorItems()]: inserts items into a [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]
 * [wiki:Docs/Ds/Vector/Ds_RemoveVectorItems Ds_RemoveVectorItems()]: removes items from a [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]
 * [wiki:Docs/Ds/Vector/Ds_ResizeVector Ds_ResizeVector()]: resizes the [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]'s buffer

== Thread Safety ==

Each function in this library is entirely reentrant, but [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]s themselves aren't intrinsically safe to pass between threads that would modify their contents.  That is, this library does no mutual exclusion or other synchronization for you.  Luckily, it's a fairly trivial exercise to implement a wrapper around this library using mutices from [wiki:Docs/Po/Sync Po_sync] to make thread-safe [wiki:Docs/Ds/Vector/Ds_vector Ds_vector]s.

== Examples ==

In this example, items are added to the [wiki:Docs/Ds/Vector/Ds_vector Ds_vector] in two steps, then items are removed from the middle.  It's intentionally convoluted to show the result of different operations.  Note the `_f` [wiki:Docs/Ds/Vector/Ds_VECTOR_SUFFIX Ds_VECTOR_SUFFIX]:
{{{
#!c
#define Ds_VECTOR_TYPE float
#define Ds_VECTOR_SUFFIX _f
#include <bohr/ds_vector.h>
#include <stdio.h>

int main(void)
{
   //a static array of 5 floats
   float a[5] = {1.0f, 2.0f, 3.0f, 4.0f, 5.0f};

   //a dynamic array
   Ds_vector_f v = Ds_VECTOR_INIT;

   //put the last 2 items into the vector
   if(!Ds_InsertVectorItems_f(&v, &a[3], 2, Ds_VECTOR_END))
   {
      printf("Error appending an item!\n");
      return 1;
   }

   //then put the first three at the beginning
   if(!Ds_InsertVectorItems_f(&v, a, 3, 0))
   {
      printf("Error inserting some items!\n");
      return 1;
   }

   //print out the elements in the vector
   for(int i = 0; i < v.num; ++i)
      printf("%f ", v.buf[i]);
   printf("\n");

   //remove the middle 3 items
   Ds_RemoveVectorItems_f(&v, NULL, 3, 1);

   //print it out again
   for(int i = 0; i < v.num; ++i)
      printf("%f ", v.buf[i]);
   printf("\n");

   return 0;
}
}}}


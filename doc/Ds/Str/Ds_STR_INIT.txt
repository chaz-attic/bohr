= `Ds_STR_INIT` =

{{{
#!c
#define Ds_STR_INIT {NULL, 0, 0}
}}}

Initializes a [wiki:Docs/Ds/Str/Ds_str Ds_str] variable declaration.

A [wiki:Docs/Ds/Str/Ds_str Ds_str] set to this compound literal value is in the same state as if it'd been initialized with [wiki:Docs/Ds/Str/Ds_InitStr Ds_InitStr()] with `size` passed as `0`.


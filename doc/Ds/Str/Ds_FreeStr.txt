= `Ds_FreeStr()` =

{{{
#!c
void Ds_FreeStr(
   Ds_str * s
);
}}}

Frees a [wiki:Docs/Ds/Str/Ds_str Ds_str]'s buffer.

== Parameters ==

`s` (in) should point to the [wiki:Docs/Ds/Str/Ds_str Ds_str] variable whose buffer you want to free.  Passing `NULL` is an error.

== Return Values ==

Returns nothing.

== Notes ==

Not much to say, really.  Use this function when you're done with the [wiki:Docs/Ds/Str/Ds_str Ds_str]'s contents to free up memory.

Returns the [wiki:Docs/Ds/Str/Ds_str Ds_str] to the same state as if it had just been initialized with [wiki:Docs/Ds/Str/Ds_STR_INIT Ds_STR_INIT].


= `Ds_StrCatVPrint()` =

{{{
#!c
int Ds_StrCatVPrint(
   Ds_str * dest,
   const char * format,
   va_list args
);
}}}

Concatenates into a [wiki:Docs/Ds/Str/Ds_str Ds_str] using a `printf()`-formatted string and an argument pointer.

== Parameters ==

`dest` (in) should point to the [wiki:Docs/Ds/Str/Ds_str Ds_str] to concatenate into.  Passing `NULL` is an error.

`format` (in) should point to a `printf()`-formatted string.  See some documentation on `printf()` for what's acceptable here.  Passing `NULL` is an error.

`args` (in) should be a `va_list` pointer initialized to the first argument that should be passed to `vprintf()`.  The exact arguments retrievable by this pointer depend on the contents of `format`.  See some documentation on `vprintf()` for details on how this works.

== Return Values ==

Returns the final, formatted length of the string concatenated into `dest`'s buffer (not including whatever contents were already there) if successful.  This is similar to `printf()`'s return value.

Returns a number less than `0` if it fails.

== Notes ==

This function will always concatenate the formatted string onto the end of `dest`'s buffer, as determined by `dest->len`.  To make this function copy over `dest`'s existing contents instead of concatenating, set `dest->len` to `0` before calling this function.

Almost needless to say, if there's not enough space in the destination [wiki:Docs/Ds/Str/Ds_str Ds_str] for it and the source string, it will expand before being concatenated.  See [wiki:Docs/Ds/Str/Ds_STR_BEHAVIOR Ds_STR_BEHAVIOR] for how that works.

This function is useful for a function you write to accept variable arguments and pass them off to be printed in a [wiki:Docs/Ds/Str/Ds_str Ds_str].

Due to the mysterious internals of `vsnprintf()`, which this function calls internally, there can be any number of conditions for failure, but this function is most likely to fail on out of memory conditions.  If this function fails, the contents of `dest` may or may not have changed--there's no guarantee either way, due to `vsnprintf()` not defining its behavior on failure.


= `Po_CloseLibrary()` =

{{{
#!c
void Po_CloseLibrary(
   Po_lib lib
);
}}}

Closes a previously opened library.

== Parameters ==

`lib` (in) should be a [wiki:Docs/Po/Lib/Po_lib Po_lib] library handle returned from [wiki:Docs/Po/Lib/Po_OpenLibrary Po_OpenLibrary()].  If `lib` is `NULL`, this function does nothing.

== Return Values ==

No return value.

== Notes ==

Once a library is closed with this function, all references to its symbols as loaded with [wiki:Docs/Po/Lib/Po_LoadSymbols Po_LoadSymbols()] no longer point to valid memory.  Also, obviously, `lib` itself (and any copies) no longer point to anything valid.


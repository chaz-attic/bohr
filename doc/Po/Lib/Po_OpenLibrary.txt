= `Po_OpenLibrary()` =

{{{
#!c
Po_lib Po_OpenLibrary(
   const char * filename
);
}}}

Opens the shared library with the given `filename`.

== Parameters ==

`filename` (in) should be a UTF-8 string indicating the name of the shared library file to open.  The underlying system calls this function wraps should all try loading the file using the system-default library loader path (which usually includes `.`), so it shouldn't always be necessary for `filename` to specify a full path.  Specifying `NULL` for `filename` is an error.

== Return Values ==

Returns a [wiki:Docs/Po/Lib/Po_lib Po_lib] representing the library if it succeeds, or `NULL` if it fails for whatever reason.

== Notes ==

Once a library is open, you can load symbols from it into memory using [wiki:Docs/Po/Lib/Po_LoadSymbols Po_LoadSymbols()].

When you're done using the library, close it with [wiki:Docs/Po/Lib/Po_CloseLibrary Po_CloseLibrary()].


= `Po_lib` =

{{{
#!c
typedef void * Po_lib;
}}}

Represents an in-memory shared library.

== Initialization ==

As with any pointer, a `NULL` value means it doesn't point to anything useful.  Thus, use `NULL` to initialize variable declarations of this type.

== Notes ==

You should treat a `Po_lib` as a pointer to an opaque object.


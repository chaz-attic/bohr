= `Po_ExitThread()` =

{{{
#!c
void Po_ExitThread(
   int code
);
}}}

Explicitly ends the calling thread's execution.

== Parameters ==

`code` (in) should be whatever value you want propagated up to another thread's [wiki:Docs/Po/Thread/Po_JoinThread Po_JoinThread()] call.  Its significance is totally application-defined.

== Return Values ==

Does not return, as in, execution ceases in the calling function and thread.

== Notes ==

Calling this function is like calling `exit()`, except just the calling thread is ended, not the whole process.

This function is called implicitly when a thread execution [wiki:Docs/Po/Thread/Po_thread_fn function] returns.  Thus, it's never actually necessary to call this function yourself, but there is exactly one situation where it can be useful.  When the main thread of execution finishes, it implicitly calls `exit()`, ending it and all other running threads.  Thus, if you want the main thread to stop, but allow other threads to continue to execute until they stop of their own volition, call this function in the main thread before `main()` returns.


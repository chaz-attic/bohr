= `Po_CancelFind()` =

{{{
#!c
void Po_CancelFind(
   Po_findfiles * restrict find
);
}}}

Frees the context stored in `find` so you don't have to continue calling [wiki:Docs/Po/Dir/Po_FindFiles Po_FindFiles()].

== Parameters ==

`find` (in) should point to a [wiki:Docs/Po/Dir/Po_findfiles Po_findfiles] variable previously used in a successful call to [wiki:Docs/Po/Dir/Po_FindFiles Po_FindFiles()].

== Return Values ==

No return value.

== Notes ==

This is quicker than continuing to iterate [wiki:Docs/Po/Dir/Po_FindFiles Po_FindFiles()] calls until one fails.  Thus, if you find the file you're looking for out of a series of [wiki:Docs/Po/Dir/Po_FindFiles Po_FindFiles()] calls, avoid a memory leak by calling this function.


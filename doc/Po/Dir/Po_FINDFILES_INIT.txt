= `Po_FINDFILES_INIT` =

{{{
#!c
#define Po_FINDFILES_INIT {NULL, {'\0'}, 0, NULL}
}}}

Initializes a declaration of a [wiki:Docs/Po/Dir/Po_findfiles Po_findfiles] variable to an initial, cleared state.


= `Po_FindFiles()` =

{{{
#!c
int Po_FindFiles(
   Po_findfiles * restrict find
);
}}}

Iteratively finds files that match a pattern.

== Parameters ==

`find` (in/out) should point to a [wiki:Docs/Po/Dir/Po_findfiles Po_findfiles] variable.  Passing `NULL` is an error.  This variable maintains a state internal to this function.  When I refer to a "first call" to this function, I mean a call with a `find` set to its [wiki:Docs/Po/Dir/Po_FINDFILES_INIT initializer], which occurs when it's manually set as such, after this function returns `0` when passed it, or after passing it to [wiki:Docs/Po/Dir/Po_CancelFind Po_CancelFind()].  By a "subsequent call" to this function, I mean a call after this function returns nonzero when passed the `find`.

`find->pattern` (in) should point to a UTF-8 string (`NULL` is an error) on the first call to this function (it's ignored on subsequent calls).  The string should be in the form `[path]<file>` where `[path]`, if given, is a relative or absolute path ending with a slash, and `<file>` is a filename possibly containing wildcards (`*` and `?`).  If `[path]` is omitted, the current directory is used.  For example, `"/etc/*.ini"` will find all files in `/etc` ending with `.ini`, `"*"` will find all files in the current directory, and `"../README"` will find a file named `README` in the parent directory (from whatever the current directory is).  Other forms for this string may be valid in one OS or another, but using this syntax ensures maximum portability and keeps the algorithm simple.  All subsequent calls to this function return filenames that match this pattern.  (Please remember that `/` is the portable path separator.  Don't use `\`; it's just wrong.)

`find->match` (out) is filled with the matching filename (no path) if this function succeeds.

`find->match_dir` (out), if this function succeeds, is nonzero if the file in `find->match` is a directory or `0` if it isn't.

== Return Values ==

This function returns `0` if there are no more files matching the first call's `find->pattern` or an error occurs (in either case, the iteration is done and `find->match` and `find->match_dir` don't hold anything in particular).  If this function finds a (-nother) matching file without error, it returns nonzero, and `find->match` and `find->match_dir` are set.

== Notes ==

This function was designed for ease of use in a `while`-like construct.  While it returns nonzero, you've found a matching file.  Once it returns `0`, there are no more files that match.

There is some context maintained between first and subsequent calls to this function.  This context is freed only when this function returns `0` or you call [wiki:Docs/Po/Dir/Po_CancelFind Po_CancelFind()] with the same `find`.  Thus, after a successful first call, you should always either make subsequent calls to this function until one returns `0` or call [wiki:Docs/Po/Dir/Po_CancelFind Po_CancelFind()].  Calling [wiki:Docs/Po/Dir/Po_CancelFind Po_CancelFind()] is faster than continuing to iterate, so if, for example, you find the one file you're looking for, call [wiki:Docs/Po/Dir/Po_CancelFind Po_CancelFind()] instead of just not calling this function anymore, to avoid a memory leak.

This function is thread-safe only if called with different `find`s.  Thus, each full iteration (first and subsequent calls) must be made from the same thread.


= `Po_InitCond()` =

{{{
#!c
int Po_InitCond(
   Po_cond * restrict cnd,
   int broadcast
);
}}}

Initializes a [wiki:Docs/Po/Sync/Po_cond Po_cond] object.

== Parameters ==

`cnd` (in) should point to the [wiki:Docs/Po/Sync/Po_cond Po_cond] object to initialize.  It's an error to specify `NULL`.

`broadcast` (in) should be a boolean flag whether you want [wiki:Docs/Po/Sync/Po_SignalCond Po_SignalCond()], when passed `cnd`, to wake up just one waiting thread, or all waiting threads.  Specify `0`, and calling [wiki:Docs/Po/Sync/Po_SignalCond Po_SignalCond()] on `cnd` will wake up only one waiting thread; specify nonzero and they'll all be awakened.

== Return Values ==

Returns a nonzero value if it succeeds, or `0` if it fails.

== Notes ==

A [wiki:Docs/Po/Sync/Po_cond Po_cond] object should not be used unless it's first initialized with this function.

It's kind of unfortunate that how many threads waiting on a condition variable can be woken up by [wiki:Docs/Po/Sync/Po_SignalCond Po_SignalCond()] is fixed at initialization with the `broadcast` parameter.  This is a limitation with my Windows implementation, and there's no way I can see around it.  It shouldn't be a huge deal anyway--usually you want to either "signal" or "broadcast" with one condition variable, but not both.  Meh.

Once you're done with the [wiki:Docs/Po/Sync/Po_cond Po_cond], free it with [wiki:Docs/Po/Sync/Po_FreeCond Po_FreeCond()].

Note that the association between a [wiki:Docs/Po/Sync/Po_mutex Po_mutex] and a [wiki:Docs/Po/Sync/Po_cond Po_cond] doesn't happen here; it's more of a logical construct anyway.


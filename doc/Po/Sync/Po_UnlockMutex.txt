= `Po_UnlockMutex()` =

{{{
#!c
void Po_UnlockMutex(
   Po_mutex * restrict mtx
);
}}}

Unlocks a [wiki:Docs/Po/Sync/Po_mutex Po_mutex] the calling thread has a lock on.

== Parameters ==

`mtx` (in) should point to a [wiki:Docs/Po/Sync/Po_mutex Po_mutex] object previously successfully locked (in the same thread) with [wiki:Docs/Po/Sync/Po_LockMutex Po_LockMutex()].  Passing `NULL` is an error.

== Return Values ==

Doesn't return a value.

== Notes ==

Unlocking a [wiki:Docs/Po/Sync/Po_mutex Po_mutex] that the thread doesn't have a lock on is undefined (so don't do it).

Since locking a [wiki:Docs/Po/Sync/Po_mutex Po_mutex] can be recursive, the number of calls to this function must match the number of calls to [wiki:Docs/Po/Sync/Po_LockMutex Po_LockMutex()] and/or [wiki:Docs/Po/Sync/Po_TryLockMutex Po_TryLockMutex()] before the lock is actually released.


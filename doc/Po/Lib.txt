= Po_lib =

A library loading interface.

Basically, Po_lib is a thin (but portable!) wrapper around the native Windows `.dll` loader and Unix `dl` interface.

== Headers ==

{{{
#!c
#include <bohr/po_lib.h>
}}}

Or, if you want all of [wiki:Docs/Po Polonium]:
{{{
#!c
#include <bohr/po.h>
}}}

== Requirements ==

In Windows, just the standard Windows libraries are required.

In Unix, link with `libdl`.  Add `-ldl` to your GCC linker command line.

== Reference ==

=== Behavior ===

 * [wiki:Docs/Po/Lib/Po_LIB_INLINE Po_LIB_INLINE]: controls inlining and scope of functions

=== Types ===

 * [wiki:Docs/Po/Lib/Po_lib Po_lib]: represents a loaded shared library

=== Functions ===

 * [wiki:Docs/Po/Lib/Po_CloseLibrary Po_CloseLibrary()]: closes a shared library
 * [wiki:Docs/Po/Lib/Po_LoadSymbols Po_LoadSymbols()]: loads a library's symbols into memory
 * [wiki:Docs/Po/Lib/Po_OpenLibrary Po_OpenLibrary()]: opens a shared library

== Examples ==

This example is pretty basic.  It loads the zlib shared library and prints its version information.
{{{
#!c
#include <bohr/po_lib.h>
#include <stdio.h>

//name of the shared object to open
#ifdef _WIN32
   const char * const so_name = "zlib1.dll";
#else
   const char * const so_name = "libz.so";
#endif

#define NUM_FUNCS 1

//array of function names to load
const char * func_names[NUM_FUNCS] = {"zlibVersion"};

int main(void)
{
   Po_lib lib = NULL;

   //array of pointers to as-yet unspecified types
   void * funcs[NUM_FUNCS];

   //after getting function pointers into funcs, we'll put zlibVersion here to
   //make it easier to call
   const char * (* zlibVersion)(void);


   lib = Po_OpenLibrary(so_name);
   if(!lib)
   {
      printf("Couldn't load shared library '%s'!\n", so_name);
      return 1;
   }

   if(Po_LoadSymbols(lib, funcs, func_names, NUM_FUNCS) < NUM_FUNCS)
   {
      printf("Couldn't load one or more symbols!\n");
      return 1;
   }

   //funcs[0] points to the function in names[0], which was zlibVersion
   zlibVersion = (const char * (*)(void))funcs[0];

   //call our function and print its return value
   printf("zlib version: %s\n", zlibVersion());

   Po_CloseLibrary(lib);
   return 0;
}
}}}


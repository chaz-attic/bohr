= `Po_GetTime()` =

{{{
#!c
Po_time_t Po_GetTime(void);
}}}

Returns the number of system clock ticks since some fixed (since system boot) starting point.

== Parameters ==

None.

== Return Values ==

Returns the number of clock ticks elapsed since some arbitrary point.  The starting point is constant while the machine is running, but variable between system boots.

== Notes ==

The actual value returned by this function isn't so useful as a difference between two calls to this function.  Taking the difference between two calls to this function gives you a high-precision wall-clock elapsed time.  Convert to seconds by dividing by the system's clock frequency (see [wiki:Docs/Po/Time/Po_GetFrequency Po_GetFrequency()]).

This function does no error checking, and will return random data if the system doesn't have a high-resolution timer.  For this reason, you must successfully call [wiki:Docs/Po/Time/Po_GetFrequency Po_GetFrequency()], which does do error checking, at least once (probably in an initialization routine) before ever calling this function.

Due to bugs in Windows and/or certain BIOSes, this function may give incorrect readings on multiprocessor systems running Windows.  See the remarks here: http://msdn2.microsoft.com/en-us/library/ms644904.aspx.  To workaround these bugs, use Linux instead.  There's really nothing [wiki:Docs/Po Polonium] can do about it.


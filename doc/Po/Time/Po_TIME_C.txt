= `Po_TIME_C()` =

{{{
#!c
#define Po_TIME_C INT64_C
}}}

Declares literal constants with type [wiki:Docs/Po/Time/Po_time_t Po_time_t].

Used as such: `Po_TIME_C(0)`, which results in a `0` the size of a [wiki:Docs/Po/Time/Po_time_t Po_time_t].


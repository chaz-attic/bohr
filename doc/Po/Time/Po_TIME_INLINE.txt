= `Po_TIME_INLINE` =

{{{
#!c
#define Po_TIME_INLINE inline static
}}}

Controls inlining and scope of [wiki:Docs/Po/Time Po_time]'s functions.

Unless specified, this macro inherits from the default [wiki:Docs/Po/Po_INLINE Po_INLINE].

This macro controls how [wiki:Docs/Po/Time Po_time] operates.  Thus, if you wish to override its default, define it on the compiler command line or in your application's source files above any included headers.

This (and its `Po_*_INLINE` brethren) can be used to exert inlining/scope control over functions in individual [wiki:Docs/Po Polonium] sub-modules.  Thus, you can make some modules' functions `inline` and others' not.


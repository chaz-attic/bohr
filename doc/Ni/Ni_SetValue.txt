= `Ni_SetValue()` =

{{{
#!c
int Ni_SetValue(
   Ni_node restrict n,
   const char * restrict value,
   int value_len
);
}}}

Sets the value of the given node to the given string, or removes the node's value altogether.

== Parameters ==

`n` (in) should be the [wiki:Docs/Ni/Ni_node Ni_node] whose value you wish to set/remove.

`value` (in) should point to a string to use as the new value, or be `NULL`.  If `NULL`, `n`'s value is removed entirely (meaning `n` would then have no value).  If not `NULL`, the string pointed to can be any arbitrary byte values--it need not be ASCII or UTF-8 or anything else if you don't want.

`value_len` (in) should be the length (in `char`s) of the string in `value` (not including terminating null), or a negative integer.  If negative, the length of `value` is calculated with `strlen()`, which obviously won't always work if `value` contains binary data.  This parameter is ignored if `value` is `NULL`.

== Return Values ==

Returns the length of `value`.  This will be exactly `value_len`, if you passed it as non-negative.  If you passed a negative `value_len`, this function will return the `strlen()`-calculated length.  If you passed `NULL` for `value`, returns `0`.

Returns a negative integer in error conditions.  This includes the case where `n` is a root node, since root nodes can't have values, and thus trying to set the value of a root node is an error.  Also returns a negative integer if you pass `NULL` for `n`.

== Notes ==

This function is the way to directly set the exact contents of a node's value.  To set the value from a data type other than a string, see [wiki:Docs/Ni/Ni_SetValueInt Ni_SetValueInt()], [wiki:Docs/Ni/Ni_SetValueFloat Ni_SetValueFloat()], [wiki:Docs/Ni/Ni_SetValueBool Ni_SetValueBool()], and [wiki:Docs/Ni/Ni_ValuePrint Ni_ValuePrint()] (and [wiki:Docs/Ni/Ni_ValueVPrint Ni_ValueVPrint()]).  This function is also the only way to remove a node's value.

Value strings can contain arbitrary binary data.  Hence, the `value_len` parameter is useful for when `value` might have internal null bytes.  It should be the length of `value` in `char`s, which is to say, bytes, not UTF-8 characters.

The `value` string need not persist after this function returns; its contents are copied to a buffer internal to Nickel.

Calling this function sets the node to the "modified" state.  See [wiki:Docs/Ni/Ni_GetModified Ni_GetModified()] for an explanation of the "modified" state.

The only condition in which this function can fail (assuming you passed all valid parameters) is if a call to `realloc()` to grow a memory buffer fails.  I assume this only happens when the machine is low on memory.  In whatever case, if this function fails, `n`'s value (and its modified state, and everything about `n` really) is not modified in the slightest.


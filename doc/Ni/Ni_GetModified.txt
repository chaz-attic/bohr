= `Ni_GetModified()` =

{{{
#!c
int Ni_GetModified(
   Ni_node restrict n
);
}}}

Returns the "modified" state of the given node.

== Parameters ==

`n` (in) should be the [wiki:Docs/Ni/Ni_node Ni_node] whose "modified" state you want to get.

== Return Values ==

Returns nonzero if the node is "modified", or `0` if it isn't.

Also returns `0` if you pass a `NULL` `n`.

== Notes ==

The "modified" state isn't used internally by Nickel, so its significance is entirely application-defined.  The idea is that it's useful for keeping track of "global" defaults vs. "local" overrides.

How that works: first read in (with [wiki:Docs/Ni/Ni_ReadStream Ni_ReadStream()] or [wiki:Docs/Ni/Ni_ReadFile Ni_ReadFile()]) a "global" options file full of default values; then use [wiki:Docs/Ni/Ni_SetModified Ni_SetModified()] to recursively set all the nodes in the tree to "not modified" (the function handles the recursion for you, so you can do this with one single function call); finally, read in some "local" overrides for the defaults into the same tree.  Now, by reading the "modified" state of each node, you know which nodes have been left their default and which ones were overridden.

[wiki:Docs/Ni/Ni_WriteStream Ni_WriteStream()] and [wiki:Docs/Ni/Ni_WriteFile Ni_WriteFile()] can be instructed to only output "modified" nodes, so you can easily generate the "local" overrides file without clobbering options that were left their default value.  Useful?  ''I'' think so.

The "modified" state is automatically set by all of the [wiki:Docs/Ni/Ni_SetValue Ni_SetValue()] functions.


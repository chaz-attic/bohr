= `Ni_GetName()` =

{{{
#!c
const char * Ni_GetName(
   Ni_node restrict n,
   int * restrict len_out
);
}}}

Returns the name of the given node.

== Parameters ==

`n` (in) should be the [wiki:Docs/Ni/Ni_node Ni_node] whose name you want.

`len_out` (out) should point to an integer to fill with the length (in `char`s) of the returned string, or be `NULL`.  If `NULL`, you'll just have to call `strlen()` on the returned string if you ever need its length (which will lead to incorrect results if the name string contains `'\0'` bytes [see the [#Notes notes]]).

== Return Values ==

Returns a pointer to a null-terminated string holding the `n`'s name.  This string can contain binary data (i.e. not ASCII, internal nulls, etc.), if the node was named as such.  If `n` is a tree's root node, returns `NULL` (as root nodes can't have names).

Also returns `NULL` if you pass `NULL` for `n`.

== Notes ==

The returned string is stored internally to Nickel and won't move for the life of the node.  Don't try to `free()` it manually.  Also, don't try to modify the contents of the returned string; it's `const` for a reason.  If you need to rename a node, you'll have to do it manually (by adding a new node [see [wiki:Docs/Ni/Ni_GetChild Ni_GetChild()]] with the same value and removing the old one; good luck if the node has children, you're on your own).

If you want to copy the name elsewhere so you can mess with it, note that names in Nickel can have a maximum length of [wiki:Docs/Ni/Ni_KEY_SIZE Ni_KEY_SIZE]`-1` `char`s, meaning a buffer declared as `char buffer[Ni_KEY_SIZE];` is big enough to hold the name and terminating null.  See [wiki:Docs/Ni/Ni_KEY_SIZE Ni_KEY_SIZE] for a discussion of this.

If you're using UTF-8 in your .ini files and source code strings (as you should), be aware that strings inside Nickel are treated as binary (so they can hold arbitrary data).  Names are truncated at [wiki:Docs/Ni/Ni_KEY_SIZE Ni_KEY_SIZE]`-1` `char`s (i.e. bytes, not UTF-8 characters), which means truncation can happen in the middle of a UTF-8 character.  As a result, even if you're very careful to pass only valid UTF-8 to Nickel, this function can return a string that's valid UTF-8 up to the very end, where a few bytes may be missing from the last character, making it invalid UTF-8.  This only happens with long names ([wiki:Docs/Ni/Ni_KEY_SIZE Ni_KEY_SIZE] `char`s or longer), and is obviously only an issue if you expect UTF-8 back from Nickel.  (Truncation happens the same way for all names, so even if Nickel internally truncates a name in the middle of a UTF-8 character, you'll be able to find it again by passing the same valid UTF-8 string to [wiki:Docs/Ni/Ni_GetChild Ni_GetChild()] to search for it as you did when you created it.)

If you passed a non-`NULL` `len_out`, the integer it points to will be filled with the returned string's length in `char`s (that is, bytes, not UTF-8 characters) before the function returns.  Name strings can contain arbitrary binary data, so this parameter is necessary so no data is "lost" when `strlen()` is used to calculate the length for names with internal null bytes.  It's also useful in general for efficiency's sake.


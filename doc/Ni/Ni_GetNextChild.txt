= `Ni_GetNextChild()` =

{{{
#!c
Ni_node Ni_GetNextChild(
   Ni_node restrict n,
   Ni_node restrict child
);
}}}

Iteratively enumerates the children of the given node.

== Parameters ==

`n` (in) should be the [wiki:Docs/Ni/Ni_node Ni_node] whose children you want to enumerate.

`child` (in) should be the [wiki:Docs/Ni/Ni_node Ni_node] returned by the most recent previous call to this function.  This parameter is the "state" that tells the function which child to return next.  Pass `NULL` to get the first child of `n`.

== Return Values ==

Returns the next [wiki:Docs/Ni/Ni_node Ni_node] child of `n`.  If you passed the last child of `n` as `child`, returns `NULL`  (to put it another way, returns `NULL` when there are no more children).

Also returns `NULL` if you pass `NULL` for `n`.

== Notes ==

This function enumerates all the children of `n`, one child at a time.  Here's a usage example:
{{{
#!c
Ni_node child = NULL;
while((child = Ni_GetNextChild(some_node, child)) != NULL)
{ /* do something with child */ }
}}}

The ordering of children inside Nickel (and as returned by this function) is not intuitive.  Children are '''not''' ordered by when they were added, nor by their name.  (They are instead ordered by their name's hash value, for speed.)  As such, the children returned by this function may seem to be returned in a random order, but of course the sequence doesn't change if you enumerate the children of a node several times.  Just be warned... the ordering may not be what you expect.

Modifying the tree immediately under `n` (that is, `n`'s children) between calls to this function can lead to errors.  Specifically, if you add or remove children from `n` between calls, this function may repeat or skip any number of children.  So, if you add or remove children from `n`, start your iteration over by passing `NULL` for `child`.  (Things that are safe to do [i.e. that won't mess up the enumerated sequence] include changing anything's value, adding or removing siblings of `n`, and adding or removing children of `n`'s children.)


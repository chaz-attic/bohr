= `Ni_GetValue()` =

{{{
#!c
const char * Ni_GetValue(
   Ni_node restrict n,
   int * restrict len_out
);
}}}

Returns the value of the given node as a string.

== Parameters ==

`n` (in) should be the [wiki:Docs/Ni/Ni_node Ni_node] whose value you want.

`len_out` (out) should point to an integer to fill with the length (in `char`s) of the returned string, or be `NULL`.  If `NULL`, you'll just have to call `strlen()` on the returned string if you ever need its length (which will lead to incorrect results if the value string contains `'\0'` bytes [see the [#Notes notes]]).

== Return Values ==

Returns a pointer to a null-terminated string holding the `n`'s value.  This string can contain binary data (e.g. not ASCII, internal nulls, etc.), if that's what the value currently contains.  Returns `NULL` if `n` has no value (which includes the case where `n` is the root node of a tree, since root nodes can have neither names nor values).

Also returns `NULL` if you pass `NULL` for `n`.

== Notes ==

The returned string is stored internally to Nickel and may move around if the node's value changes.  Don't try to `free()` it manually.  Don't keep the returned pointer around if the node's value may change.  Also, don't try to modify the contents of the returned string; it's `const` for a reason.  To change a node's value, use [wiki:Docs/Ni/Ni_SetValue Ni_SetValue()].

Values are stored internally to Nickel as strings, so this function returns the value as Nickel sees it, without translation.  To translate the value into another data type (e.g. integer), you can use [wiki:Docs/Ni/Ni_GetValueInt Ni_GetValueInt()], [wiki:Docs/Ni/Ni_GetValueFloat Ni_GetValueFloat()], [wiki:Docs/Ni/Ni_GetValueBool Ni_GetValueBool()], or [wiki:Docs/Ni/Ni_ValueScan Ni_ValueScan()] (or [wiki:Docs/Ni/Ni_ValueVScan Ni_ValueVScan()]).  Or, you can just do the conversion yourself.  Whatevs.

If you passed a non-`NULL` `len_out`, the integer it points to will be filled with the returned string's length in `char`s (that is, bytes, not UTF-8 characters) before the function returns.  Value strings can contain arbitrary binary data, so this parameter is necessary so no data is "lost" when `strlen()` is used to calculate the length for values with internal null bytes.  It's also useful in general for efficiency's sake.

If this function returns `NULL`, `n` has no value (though see [#ReturnValues above]).  This is a distinct condition from an empty value, which is returned as `""` (a string with `0` length).


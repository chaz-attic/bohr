= `Ni_SetValueFloat()` =

{{{
#!c
int Ni_SetValueFloat(
   Ni_node restrict n,
   double value
);
}}}

Sets the given node's value to a (double precision) floating point number.

== Parameters ==

`n` (in) should be the [wiki:Docs/Ni/Ni_node Ni_node] whose value to set.

`value` (in) should be the `double` to use as `n`'s new value.

== Return Values ==

Returns the string length of `value` as a decimal number (e.g. passing `1.5` would return `3`), or a negative integer in an error condition (which includes trying to set the value of a root node, since root nodes can't have values).

Also returns a negative number if you pass `NULL` for `n`.

== Notes ==

This function is implemented exactly as the one-liner:
{{{
#!c
return Ni_ValuePrint(n, "%.17g", value);
}}}
Thus, see [wiki:Docs/Ni/Ni_ValuePrint Ni_ValuePrint()].

This function cannot be used to remove a node's value.  See [wiki:Docs/Ni/Ni_SetValue Ni_SetValue()] for how to remove values.

Note that since [wiki:Docs/Ni/Ni_ValuePrint Ni_ValuePrint()] sets the node to the "modified" state, so does this function.  See [wiki:Docs/Ni/Ni_GetModified Ni_GetModified()] for an explanation of the "modified" state.

See also [wiki:Docs/Ni/Ni_SetValue Ni_SetValue()].


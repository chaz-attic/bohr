= `Ni_ValueVScan()` =

{{{
#!c
int Ni_ValueVScan(
   Ni_node restrict n,
   const char * restrict format,
   va_list args
);
}}}

Calls `vsscanf()` on the given node's value.

== Parameters ==

`n` (in) should be the [wiki:Docs/Ni/Ni_node Ni_node] whose value to scan.

`format` (in) should be a `scanf()`-formatted string, specifying what to scan for.  See documentation on `scanf()` for what's acceptable.

`args` (in) should be a `va_list` pointer initialized to the first in a series of arguments necessary given the `format` string.  See `vsscanf()` for how this works.

== Return Values ==

Exactly the same return values as [wiki:Docs/Ni/Ni_ValueScan Ni_ValueScan()].

== Notes ==

See [wiki:Docs/Ni/Ni_ValueScan Ni_ValueScan()] for how this function is useful.  The two functions are identical, except in how you pass parameters.

This function is the `vsscanf()` to [wiki:Docs/Ni/Ni_ValueScan Ni_ValueScan()]'s `sscanf()`.  See some documentation on the `scanf()` family of functions for why the difference is significant.


= `Ni_KEY_SIZE` =

{{{
#!c
#define Ni_KEY_SIZE 128
}}}

The buffer size in `char`s needed to hold the name of a node with terminating null.

This also has the significance of defining how many `char`s are significant in names in Nickel internally.  Only `Ni_KEY_SIZE-1` `char`s of a node's name are stored by Nickel.  When searching, at most `Ni_KEY_SIZE-1` `char`s are used to compare two names.  The value is sufficiently big to avoid all but the most extreme name conflicts, yet small enough to keep a tight cap on running time of hashing and searching on names.

Note that when I say `char`s, I generally mean bytes (unless Nickel is compiled in a system where a `char` is more than one byte).  However, a `char` is not equivalent to a UTF-8 character, since in UTF-8 characters may span several bytes.  Just keep this distinction in mind.  In Nickel, all strings are assumed to be binary, though UTF-8 (highly recommended) is preserved.  See [wiki:Docs/Ni/Ni_GetName Ni_GetName()] for one of the few places this affects the user.


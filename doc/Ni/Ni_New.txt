= `Ni_New()` =

{{{
#!c
Ni_node Ni_New(void);
}}}

Allocates an entirely new Nickel tree.

== Parameters ==

None.

== Return Values ==

Returns the root [wiki:Docs/Ni/Ni_node Ni_node] of the new tree.  On error (i.e. out of memory), returns `NULL`.

== Notes ==

This function is used to allocate an entirely new tree.  Though it actually only allocates memory for one node (the root, which is returned), the node can never be added as a child of another node (e.g. in another tree), due to the architecture of Nickel's API.

If instead you want to make a new child of an existing node (in an existing tree), use the [wiki:Docs/Ni/Ni_GetChild Ni_GetChild()] function.

When you're done with the tree, free it with [wiki:Docs/Ni/Ni_Free Ni_Free()] to avoid memory leaks.

The only condition where this function can fail is when `malloc()` returns `NULL`.


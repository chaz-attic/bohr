= `Ni_SetValueBool()` =

{{{
#!c
int Ni_SetValueBool(
   Ni_node restrict n,
   int value
);
}}}

Sets the given node's value to a boolean value.

== Parameters ==

`n` (in) should be the [wiki:Docs/Ni/Ni_node Ni_node] whose value you're trying to set.

`value` (in) should be the boolean value to use as `n`'s new value.  Like the C standard, nonzero means true, `0` means false.

== Return Values ==

Technically, this function returns `4`, `5`, or a negative integer (as these are the lengths of possible strings the value is set to, and the error condition; see [#Notes below]).  You should probably treat it as if it returns a negative integer on error (which includes trying to set the value of a root node, since root nodes can't have values), or a non-negative integer on success.

Returns a negative integer if you pass `NULL` for `n`.

== Notes ==

This function is implemented exactly as the one-liner:
{{{
#!c
return Ni_SetValue(n, (value ? "true" : "false"), (value ? 4 : 5));
}}}
Thus, see [wiki:Docs/Ni/Ni_SetValue Ni_SetValue()].

This function cannot be used to remove a node's value.  See [wiki:Docs/Ni/Ni_SetValue Ni_SetValue()] for how to remove values.

Note that since [wiki:Docs/Ni/Ni_SetValue Ni_SetValue()] sets the node to the "modified" state, so does this function.  See [wiki:Docs/Ni/Ni_GetModified Ni_GetModified()] for an explanation of the "modified" state.


= `Ni_GetValueInt()` =

{{{
#!c
long Ni_GetValueInt(
   Ni_node restrict n
);
}}}

Returns the value of the given node interpreted as an integer (well, a long integer).

== Parameters ==

`n` (in) should be the [wiki:Docs/Ni/Ni_node Ni_node] whose value you want.

== Return Values ==

Returns `n`'s value as a `long` integer.  If `n` has no value, this function returns `0L`.

Also returns `0L` if you pass `NULL` for `n`.

== Notes ==

The interpretation from string to integer is performed by the `strtol()` function, with third (`base`) parameter as `0`.  See some documentation on that function for how that works.  It's pretty self-explanatory.

See also [wiki:Docs/Ni/Ni_GetValue Ni_GetValue()].


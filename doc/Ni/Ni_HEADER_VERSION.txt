= `Ni_HEADER_VERSION` =

{{{
#!c
#define Ni_HEADER_VERSION UINT32_C(0x01000000)
}}}

The version of Nickel available in the included header.

Versions of Nickel are expressed in the source as a `uint32_t` whose component bytes each make up part of the version number.  From most significant byte to least: major version, revision number, API age, and release number.  Hence, `0x01020304` would be version 1.2.3(.4).  (The bytes are to be interpreted as integers, so `0x0a000000` would be version 10.0.0.

The first three numbers (major, revision, and age) are the Nickel .so's libtool version number (even though as of yet Nickel doesn't use libtool, it's still a useful convention to follow).  See [http://www.gnu.org/software/libtool/manual.html#Libtool-versioning the libtool manual] for how to interpret those numbers.

The last number (release) is for changes to Nickel that don't affect the binary interface, e.g. revising comments, cleaning up the source, updating the README, etc.

See also [wiki:Docs/Ni/Ni_VERSION Ni_VERSION] and [wiki:Docs/Ni/Ni_GetVersion Ni_GetVersion()].


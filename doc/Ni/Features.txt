= Nickel Features =

Nickel is written with an emphasis on efficiency, but it's still much more robust than other .ini libraries.

 * Written in ISO standard C99.  Compiles in any environment with a modern C compiler.
 * Native support for UTF-8.  All languages in the Unicode standard are fair game.
 * Extremely fast.  My parser is hand-written to outperform other .ini parsers.  Keys are hashed using Bob Jenkins' amazing [http://burtleburtle.net/bob/hash/ lookup3.c] algorithm for quick lookup.
 * Real hierarchies of keys are supported--nay, encouraged!  The data structure is a tree of maps.
 * Values are effectively limited in length only by available memory (for fairness' sake, values longer than 2GB may cause problems).
 * Multi-line values are trivially easy to specify: just end the line with a `\`.
 * Supports C-style escape sequences in keys and values.  No restrictions on what byte values are valid anywhere, so you can use any character set you want, or include arbitrary binary data.
 * Facilitates keeping separate "global" and "local" options.  Read in some global "default" values from a file, override a few, and Nickel allows you to write only the modified values back to disk.  You can read any number of files into the same object, making it easy to get back to a state with mostly "defaults" and a few overridden values.
 * Hits the disk minimally, and only sequentially.  Builds an in-memory representation instead of parsing the file every time you need a value.
 * Niceties like allowing data to pass into/out of arbitrary `FILE *` streams (e.g. `stdin` and `stdout`), being able to `printf()` into and `scanf()` out of values, etc.

Features that some people might not like:
 * Maintains a good worst-case running time even on huge trees, at the expense of some memory.
 * Maintains a good worst-case searching time by capping how many characters are significant in keys (at more than a hundred bytes).  You can have longer keys, but only the first hundred or so bytes are used to identify the item in question.
 * Sibling keys are treated as being unique.  Thus, when reading a .ini file that has several lines (under the same parent) with the same key (e.g. `php.ini`'s many `extension=` lines), only the last one in the file counts.
 * Minimal file error checking.  The code is very stable, mind you, but it'll silently ignore invalid data in the .ini file.
 * Automatic conversion of old Mac OS (CR) and DOS/Windows (CRLF) line endings to Unix (LF) style.  Requires a literal `\r` in the .ini file to maintain a CR character.
 * Comments are truly ignored when reading; no (contextual) comments are generated when writing.


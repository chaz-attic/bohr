= `Ni_GetValueFloat()` =

{{{
#!c
double Ni_GetValueFloat(
   Ni_node restrict n
);
}}}

Returns the given node's value interpreted as a (double precision) floating point number.

== Parameters ==

`n` (in) should be the [wiki:Docs/Ni/Ni_node Ni_node] whose value you want.

== Return Values ==

Returns the value of `n` as a `double`.  Returns `0.0` if `n` has no value.

Also returns `0.0` if you pass `NULL` for `n`.

== Notes ==

The interpretation from string to floating point is done by the `strtod()` function.  Look to some docs on that function to see how it works.

See also [wiki:Docs/Ni/Ni_GetValue Ni_GetValue()].


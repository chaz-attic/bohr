= `Ni_SetValueInt()` =

{{{
#!c
int Ni_SetValueInt(
   Ni_node restrict n,
   long value
);
}}}

Sets the given node's value to an integer value (a long integer).

== Parameters ==

`n` (in) should be the [wiki:Docs/Ni/Ni_node Ni_node] whose value you're trying to set.

`value` (in) should be the `long` integer to use as `n`'s new value.

== Return Values ==

Returns the string length of `value` as a decimal number (e.g. passing `15` would return `2`), or a negative integer in an error condition (which includes trying to set the value of a root node, since root nodes can't have values).

Also returns a negative integer if you pass `NULL` for `n`.

== Notes ==

This function is implemented exactly as the one-liner:
{{{
#!c
return Ni_ValuePrint(n, "%ld", value);
}}}
Thus, see [wiki:Docs/Ni/Ni_ValuePrint Ni_ValuePrint()].

This function cannot be used to remove a node's value.  See [wiki:Docs/Ni/Ni_SetValue Ni_SetValue()] for how to remove values.

Note that since [wiki:Docs/Ni/Ni_ValuePrint Ni_ValuePrint()] sets the node to the "modified" state, so does this function.  See [wiki:Docs/Ni/Ni_GetModified Ni_GetModified()] for an explanation of the "modified" state.

See also [wiki:Docs/Ni/Ni_SetValue Ni_SetValue()].


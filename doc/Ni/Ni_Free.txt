= `Ni_Free()` =

{{{
#!c
void Ni_Free(
   Ni_node restrict n
);
}}}

Recursively frees a Nickel tree or subtree.

== Parameters ==

`n` (in) should be the root [wiki:Docs/Ni/Ni_node Ni_node] of the tree or subtree you want to free.  If `NULL`, this function has no effect.

== Return Values ==

No return value.

== Notes ==

Correctly handles recursively freeing all of `n`'s children.

You can free either an entire tree or just part of a tree with this function.  So, any [wiki:Docs/Ni/Ni_node Ni_node] is game.  It correctly handles removing references to the node if it's a child of a parent node, so the overall tree stays in sync.

Obviously, any [wiki:Docs/Ni/Ni_node Ni_node]s in the tree/subtree, including `n` itself, no longer exist after this function returns.  So be careful about keeping [wiki:Docs/Ni/Ni_node Ni_node] pointers around.

This function frees memory allocated with [wiki:Docs/Ni/Ni_New Ni_New()] (and calls to [wiki:Docs/Ni/Ni_GetChild Ni_GetChild()] that create children).  Use it to avoid memory leaks when you're done with part or all of a Nickel tree.


= `Ni_GetVersion()` =

{{{
#!c
uint32_t Ni_GetVersion(void);
}}}

Returns the version of Nickel supported by the library.

== Parameters ==

None.

== Return Values ==

The obvious.

== Notes ==

The number returned by this function obeys the same format as other in-source Nickel version numbers.  See [wiki:Docs/Ni/Ni_HEADER_VERSION Ni_HEADER_VERSION].

The value returned by this function is the version of Nickel the .so you're linked with supports.  This number can signify a range of API versions; see [wiki:Docs/Ni/Ni_HEADER_VERSION Ni_HEADER_VERSION] for an explanation.


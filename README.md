Bohr
====

At one point, I was really excited about producing some libraries to facilitate
cross-platform game development: something more modern and a little closer to
the hardware than [SDL][1].  This repo was my playground for these ideas.

I had fun with it for a while, and I think some worthwhile ideas came out of
it, but I'm not really interested in continuing down this path anymore.  It's
presented here mostly for amusement and maybe inspiration at this point.

Potentially Worthwhile Ideas
----------------------------

One idea here that I think is actually pretty rad is Nickel, a library for
efficiently accessing config data out of .ini-like files.  Nickel has some fun
features like arbitrarily deep hierarchical sections of config options, and
separate tracking of a global options file and user overrides.  At some point
I'd like to rip Nickel out of here and make it a standalone library in its own
right.  One day.

There are also many snippets of code that I'm both proud and ashamed of, such
as some kinky C macro abuse to emulate generics (e.g. vectors of any type you
can throw at them), or how I reimplement many common data structures ("so you
don't have to!"), or where I wrap some standard POSIX functions (you know, for
portability).  I guess my feeling now is that there's a lot of good but
pointless code in here.

Anyway, I hope you find something fun here regardless of whether it's useful.


Enjoy!


[1]: http://www.libsdl.org/

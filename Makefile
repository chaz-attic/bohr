###############################################################################
# The Bohr Game Libraries <https://github.com/chazomaticus/bohr>
# Copyright (C) 2012 Charles Lindsay.  Some rights reserved; see COPYING.
###############################################################################


# This Makefile may need help on non-GNU/Linux systems.  Contributions are
# welcome!  ...But no GNU autotools please.

# If you're in Windows and make isn't producing a DLL, edit the line below that
# sets SYS_RULES to read 'SYS_RULES := win'.  If you'll tell me what 'uname -s'
# reports on your system, I'll incorporate a more general fix here in a future
# version.


# Standard configurable make options.  Feel free to edit this section!
CC      = gcc
LD      = gcc
INSTALL = install

CPPFLAGS ?=
CFLAGS   ?= -O2
LDFLAGS  ?=

DESTDIR     =
prefix      = /usr/local
exec_prefix = $(prefix)
libdir      = $(exec_prefix)/lib
includedir  = $(prefix)/include


# Unless you know what you're doing, don't edit subsequent sections.


# Install destinations.
LIBS_DEST    = $(abspath $(DESTDIR)$(libdir))
HEADERS_DEST = $(abspath $(DESTDIR)$(includedir))

# Build-time OS-specific stuff.  Determines if we need to use any special rules
# for the system we're building on.
SYS       := $(shell uname -s | tr A-Z a-z)
SYS_RULES := $(if $(findstring mingw,$(SYS)),win)


# Project-specific stuff.
PROJ      = bohr
VER_MAJOR = 0
VER_MINOR = 0
VER_AGE   = 0

RELEASE = $(PROJ)-$(VER_MAJOR).$(VER_MINOR).$(VER_AGE)

IN_NAME = indium
NI_NAME = nickel
W_NAME  = tungsten

LIBS = $(PROJ) $(IN_NAME) $(NI_NAME) $(W_NAME)

DIST_FILES = \
	AUTHORS                                     \
	COPYING                                     \
	INSTALL                                     \
	Makefile                                    \
	README                                      \
	include/Makefile                            \
	include/bohr/ds.h                           \
	include/bohr/ds_hash.h                      \
	include/bohr/ds_queue.h                     \
	include/bohr/ds_str.h                       \
	include/bohr/ds_vector.h                    \
	include/bohr/in.h                           \
	include/bohr/ni.h                           \
	include/bohr/po.h                           \
	include/bohr/po_dir.h                       \
	include/bohr/po_lib.h                       \
	include/bohr/po_sync.h                      \
	include/bohr/po_thread.h                    \
	include/bohr/po_time.h                      \
	include/bohr/w.h                            \
	src/Makefile                                \
	src/indium/Makefile                         \
	src/indium/linux-evdev/devices.c            \
	src/indium/linux-evdev/indium.c             \
	src/indium/linux-evdev/internal.h           \
	src/indium/linux-evdev/poll.c               \
	src/indium/tests/Makefile                   \
	src/indium/tests/testenumdevices.c          \
	src/indium/tests/testinputevents.c          \
	src/indium/win-di8/indium.c                 \
	src/indium/win-di8/init.c                   \
	src/indium/win-di8/internal.h               \
	src/indium/win-di8/poll.c                   \
	src/nickel/Makefile                         \
	src/nickel/buf.c                            \
	src/nickel/hash.c                           \
	src/nickel/internal.h                       \
	src/nickel/io.c                             \
	src/nickel/nickel.c                         \
	src/nickel/tests/Makefile                   \
	src/nickel/tests/ni.c                       \
	src/nickel/tests/ni_parse_spaces_quotes.ini \
	src/nickel/tests/ni_parse_children.ini      \
	src/nickel/tests/ni_parse_oddities.ini      \
	src/nickel/tests/ni_big_oct.ini             \
	src/tests/Makefile                          \
	src/tests/ds_compile.c                      \
	src/tests/po_compile.c                      \
	src/tests/po_home.c                         \
	src/tests/po_ls.c                           \
	src/tungsten/Makefile                       \
	src/tungsten/tests/Makefile                 \
	src/tungsten/tests/testenum.c               \
	src/tungsten/tests/testfullscreen.c         \
	src/tungsten/tests/testwin.c                \
	src/tungsten/win/internal.h                 \
	src/tungsten/win/monitors.c                 \
	src/tungsten/win/tungsten.c                 \
	src/tungsten/x/internal.h                   \
	src/tungsten/x/tungsten.c


export


all: libs
libs: $(LIBS)
clean: $(LIBS:%=clean-%)

tests: $(LIBS:%=tests-%)
check: $(LIBS:%=check-%)

install: install-libs install-headers
install-libs: $(LIBS:%=install-libs-%)
install-headers: $(LIBS:%=install-headers-%)

uninstall: uninstall-libs uninstall-headers
uninstall-libs: $(LIBS:%=uninstall-libs-%)
uninstall-headers: $(LIBS:%=uninstall-headers-%)

$(LIBS) $(LIBS:%=clean-%) $(LIBS:%=tests-%) $(LIBS:%=check-%) $(LIBS:%=install-libs-%) $(LIBS:%=uninstall-libs-%):
	$(MAKE) $@ -C src
$(LIBS:%=install-headers-%) $(LIBS:%=uninstall-headers-%):
	$(MAKE) $@ -C include

dist: $(RELEASE).tar.gz
$(RELEASE).tar.gz:
	rm -rf $(RELEASE)
	mkdir $(RELEASE)
	cp --parents $(DIST_FILES) $(RELEASE)
	tar -czf $@ $(RELEASE)
	rm -rf $(RELEASE)

.PHONY: all libs clean tests check install install-libs install-headers uninstall uninstall-libs uninstall-headers $(LIBS) $(LIBS:%=clean-%) $(LIBS:%=tests-%) $(LIBS:%=check-%) $(LIBS:%=install-libs-%) $(LIBS:%=uninstall-libs-%) $(LIBS:%=install-headers-%) $(LIBS:%=uninstall-headers-%) dist
